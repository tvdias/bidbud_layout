<?php

	Router::connect('/', array('controller' => 'pages', 'action' => 'home'));
	Router::connect('/test_view', array('controller' => 'pages', 'action' => 'test_view'));
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));
	Router::connect(
			'/:test_controller/:test_action',
			array('controller' => 'pages', 'action' => 'test_view'),
			array('pass' => array('test_controller', 'test_action'), 'test_controller' => '[a-zA-Z0-9]*', 'test_action' => '[a-zA-Z0-9]*')
	);

/**
 * Load all plugin routes.  See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
