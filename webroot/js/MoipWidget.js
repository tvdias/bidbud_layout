(function() {
	var jQuery;
	
	var falha = new Array();
	var indice = 0;
	var URL = "https://www.moip.com.br";


	var url_request = window.location;

	if (window.jQuery === undefined || window.jQuery.fn.jquery !== '1.7') {
	    var script_tag = document.createElement('script');
	    script_tag.setAttribute("type","text/javascript");
	    script_tag.setAttribute("src", URL + "/scripts/jquery-1.7.min.js");
	    script_tag.onload = scriptLoadHandler;
	    script_tag.onreadystatechange = function () {
	        if (this.readyState == 'complete' || this.readyState == 'loaded') {
	            scriptLoadHandler();
	        }
	    };
	
	    (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
	} else {
	    jQuery = window.jQuery;
	    jQuery.getScript(URL + "/js/conutils.js");
		jQuery.getScript(URL + "/widget-fb/js/json2.js");
		jQuery.getScript(URL + "/scripts/util.js");
	}
	
	function scriptLoadHandler() {
	    jQuery = window.jQuery.noConflict(false);
	    jQuery.getScript(URL + "/js/conutils.js");
		jQuery.getScript(URL + "/widget-fb/js/json2.js");
		jQuery.getScript(URL + "/scripts/util.js");
	}

	MoipWidget = function(json) { 

	jQuery(document).ready(function($) {
	    prepare_the41st();
		param = {  
				pagamentoWidget:{
					referer : url_request.href,
		        	token : $("#MoipWidget").attr("data-token"),
		            jscData : $("#mcdata").val(),
		        	dadosPagamento: json
				}
	    };
		
	var service_url = "/rest/pagamento?callback=?"; 

	validate_before_submit(param);

       if (falha.length < 1) {
    	   var conf = {	
				url: URL + service_url,
				type: "GET",
				dataType: "json", 
				async: true,
				data: {"pagamentoWidget":JSON.stringify(param)},
				headers:{
					"Content-Type": "application/json",
					"Accept": "application/json"
				},
				success: function(data){
					
					if(data.StatusPagamento == "Sucesso"){
						var callback_success = $("#MoipWidget").attr("callback-method-success");
						addInfoToanswwer(data);
			            eval(callback_success)(data);
					} else {
						var callback_error = $("#MoipWidget").attr("callback-method-error");
				        eval(callback_error)(data);
					}
				}
    	   };
    	   $.ajax(conf);

        } else { 
	      var callback_error = $("#MoipWidget").attr("callback-method-error");
	      eval(callback_error)(falha);             
		}
      });
    };
	
	add_error = function(data){
	    falha[indice++] = data;
	};

	prepare_the41st = function() {
		var divMoip = jQuery("#MoipWidget");
		var input41st = document.createElement("input");
		jQuery(input41st).attr("type","hidden").attr("id","mcdata").attr("name","mcdata");
		divMoip.append(input41st);
		mcdata.parsem("mcdata");
	};
	
	validate_before_submit = function(json) {
		
		validate_token(json);
		//eval("validate_" + json.pagamentoWidget.dadosPagamento.Forma + "(" + json + ")"); // call correct validation
		if(!jQuery.isFunction(window["validate_" + json.pagamentoWidget.dadosPagamento.Forma])) {
			add_error("Forma inválida");
			return;
		}
		var fn = window["validate_" + json.pagamentoWidget.dadosPagamento.Forma];
		fn(json);
	};
	
	validate_token = function(json) {
		if(json.pagamentoWidget.token == undefined || json.pagamentoWidget.token == "") {
			add_error("TOKEN inválido");			
		}
	};
	
	validate_CartaoCredito = function(json) {
		var parcelas = json.pagamentoWidget.dadosPagamento.Parcelas;
		validate_credit_card(json);
		validate_parcelas(parcelas);
	};
	
	validate_BoletoBancario = function(json) {
		//Sem valida��es
	};
	
	validate_DebitoBancario = function(json) {
		if (json.pagamentoWidget.dadosPagamento.Instituicao == undefined || json.pagamentoWidget.Instituicao) {
			add_error("Instituição Invalida");
		} 
	};
	
	validate_vault = function(json) {
		if(json.cartao.Cofre == undefined || json.cartao.Cofre == ""){
			add_error("COFRE inválido");
		}
		
		if (json.cartao.CodigoSeguranca == undefined || json.cartao.CodigoSeguranca == "") {
			add_error("Código de segurança inválido");
		}
	};
	
	validate_credit_card = function(json) {
		
		var dados = {cartao:json.pagamentoWidget.dadosPagamento.CartaoCredito};

		if(dados.cartao == undefined || dados.cartao == ""){
			add_error("Informe os dados do cartão de crédito");
		} else {	

			if(has_vault(dados)){
				validate_vault(dados);	
		
			} else {		

				if(dados.cartao.Numero == undefined || dados.cartao.Numero == ""){
					add_error("Número de cartão inválido");
				} else {
					//validate_card_brand(dados.cartao.Numero);
				}

				if(dados.cartao.CodigoSeguranca == undefined || dados.cartao.CodigoSeguranca == ""){
					add_error("Código de segurança inválido");
				} 
		 
				validate_data(dados.cartao.Expiracao);
				validate_portador(dados.cartao.Portador);
			}
		}
	}; 
	
  validate_parcelas = function(parcelas){

      if(parcelas != undefined || parcelas != ""){
	 	 if(isNaN(parcelas) || parcelas < 1 || parcelas > 15){
			  add_error("Número de parcelas inválido");
		  }                  
   	  } else {
              add_error("Námero de parcelas inválido");                    
 	  }
   };
	
	validate_portador = function(portador){
		
		if(portador.Nome == undefined || portador.Nome == ""){
            add_error("Nome do portador inválido");
		}
		
		if(portador.DataNascimento == undefined || portador.DataNascimento == ""){
            add_error("Data nascimento do portador inválido");
		}
		
		if(portador.Telefone == undefined || portador.Telefone == ""){
            add_error("Telefone do portador inválido");
		}
		
		if(portador.Identidade == undefined || portador.Identidade == ""){
            add_error("Nome do portador invalido");
		}		
	};
	
	
	validate_data = function(data){
		
		if(data == undefined || data == ""){
			add_error("Data de expiração inválida");							
		} else {
			
			if(data.length != 5 || data[2] !== "/"){
				add_error("Data de expiração deve ser MM/AA");	
			} else {
				 var mes_ano = data.split("/");
				 var mes = mes_ano[0];
				
				 if(Number(mes) > 12 || Number(mes) < 1){
					 add_error("Data de expiração inválida");
				 }
			}				
		}	
		
	};
	
	addInfoToanswwer = function(data) {
		data["url"] = URL + "/Instrucao.do?token=" + $("#MoipWidget").attr("data-token");
	};
	
	has_vault = function(json) {
		return json.cartao.Cofre != undefined && json.cartao.Cofre != "";
	};

	window.MoipWidget = MoipWidget;

})();
