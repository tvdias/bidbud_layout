var timestamp = 0;
var gtTimeout = 950;
var gtUpdtTime = 1000;
var errCnt = 0;
var showErr = 0;

//$(document).ready(function() {
function gettime() {
//	//var timestamp = 0;
//	var gtTimeout = 750;
//	//var gtTimeoutMin = 350;
//	//var gtTimeoutMax = 950;
//	var gtUpdtTime = 1000;
//	//var gtUpdtCicle = 5;
//	//var errTimer = 0;
//	var errCnt = 0;
//	var showErr = 0;

	setInterval(function() {
		//if(timestamp%gtUpdtCicle == 0){
		//if(true){
			$.ajax({
				url: "http://timer.bidbud.com.br/index.php",
				dataType: "jsonp",
				jsonpCallback: "gettime_cb",
				timeout: gtTimeout,
				//async: true,
				//global: false,
				//cache: false,
				//success: function(data) {
				//	timestamp=data;
				//	clkCycle();
				//	//gtTimeout = Math.max(gtTimeoutMin, gtTimeout-10);
				//	if (errCnt > 0) {
				//		errCnt = showErr = 0;
				//		$("#bottom_message").slideUp();
				//	}
				//},
				//success: gettime_cb,
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					//$('#debug_node').append('<li>'+ data.toString() +'</li>');
					//gtTimeout = Math.min(gtTimeoutMax, gtTimeout+50);
					if (timestamp > 0) {
						timestamp++;
						clkCycle();
					}
					errCnt++;
					if (errCnt > 2 && showErr == 0) {
						$("#bottom_message div p").html("Sua conexão apresenta problemas de comunicação com nossos servidores.");
						$("#bottom_message").delay(100).slideDown();
						showErr = 1;
					}
				}
			});
		////} else {
		////	timestamp++;
		////}
		//$('#timestamp').html(timestamp);
		////if (errCnt > 0) errTimer++;
	}, gtUpdtTime);
}
//});
function gettime_cb(time) {
	timestamp=time;
	clkCycle();
	if (errCnt > 0) {
		errCnt = showErr = 0;
		$("#bottom_message").slideUp();
	}
}

//var dogbarkSnd = document.createElement('audio');
//var canPlayDogbark = dogbarkSnd.play ? true : false;
//var clkCycleTime=500;
//var clkDivider=clkCycleTime/1000;

//function clkCycle() {
//	var clkCycleTime=500;
//	setInterval(function() {
//		if (timestamp > 0) {
//			$('div[name="auction_element"]').each(function(index) {
//				var acId 	= this.id.split('_', 2)[1];
//				var acCl	= $('#cl_' + acId).text();
//				var acTime	= $('#timer_' + acId);
//
//				if (acCl == 0) {
//					//var tmTimestamp	= $('#timestamp');
//					//var acTL	= $('#tl_' + acId);
//					//var acET	= $('#et_' + acId);
//					//var acTlt	= $(acTL).text() - clkDivider;
//					//var acTlt	= $(acET).text() - $(tmTimestamp).text();
//					var acTlt	= $('#et_' + acId).text() - timestamp;
//					//acTL.text(acTlt);
//					acTime.text(secsToString(acTlt));
//
//					if(acTlt <= 10) {
//						acTime.addClass("countdown_ending");
//						//if(acTlt == 3 && canPlayDogbark) dogbarkSnd.play();
//						if(acTlt < 0) {
//							acTime.text("Verificando...");
//						}
//					} else {
//						acTime.removeClass("countdown_ending");
//					}
//				}
//			});
//		}
//	}, clkCycleTime);
//}

function clkCycle() {
	$('div[name="auction_element"]').each(function(index) {
		var acId 	= this.id.split('_', 2)[1];
		var acCl	= $('#cl_' + acId).text();
		var acTime	= $('#timer_' + acId);

		if (acCl == 0) {
			//var tmTimestamp	= $('#timestamp');
			//var acTL	= $('#tl_' + acId);
			//var acET	= $('#et_' + acId);
			//var acTlt	= $(acTL).text() - clkDivider;
			//var acTlt	= $(acET).text() - $(tmTimestamp).text();
			var acTlt	= $('#et_' + acId).text() - timestamp;
			//acTL.text(acTlt);
			acTime.text(secsToString(acTlt));

			if(acTlt <= 10) {
				acTime.addClass("countdown_ending");
				//if(acTlt == 3 && canPlayDogbark) dogbarkSnd.play();
				if(acTlt < 0) {
					acTime.text("Verificando...");
				}
			} else {
				acTime.removeClass("countdown_ending");
			}
		}
	});
}

function updateCnt(data) {
	var tp = (data.tp).toLowerCase();
	switch(data.tp) {
		case "cl":
			update_cl(data);
			break;
		case "bd":
			update_bd(data);
			break;
		case "sp":
			update_sp(data);
			break;
		case "usp":
			update_usp(data);
			break;
	}
}

function update_cl(data){
	var ids = data.ids;

	$.each(ids, function(i, obj) {
		var acId = obj; 

		$('#cl_' + acId).text(1);
		$('#bidmainbtn_' + acId +' > a').addClass('ended');
		$('#timer_' + acId).addClass('arrematado').text('Arrematado!');
	});
}

function update_bd(data){
	var acId	= data.aid;
	var acPr	= $('#pr_' + acId);
	var data_p_int	= parseInt(data.p);
	var acPr_int	= parseInt(acPr.text());

	if (!isNaN(data_p_int) && !isNaN(acPr_int) && data_p_int > acPr_int) {
		var acTime	= $('#timer_' + acId);
		var acPrice	 = $('#price_' + acId);
		var acLeader = $('#leader_' + acId);
		var bidHistoryTable = $('#bidHistoryTable_' + acId);

		acPr.text(data.p);
		$('#et_' + acId).text(data.et);
		var acTlt = data.et - timestamp;
		if(acTlt <= 10)
			acTime.text(secsToString(acTlt)).addClass("countdown_ending");
		else
			acTime.text(secsToString(acTlt)).removeClass("countdown_ending");

		acPrice.text(Number(data.p/100).toMoney(2,',','')).animate({color:"#A2AD00"},'fast').animate({color:"#2199D5"},'fast');
		if (!data.l) data.l = "&nbsp;"
			acLeader.text(data.l);

		$('#discount_'+ acId).text(Number(data.d).toMoney(2,',',''));
		if (bidHistoryTable.length > 0) {
			$('#saving_'+ acId).text(Number(data.s).toMoney(2,',',''));

			var hasRows = false;
			var rows = '';
			if (data.b.length > 0) {
				$.each(data.b, function(n, tRow) {
					//rData = tRow.split('/', 3);
					//rows += '<tr><td align=\"center\" class=\"leilaoTd cinza s12\">' + rData[0] + "</td><td align=\"center\" class=\"leilaoTd cinza s12\">" + rData[1] + '</td><td align=\"center\" class=\"leilaoTd cinza s12\">' + rData[2] + '</td></tr>\n';
					rows += "<tr><td align=\"center\" class=\"leilaoTd cinza s12\">" + tRow.l + '</td><td align=\"center\" class=\"leilaoTd cinza s12\">' + tRow.d + '</td></tr>\n';
					//rows += '<tr><td align=\"center\" class=\"leilaoTd cinza s12\">' + tRow.p + "</td><td align=\"center\" class=\"leilaoTd cinza s12\">" + tRow.u + '</td><td align=\"center\" class=\"leilaoTd cinza s12\">' + tRow.d + '</td></tr>\n';
					hasRows = true;
				} );
			}
			//if (hasRows) $(bidHistoryTable).find('tbody').html(rows);
			if (hasRows) $('tbody', bidHistoryTable).html(rows);
		}
	}
}

function update_sp(data){
	var ids = data.ids;

	$.each(ids, function(i, obj) {
		var acId = obj; 

		$('#cl_' + acId).text(1);
		$('#bidmainbtn_' + acId +' > a').removeClass('ended').addClass('suspended');
		$('#timer_' + acId).addClass('arrematado').text('Suspenso!');
		$('#et_' + acId).text("99999999999");
	});
}

function update_usp(data){
	var end_times = data.end_times;

	$.each(end_times, function(i, obj) {
		var acId = obj.id; 

		$('#cl_' + acId).text(0);
		$('#bidmainbtn_' + acId +' > a').removeClass('ended').removeClass('suspended');
		$('#timer_' + acId).removeClass('arrematado').text('Retornando...');
		$('#et_' + acId).text(obj.end_time);
	});
}

function secsToString(diff) {
	if (diff < 0){
		//diff = 0;
		return '00:00:00';
}

	day = Math.floor(diff/86400);
	diff -= day * 86400;
	hour = Math.floor(diff/3600);
	diff -= hour * 3600;
	minute = Math.floor(diff/60);
	//second = diff - minute * 60;
	second = Math.ceil(diff - minute * 60);
	//second = Math.floor(diff - minute * 60);
	//second = (diff - minute * 60).toFixed(1);
	if (day < 1){
		day = '';
	} else {
		day = day+'d';
	}
	if (hour < 10){
		hour = '0'+hour;
	}
	if (minute < 10){
		minute = '0'+minute;
	}
	if (second < 10) {
		second = '0'+second;
	}
	return day+' '+hour+':'+minute+':'+second;
}

//http://stackoverflow.com/questions/149055/how-can-i-format-numbers-as-money-in-javascript
Number.prototype.toMoney = function(decimals, decimal_sep, thousands_sep)
{ 
 var n = this,
 c = isNaN(decimals) ? 2 : Math.abs(decimals), //if decimal is zero we must take it, it means user does not want to show any decimal
 d = decimal_sep || ',', //if no decimal separetor is passed we use the comma as default decimal separator (we MUST use a decimal separator)

 /*
 according to [http://stackoverflow.com/questions/411352/how-best-to-determine-if-an-argument-is-not-sent-to-the-javascript-function]
 the fastest way to check for not defined parameter is to use typeof value === 'undefined' 
 rather than doing value === undefined.
 */ 
 t = (typeof thousands_sep === 'undefined') ? '.' : thousands_sep, //if you don't want ot use a thousands separator you can pass empty string as thousands_sep value

 sign = (n < 0) ? '-' : '',

 //extracting the absolute value of the integer part of the number and converting to string
 i = parseInt(n = Math.abs(n).toFixed(c)) + '', 

 j = ((j = i.length) > 3) ? j % 3 : 0; 
 return sign + (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : ''); 
};


$(document).ready(function(){
	if(!user_logged_in) {
		$('.bidmainbtn > a, .budLarge > a, .budSmall > a').mouseover(function() {
			$(this).addClass('mustlog');
		}).mouseout(function() {
			$(this).removeClass('mustlog');
		}).attr('href', "/login");
	} else {
		// Function for bidding
		//$('.bidmainbtn > a').click(function(){
		$('a.bidlnk').click(function() {
			var auction_id = $(this).attr('id').split('_')[1];

			//if(!user_logged_in) {
			//	location.href = "/login";
			//	return false;
			//}

			$.ajax({
				url: '/bid.php',
				dataType: 'json',
				type: 'POST',
				timeout: 1250,
				global: false,
				cache: false,
				//data: {id:auction_id, sid:current_sid},
				data: {id:auction_id},
				success: function(data) {
					//bidMessage.html(data.Auction.message).show(1).animate({opacity: 1.0}, 2000).hide(1);
					//bidButton.show(1);
					//bidLoading.hide(1);
					//$('#debug_node').append('<li>'+ data.toString() +'</li>');
					if (data.success == 1) {
						$('.commonBids').text(data.user_balance);
						//$('.expBids').text(data.user_exp_balance);

						var usrBids = $('#myBidsCount_'+auction_id);
						var usrBidsT = parseInt(usrBids.text())+1;
						usrBids.text(usrBidsT);

//					// updates discount level if cur < last
//					if (disc_cur < disc_last) {
//						var disc_cur_new = disc_cur;
//						for (var i = disc_cur+1; i <= disc_last ; i++ ) {
//							if (usrBidsT >= minbids[i]) {
//								disc_cur_new = i;
//								break;
//							}
//						}
//						if (disc_cur_new > disc_cur) {
//							disc_cur = disc_cur_new; 
//							alert("novo desconto!");
//						}
//						else
//							alert("sem desconto novo...");
//					}
					} else {
						// alert
						if (data.show_message == 1)
							alert(data.message);
						// confirm dialog
						else if (data.show_message == 2) {
							var r = confirm(data.message);
							if (r == true)
								window.location = data.message_url;
						}
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown){
					//$('.countdown').text(textStatus);
					//$('#debug_node').prepend('<li>'+ textStatus +' '+ errorThrown +' '+ XMLHttpRequest +'</li>');
				}
			});

			return false;
		});
	}

//	//www.javascriptkit.com/javatutors/createelementcheck.shtml
//	if (canPlayDogbark) {
//		dogbarkSnd.setAttribute('src', 'http://c766382.r82.cf2.rackcdn.com/dogbark.wav');
//		dogbarkSnd.load()
//		dogbarkSnd.addEventListener("load", function() { 
//			dogbarkSnd.play();
//		}, true);
//	}
});

function sumCounter(objCounter, value) {
	bids_counter = parseInt(objCounter.text());
	bids_counter = value + bids_counter;
	if (bids_counter <= 0 ) bids_counter = 0;
	objCounter.text(bids_counter);
}

function incrementCounter(counter_id) {
	counter = parseInt(counter_id.text());
	if (counter < 0 || isNaN(counter)) counter = 0;
	counter++;
	counter_id.text(counter);
}

//function setCounter(objCounter, value) {
//	objCounter.text(value);
//}

$(document).ready(function() {
	// hide #back-top first
	$("#fl-bids").hide();
	$(window).scroll(function () {
		if ($(this).scrollTop() > 100) {
			//$('#fl-bids').fadeIn();
			$('#fl-bids').fadeIn();
		} else {
			$('#fl-bids').fadeOut();
		}
	});
	// scroll body to 0px on click
	$('#fl-bids a.topo').click(function () {
		$('body,html').animate({
			scrollTop: 0
		}, 800);
		return false;
	});

	$("input").click(function(){resetInputVal($(this));});
	$("input").focus(function(){resetInputVal($(this));});

	// reset inputs
	function resetInputVal( obj ) {
		previousTXT = obj.val();
		id = obj.attr('id');
		if (id == "headeruserpassword") {
			obj.replaceWith('<input id="pass" type="password" value="" tabindex="2" name="data[User][password]" style="position: absolute; top: 115px; left: 40px; width: 180px;" />')
			$('#pass').focus();
		}
		if (previousTXT == "Procurar um Leilão" || previousTXT == "Usuário" || previousTXT == "Senha") {
			obj.val("");
		}
	}

	// search form
	$("#mainSearchText").blur(function(){
		procurarTXT = $(this).val();
		if (procurarTXT == "") {
			$(this).val("Procurar um Leilão");
		}
	});

	// menu
	$("#categorias, #categoriasSubMenu").hover(function() {
		$("#categoriasSubMenu").show();
		$("#categorias").attr("src", "https://c14985174.ssl.cf2.rackcdn.com/img/menu/categorias_over.png");
	}, function () {
		$("#categoriasSubMenu").hide();
		$("#categorias").attr("src", "https://c14985174.ssl.cf2.rackcdn.com/img/menu/categorias.png");
	});
	$("#login, #loginOver").hover(function() {
		$("#loginOver").show();
	}, function () {
		$("#loginOver").hide();
	});

	// close auction box
	$(".closeBox").click(function(){
		$(this).parent().parent().hide("250");
	});
	
	// forms
	$(".inputText").focus(function() {
		$(this).css("background", "#faffbd");
	});
	$(".inputText").blur(function() {
		$(this).css("background", "white");
	});

	$("#tweets, #likes").hover(
		function() {
			$(this).animate({left: '0px'}, 200);
		},
		function () {
			$(this).animate({left: '-100px'}, 200);
	});

	$(".favoriteIcon").click(function () {
		var item_to_like = $(this).attr('id').split('_')[1];
		var image = $(this).find('img');
		var counter = $(this).find('span');

		$.post("/meu/items/like", {item_id:item_to_like}, function (data) {
			dislikeimage = "/img/hrt.png";
			likeimage = "/img/hrt_red.png";
			if (data.error != "1") {
				if (data.likeit) {
					image.attr('src', likeimage);
					sumCounter(counter, 1);
				} else {
					image.attr('src', dislikeimage);
					sumCounter(counter, -1);
				}
			}
		},"json");

		return false;
	});
});


// SSL Seal
function seal_getFlashVersion() {
	var version = 0;
	if (navigator.plugins && navigator.mimeTypes.length) {
		var plugin = navigator.plugins["Shockwave Flash"];
		if (plugin && plugin.description) {
			version = parseInt(plugin.description.replace(/([a-zA-Z]|\s)+/, "").replace(/(\s+r|\s+b[0-9]+)/, ".").split(".")[0]);
		}
	}
	else {
		try {
			var flashObj = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.7");
		}
		catch (e) {
			try {
				var flashObj = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.6");
				version = 6;
				flashObj.AllowScriptAccess = "always";
			}
			catch (e) {
				if (version == 6) {
					return version;
				}
			}
			try {
				flashObj = new ActiveXObject("ShockwaveFlash.ShockwaveFlash");
			}
			catch (e) {}
		}
		if (flashObj != null) {
			version = parseInt(flashObj.GetVariable("$version").split(" ")[1].split(",")[0]);
		}
	}
	return version;
}

function seal_useFlash() {
	var minVersion = 8;
	var resellerId = "1";
	var sealVersion = "3";
	var useFlashPref = "false";

	return useFlashPref == "true" && (resellerId == "1" || sealVersion == "0") && self == top && minVersion <= seal_getFlashVersion();
}

function seal_installSeal() {
	var commonName = "apetrexoleilao.com.br";
	var org = "apetrexoleilao.com.br";
	var resellerId = "1";
	var sealVersion = "3";
	var isEV = "false";
	var requiresFilter = "false";
	var sealName = "siteseal_gd_3_h_l_m";
	var flashUrl = "https://seal.godaddy.com/flash/3/siteseal_gd_3_h_l_m.swf";
	var imageUrl = "https://seal.godaddy.com/images/3/siteseal_gd_3_h_l_m.gif";
	var sealWidth = "132";
	var sealHeight = "31";
	var sealType = "dv";
	
	if (seal_useFlash()) {
		document.write('<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="https://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0" width="' + sealWidth + '" height="' + sealHeight + '" id="' + sealName + '" align="middle">');
		document.write('<param name="movie" value="' + flashUrl);

		if (sealVersion == "0" && sealType == "dv")
			document.write('?domainName=' + commonName + '&color=000000');
		else if (sealVersion == "0" && (sealType == "iv" || sealType == "ev"))
			document.write('?companyName=' + commonName + ' - ' + org);

		document.write('" />');
		document.write('<param name="quality" value="high" />');
		document.write('<param name="AllowScriptAccess" value="always" />');

		if (sealVersion == "0" && (sealType == "iv" || sealType == "ev"))
			document.write('<param name="bgcolor" value="#333333" />');
		else
			document.write('<param name="wmode" value="transparent" />');

		document.write('<embed src="' + flashUrl);

		if (sealVersion == "0" && (sealType == "iv" || sealType == "ev"))
			document.write('?companyName=' + commonName + ' - ' + org + '" bgcolor="#333333"');
		else {
			if (sealVersion == "0" && sealType == "dv")
				document.write('?domainName=' + commonName + '&color=000000');
			document.write('" wmode="transparent"');
		}

		document.write(' quality="high" width="' + sealWidth + '" height="' + sealHeight + '" name="' + sealName + '" align="middle" type="application/x-shockwave-flash" pluginspage="https://www.macromedia.com/go/getflashplayer" AllowScriptAccess="always" />');
		document.write('</object>');
	}
	else if (requiresFilter == "true" && document.body && document.body.filters)
		document.write('<span style="display:inline-block;cursor:pointer;cursor:hand;filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'' + imageUrl + '\', sizingMethod=\'crop\');width:' + sealWidth + 'px;height:' + sealHeight + 'px;" onclick="verifySeal();"></span>');
	else
		//document.write('<img style="cursor:pointer;cursor:hand" width="' + sealWidth + '" height="' + sealHeight + '" src="' + imageUrl + '" onclick="verifySeal();"/>');
		$("#seals").append('<img style="cursor:pointer;cursor:hand" width="' + sealWidth + '" height="' + sealHeight + '" src="' + imageUrl + '" onclick="verifySeal();"/>');

	if (isEV == "true") {
		if (resellerId == "1")
			document.write('<img src="https://evbeacon.godaddy.com/images/spacer.gif"/>');
		else
			document.write('<img src="https://evbeacon.starfieldtech.com/images/spacer.gif"/>');
	}
}

function verifySeal() {
	var bgHeight = "433";
	var bgWidth = "592";
	var url = "https://seal.godaddy.com/verifySeal?sealID=M56JaI6GBj3AuSvY54i0tKo7AmOSpxotS4dXbfQ9jhSMGRSJY0X1";
	window.open(url,'SealVerfication','location=yes,status=yes,resizable=yes,scrollbars=no,width=' + bgWidth + ',height=' + bgHeight);
}

/* MOIP - start */
function sendmoip() {
	var settings = {
		"Forma": "CartaoCredito",
		"Instituicao": $("input[name=OpcsCartao]:checked").val(), //Visa", //MasterCard
		"Parcelas": 1,
		"CartaoCredito": {
			"Numero": $("input[name=Numero]").val(),
			"Expiracao": $("input[name=Expiracao]").val(),
			"CodigoSeguranca": $("input[name=CodigoSeguranca]").val(),
			"Portador": {
				"Nome": $("input[name=Nome]").val(),
				"DataNascimento": $("input[name=DataNascimento]").val(),
				"Telefone": $("input[name=Telefone]").val(),
				"Identidade": $("input[name=Identidade]").val()
			}
		}
	};

	falha = new Array();
	$("#processingpayment").dialog('open');
	//$(".ui-dialog-titlebar").hide();
	//$("#sendToMoip").attr("disabled", "disabled");
	$("#sendToMoip").hide();
	MoipWidget(settings);
	return false;
}

var funcao_falha = function(data) {
	//CALLBACK DE FALHA
	//alert('Falha \n\n ' + data);
	//$("#sendToMoip").removeAttr("disabled");
	//alert("Falha ao processar o pagamento.");
	$("#processingpayment_fail p").html("Falha ao processar o pagamento.<br/>\nVerifique os dados do formulário de pagamento.");
	$("#processingpayment").dialog('close');
	$("#processingpayment_fail").dialog('open');
	$("#sendToMoip").show();
};
var funcao_falha_validacao = function(data) {
	//CALLBACK DE FALHA
	//alert('Falha \n\n ' + data);
	//$("#sendToMoip").removeAttr("disabled");
	//alert("Falha ao processar o pagamento.");
	var errors_str = "<ul>\n";
	for (error in data) errors_str += "<li>"+data[error]+"</li>\n";
	errors_str += "</ul>";
	$("#processingpayment_fail p").html("Falha ao processar o pagamento.<br/>\n\n" + errors_str);
	$("#processingpayment").dialog('close');
	$("#processingpayment_fail").dialog('open');
	$("#sendToMoip").show();
};
var funcao_falha_comunicacao = function(data) {
	//CALLBACK DE FALHA
	//alert('Falha \n\n ' + data);
	//$("#sendToMoip").removeAttr("disabled");
	//alert("Falha ao processar o pagamento.");
	$("#processingpayment_fail p").html("Falha ao processar o pagamento.<br/>\n\n" + data);
	$("#processingpayment").dialog('close');
	$("#processingpayment_fail").dialog('open');
	$("#sendToMoip").show();
};

/* moipwidget.js */
/*(function() {
	var jQuery;

	var falha = new Array();
	var indice = 0;
	var URL = "https://www.moip.com.br";

	var url_request = window.location;

	if (window.jQuery === undefined || window.jQuery.fn.jquery !== '1.7') {
		var script_tag = document.createElement('script');
		script_tag.setAttribute("type","text/javascript");
		script_tag.setAttribute("src", URL + "/scripts/jquery-1.7.min.js");
		script_tag.onload = scriptLoadHandler;
		script_tag.onreadystatechange = function () {
			if (this.readyState == 'complete' || this.readyState == 'loaded') {
				scriptLoadHandler();
			}
		};

		(document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
	} else {
		jQuery = window.jQuery;
		jQuery.getScript(URL + "/js/conutils.js");
		jQuery.getScript(URL + "/widget-fb/js/json2.js");
		jQuery.getScript(URL + "/scripts/util.js");
	}

	function scriptLoadHandler() {
		jQuery = window.jQuery.noConflict(false);
		jQuery.getScript(URL + "/js/conutils.js");
		jQuery.getScript(URL + "/widget-fb/js/json2.js");
		jQuery.getScript(URL + "/scripts/util.js");
	}

	MoipWidget = function(json) {
		jQuery(document).ready(function($) {
			prepare_the41st();
			param = {
				pagamentoWidget:{
					referer : url_request.href,
					token : $("#MoipWidget").attr("data-token"),
					jscData : $("#mcdata").val(),
					dadosPagamento: json
				}
			};

			var service_url = "/rest/pagamento?callback=?"; 

			falha = new Array();
			validate_before_submit(param);

			if (falha.length < 1) {
				var conf = {	
						url: URL + service_url,
						type: "GET",
						dataType: "json",
						async: true,
						data: {"pagamentoWidget":JSON.stringify(param)},
						headers:{
							"Content-Type": "application/json",
							"Accept": "application/json"
						},
						success: function(data){
							if(data.StatusPagamento == "Sucesso"){
								var callback_success = $("#MoipWidget").attr("callback-method-success");
								addInfoToanswwer(data);
								eval(callback_success)(data);
							} else {
								var callback_error = $("#MoipWidget").attr("callback-method-error");
								eval(callback_error)(data);
							}
						},
						error: function(XMLHttpRequest, textStatus, errorThrown){
							var callback_error = $("#MoipWidget").attr("callback-method-error-network");
							eval(callback_error)("Erro de comunicação com MoIP.");
						}
				};
				$.ajax(conf);
			} else { 
				var callback_error = $("#MoipWidget").attr("callback-method-error-validation");
				eval(callback_error)(falha); 
			}
		});
	};

	add_error = function(data){
		falha[indice++] = data;
	};

	prepare_the41st = function() {
		var divMoip = jQuery("#MoipWidget");
		var input41st = document.createElement("input");
		jQuery(input41st).attr("type","hidden").attr("id","mcdata").attr("name","mcdata");
		divMoip.append(input41st);
		mcdata.parsem("mcdata");
	};

	validate_before_submit = function(json) {
		validate_token(json);
		//eval("validate_" + json.pagamentoWidget.dadosPagamento.Forma + "(" + json + ")"); // call correct validation
		if(!jQuery.isFunction(window["validate_" + json.pagamentoWidget.dadosPagamento.Forma])) {
			add_error("Forma inválida");
			return;
		}
		var fn = window["validate_" + json.pagamentoWidget.dadosPagamento.Forma];
		fn(json);
	};

	validate_token = function(json) {
		if(json.pagamentoWidget.token == undefined || json.pagamentoWidget.token == "") {
			add_error("TOKEN inválido");			
		}
	};
	
	validate_CartaoCredito = function(json) {
		var parcelas = json.pagamentoWidget.dadosPagamento.Parcelas;
		validate_credit_card(json);
		validate_parcelas(parcelas);
	};
	
	validate_BoletoBancario = function(json) {
		//Sem validacoes
	};

	validate_DebitoBancario = function(json) {
		if (json.pagamentoWidget.dadosPagamento.Instituicao == undefined || json.pagamentoWidget.Instituicao) {
			add_error("Instituição Inválida");
		}
	};

	validate_vault = function(json) {
		if(json.cartao.Cofre == undefined || json.cartao.Cofre == ""){
			add_error("COFRE inválido");
		}
		if (json.cartao.CodigoSeguranca == undefined || json.cartao.CodigoSeguranca == "") {
			add_error("Código de segurança inválido");
		}
	};
	
	validate_credit_card = function(json) {
		var dados = {cartao:json.pagamentoWidget.dadosPagamento.CartaoCredito};
		if(dados.cartao == undefined || dados.cartao == ""){
			add_error("Informe os dados do cartão de crédito");
		} else {	
			if(has_vault(dados)){
				validate_vault(dados);	
			} else {		
				if(dados.cartao.Numero == undefined || dados.cartao.Numero == ""){
					add_error("Número de cartão inválido");
				} else {
					//validate_card_brand(dados.cartao.Numero);
				}
				if(dados.cartao.CodigoSeguranca == undefined || dados.cartao.CodigoSeguranca == ""){
					add_error("Código de segurança inválido");
				} 
				validate_data(dados.cartao.Expiracao);
				validate_portador(dados.cartao.Portador);
			}
		}
	}; 
	
	validate_parcelas = function(parcelas){
		if(parcelas != undefined || parcelas != ""){
			if(isNaN(parcelas) || parcelas < 1 || parcelas > 15){
				add_error("Número de parcelas inválido");
			}
		} else {
			add_error("Número de parcelas inválido");
		}
	};

	validate_portador = function(portador){
		if(portador.Nome == undefined || portador.Nome == ""){
			add_error("Nome do portador inválido");
		}	
		if(portador.DataNascimento == undefined || portador.DataNascimento == ""){
			add_error("Data nascimento do portador inválida");
		}
		if(portador.Telefone == undefined || portador.Telefone == ""){
			add_error("Telefone do portador inválido");
		}
		if(portador.Identidade == undefined || portador.Identidade == ""){
			add_error("Nome do portador inválido");
		}		
	};

	validate_data = function(data){
		if(data == undefined || data == ""){
			add_error("Data de expiração inválida");							
		} else {
			if(data.length != 5 || data[2] !== "/"){
				add_error("Data de expiração deve ser MM/AA");	
			} else {
				var mes_ano = data.split("/");
				var mes = mes_ano[0];
				if(Number(mes) > 12 || Number(mes) < 1){
					add_error("Data de expiração inválida");
				}
			}
		}
	};

	addInfoToanswwer = function(data) {
		data["url"] = URL + "/Instrucao.do?token=" + $("#MoipWidget").attr("data-token");
	};

	has_vault = function(json) {
		return json.cartao.Cofre != undefined && json.cartao.Cofre != "";
	};

	window.MoipWidget = MoipWidget;
})();*/
/* MOIP - end */