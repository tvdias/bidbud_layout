<?php
/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

/**
 * Default helper
 *
 * @var array
 */
	public $helpers = array('Html');
	public $components = array(/*'RequestHandler',*/ 'Session');

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array();

/**
 * Displays a view
 *
 * @param mixed What page to display
 */
	public function display() {
		$path = func_get_args();

		$count = count($path);
		if (!$count) {
			$this->redirect('/');
		}
		$page = $subpage = $title = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			switch($path[$count - 1]) {
				case "about":
					$title_for_layout = "Sobre";
					break;
				case "agreement":
					$title_for_layout = "Termos e Condições de Uso";
					break;
				case "how_it_works":
					$title_for_layout = "Como Funciona";
					break;
				case "nojs":
					$title_for_layout = "Javascript não habilitado";
					break;
				case "out_of_service":
					$title_for_layout = "Em Manutenção";
					break;
				case "privacy":
					$title_for_layout = "Política de Privacidade";
					break;
				default:
					$title_for_layout = Inflector::humanize($path[$count - 1]);
					break;
			}
		}
		$this->set(compact('page', 'subpage', 'title_for_layout'));
		$this->render(implode('/', $path));
	}

/**
 * Home view
 *
 * @param mixed What page to display
 */
	public function home() {
		$this->set('title_for_layout', "Início");
		$this->render('home', 'cake');
	}

/**
 * Tests a view
 *
 * @param mixed What page to display
 */
	public function test_view($test_controller = NULL, $test_action = NULL) {
		if ($this->request->is('post')) {
			//echo pr($this->request['data']);
			if (isset($this->request['data']['Changeview']['test_controller_action'])) {
				$temp = explode("___", $this->request['data']['Changeview']['test_controller_action']);
				$test_controller = $temp[0];
				$test_action = $temp[1];
			}
			if (isset($this->request['data']['Changeview']['test_layout']))
				$this->layout = $this->request['data']['Changeview']['test_layout'];
			if (isset($this->request['data']['Changeview']['test_flash_message']) && $this->request['data']['Changeview']['test_flash_message'] != 0)
				$this->Session->setFlash("Essa é uma mensagem de teste!", $this->request['data']['Changeview']['test_flash_message']);
		}
		$this->set('title_for_layout', Inflector::humanize($test_controller) ." - ". Inflector::humanize($test_action));
		$this->render('/'. $test_controller .'/'. $test_action);
	}

}
