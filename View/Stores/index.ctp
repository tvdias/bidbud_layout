<div id="caminho" style="display: none">
	<p><?php echo $this->element('crumb'); ?></p>
	<div style="clear: both;"></div>
</div><!-- caminho -->

<div id="conteudoTitulo">
	<h2>Nossos Parceiros</h2>    
    <div style="clear: both;"></div>
</div><!-- conteudoTitulo -->

<div class="box">
<?php
if (!empty($stores)):
	//echo "<ul>";
	foreach ($stores as $store):
		$seller_img = $this->Image->store($store['Store']['id'], 'medium', array('clear' => true, 'alt' => "", 'style' => "margin: 20px;"));
		//echo "<li>". $html->link($seller_img, "/parceiros/".$store['User']['username'], array('class' => "underline s12"), false, false) ."</li>";
		echo $this->Html->link($seller_img, array('controller' => 'auctions', 'action' => 'store', $store['Store']['slug']), array('class' => "", 'escape' => false));
	endforeach;
	//echo "</ul>";
else: ?>
<div class="topBox">
	<div class="topBoxMSG">Desculpe</div>
	<p class="s16 cinza" style="position: absolute; left: 50px; top: 80px;">Ainda não existem parceiros cadastrados.</p>
	<div id="topBoxIMG"></div>
	<div class="no_spam"></div>
</div>
<?php endif; ?>
</div><!-- box -->

<?php echo $this->element('search_pagination'); ?>