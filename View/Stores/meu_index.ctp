<div id="caminho">
	<?php $this->Html->addCrumb('Meu BidBud', array('controller' => 'users', 'action' => 'index', $loggedUser['User']['id'])); ?>
	<p><?php echo $this->element('crumb'); ?></p>
	<div style="clear: both;"></div>
</div><!-- caminho -->

<div id="conteudoTitulo">
	<h2>Meus Parceiros</h2>    
    <div style="clear: both;"></div>
</div><!-- conteudoTitulo -->

<?php
if (!empty($stores)):
	echo "<div class=\"box\">";
		echo $this->Html->link("Leilões dos parceiros", array('meu' => true, 'controller' => 'auctions', 'action' => 'following', $loggedUser['User']['id']), array('class' => "", 'escape' => false));
		echo "<div style=\"clear: both;\"></div>";
		//echo "<ul>";
		foreach ($stores as $store):
			$seller_img = $this->Image->store($store['Store']['id'], 'medium', array('clear' => true, 'alt' => "", 'style' => "margin: 20px;"));
			//echo "<li>". $html->link($seller_img, "/parceiros/".$store['User']['username'], array('class' => "underline s12"), false, false) ."</li>";
			echo $this->Html->link($seller_img, array('controller' => 'auctions', 'action' => 'store', $store['Store']['slug'], 'meu' => false), 
array('class' => "", 'escape' => false));
		endforeach;
		//echo "</ul>";
	echo "</div><!-- box -->";
	echo $this->element('search_pagination');
else: ?>
<div class="topBox">
	<div class="topBoxMSG">Você ainda não tem nenhum parceiro favorito.</div>
	<div id="topBoxIMG"></div>
</div>
<?php endif; ?>
