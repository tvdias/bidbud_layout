<?php //echo $javascript->link('jquery.validate', array('inline' => false)); ?>
<?php echo $this->Html->script('http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js', array('inline' => false)); ?>

<?php echo $this->Html->scriptStart(array('inline' => false)); ?>
$(document).ready(function() {
	// validate signup form on keyup and submit
	var validator = $("#Activation").validate({
		rules: {
			"data[User][email]": "required",
			"data[User][activation_code]": "required"
		},
		messages: {
			//"data[User][email]": "<span>Por favor digite o seu e-mail de cadastro!</span>",
			//"data[User][activation_code]": "<span>Por favor digite o código de ativação!</span>"
			"data[User][email]": "Obrigatório!",
			"data[User][activation_code]": "Obrigatório!"
		},
		// the errorPlacement has to take the table layout into account
		errorPlacement: function(error, element) {
			//error.appendTo( element.parent() );
			//error.insertAfter( element );
			//error.insertAfter( element.prev() );
		},
        invalidHandler: function(e, validator) {
			var errors = validator.numberOfInvalids();
			if (errors) {
				var message = 'Todos os campos são obrigatórios!';
				$("p#ActivationFormErrorMsg").html(message);
			} else {
				$("p#ActivationFormErrorMsg").html('&nbsp;');
			}
		}
	}); //EOF Validation
});
<?php echo $this->Html->scriptEnd(); ?>

<style>
.error-message {
	margin-top: -10px;
	margin-bottom: 10px;
}
</style>

<div id="caminho">
    <?php //$html->addCrumb('Ativar Cadastro', '/ativacao'); ?>
	<p><?php echo $this->element('crumb'); ?></p>
	<div style="clear: both;"></div>
</div><!-- caminho -->

<div id="conteudoTitulo">
	<h2>Por favor ative sua a conta no ApetreXo Leilão</h2>
    <div style="clear: both;"></div>
</div><!-- conteudoTitulo -->

<div class="topBox" style="height: 400px;">
	<div class="topBoxMSG" style="left: 30px">Oops!</div>
	<p class="s14 cinza" style="position: absolute; left: 30px; top: 80px; width: 460px;">Sua conta foi criada mas ainda não foi ativada. Você poder clicar no link de ativação no email que enviamos ou completar o formulário abaixo com o código de ativação que também enviamos por email.</p>
	<div id="topBoxIMG"></div>
	<div style="position: absolute; top: 150px; left: 30px;">
		<?php echo $this->Form->create('User', array('id' => 'Activation', 'action' => 'activation_pre')); ?>
            <p class="passwordTXT">E-mail</p>
            <?php echo $this->Form->input('email', array('label'=> false, 'div' => false, 'class' => "password"));?>
            <p class="passwordTXT">Código de ativação</p>
            <?php echo $this->Form->input('activation_code', array('label'=> false, 'div' => false, 'class' => "password"));?>
            <p id="ActivationFormErrorMsg" style="clear:both; height:20px;">&nbsp;</p>
            <input type="submit" value="" class="enviar" />
        </form>
	</div>
</div>