<div id="caminho">
    <?php $this->Html->addCrumb('Cadastro Finalizado', '/cadastrofinalizado'); ?>
	<p><?php echo $this->element('crumb'); ?></p>
	<div style="clear: both;"></div>
</div><!-- caminho -->

<div id="conteudoTitulo">
	<h2>Parabéns!</h2>
    <div style="clear: both;"></div>
</div><!-- conteudoTitulo -->

<div class="topBox">
	<div class="topBoxMSG">Você acaba de se registrar no ApetreXo Leilão</div>
	<p class="s16 cinza" style="position: absolute; left: 50px; top: 60px; margin-right: 30px;">Você receberá um e-mail com instruções para ativar a sua conta.</p>
	<div id="topBoxIMG"></div>
	<div class="no_spam"></div>
</div>