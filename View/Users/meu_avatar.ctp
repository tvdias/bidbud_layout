<style>
.progressWrapper {
	width: 357px;
	overflow: hidden;
}

.progressContainer {
	margin: 5px;
	padding: 4px;
	border: solid 1px #E8E8E8;
	background-color: #F7F7F7;
	overflow: hidden;
}
/* Message */
.message {
	margin: 1em 0;
	padding: 10px 20px;
	border: solid 1px #FFDD99;
	background-color: #FFFFCC;
	overflow: hidden;
}
/* Error */
.red {
	border: solid 1px #B50000;
	background-color: #FFEBEB;
}

/* Current */
.green {
	border: solid 1px #DDF0DD;
	background-color: #EBFFEB;
}

/* Complete */
.blue {
	border: solid 1px #CEE2F2;
	background-color: #F0F5FF;
}

.progressName {
	font-size: 8pt;
	font-weight: 700;
	color: #555;
	width: 323px;
	height: 14px;
	text-align: left;
	white-space: nowrap;
	overflow: hidden;
}

.progressBarInProgress,
.progressBarComplete,
.progressBarError {
	font-size: 0;
	width: 0%;
	height: 2px;
	background-color: blue;
	margin-top: 2px;
}

.progressBarComplete {
	width: 100%;
	background-color: green;
	visibility: hidden;
}

.progressBarError {
	width: 100%;
	background-color: red;
	visibility: hidden;
}

.progressBarStatus {
	margin-top: 2px;
	width: 337px;
	font-size: 7pt;
	font-family: Arial;
	text-align: left;
	white-space: nowrap;
}

a.progressCancel {
	font-size: 0;
	display: block;
	height: 14px;
	width: 14px;
	/*background-image: url(../images/cancelbutton.gif);*/
	background-repeat: no-repeat;
	background-position: -14px 0px;
	float: right;
}

a.progressCancel:hover {
	background-position: 0px 0px;
}


/* -- SWFUpload Object Styles ------------------------------- */
.swfupload {
	vertical-align: top;
}
</style>

<?php
$this->Html->script('swfupload/swfupload', array('inline' => false));
//$this->Html->script('swfupload/account/fileprogress', array('inline' => false));
$this->Html->script('swfupload/handlers_avatar', array('inline' => false));

echo $this->Html->scriptStart(array('inline' => false));
?>
var swfu;
window.onload = function () {
	swfu = new SWFUpload({
		// Backend Settings
        upload_url: "<?php echo Router::url(array('admin' => false, 'meu' => false, 'controller' => 'images', 'action' => 'newupload')); ?>",
        post_params: {"PHPSESSID" : "<?php echo session_id(); ?>", "dir_to_save":"profile", "model":"Profile", "model_id":"<?php echo $loggedUser['User']['id']; ?>"},
		file_post_name: "Filedata",

		// File Upload Settings
		file_size_limit : "1 MB",
		file_types : "*.jpg;*.png;*.gif",
		file_types_description : "Somente imagens",
		file_upload_limit : "0",

		// Event Handler Settings - these functions as defined in Handlers.js
		//  The handlers are not part of SWFUpload but are part of my website and control how
		//  my website reacts to the SWFUpload events.
		file_queue_error_handler : fileQueueError,
		file_dialog_complete_handler : fileDialogComplete,
		upload_progress_handler : uploadProgress,
		upload_error_handler : uploadError,
		upload_success_handler : uploadSuccess,
		upload_complete_handler : uploadComplete,

		// Button Settings
		button_placeholder_id : "spanButtonPlaceholder",
		button_image_url : "/img/XPButtonUploadText_61x22.png",
		button_width: 61,
		button_height: 22,
		button_window_mode: SWFUpload.WINDOW_MODE.TRANSPARENT,
		button_cursor: SWFUpload.CURSOR.HAND,

		// Flash Settings
		flash_url : "/js/swfupload/swfupload.swf",

		custom_settings : {
    		upload_target : "divFileProgressContainer"
		},

		// Debug Settings
		debug: false
	});
};
<?php echo $this->Html->scriptEnd(); ?>

<div id="caminho">
    <?php $this->Html->addCrumb('Meu BidBud', '/meu-bidbud'); ?>
	<p><?php echo $this->element('crumb'); ?></p>
	<div style="clear: both;"></div>
</div><!-- caminho -->

<div id="conteudoTitulo">
	<h2>Editar Avatar</h2>
    <div style="clear: both;"></div>
</div><!-- conteudoTitulo -->

<div class="topBox">
	<?php echo $this->Form->create(null, array('url' => array('controller' => 'users', 'action' => 'avatar')));?>
		<style type="text/css">td {padding: 5px 0px 5px 0px;}</style>
		<table cellspacing="0" style="width: 100%; padding: 20px;">
			<tbody>
			<tr>
				<td class="s14 bold cinza">Alterar avatar</td>
			</tr>
			<tr>
				<td class="s14 bold cinza">
					<div class="imginside" style="margin-left:20px"><?php echo $this->Image->avatar_logged_user("medium", array(), false); ?></div>
					<div style="clear:both">&nbsp;</div>
                    <span id="spanButtonPlaceholder"></span>
					<div style="clear:both">&nbsp;</div>
                    <div id="divFileProgressContainer" style="height: 75px;"></div>
				</td>
			</tr>
			</tbody>
		</table>
	<?php echo $this->Form->end();?>
</div><!-- box -->