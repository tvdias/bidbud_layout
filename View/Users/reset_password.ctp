<div id="caminho">
	<?php //$this->html->addCrumb('Alterar Senha', '/ativacao'); ?>
	<p><?php echo $this->element('crumb'); ?></p>
	<div style="clear: both;"></div>
</div><!-- caminho -->

<div id="conteudoTitulo">
	<h2>Alterar Senha</h2>
	<div style="clear: both;"></div>
</div><!-- conteudoTitulo -->

<div class="topBox">
	<?php echo $this->Form->create('User', array('url' => array('action' => 'reset_password', $code), 'id' => 'ChangingPassword', 'name' => 'changing password', 'style' => "position: absolute; left: 50px; top: 40px;")); ?>
		<p class="passwordTXT">Nova senha</p>
		<?php echo $this->Form->input('password', array('label' => false, 'div' => false, 'type' => 'password', 'class' => 'password', 'style' => "margin-bottom: 0px;"));?>
		<p class="passwordTXT" style="margin-top: 20px;">Confirme sua senha</p>
		<?php echo $this->Form->input('password_confirm', array('label' => false, 'div' => false, 'type' => 'password', 'class' => 'password', 'style' => "margin-bottom: 0px;"));?>
		<input type="submit" value="" style="margin-top: 30px;" class="enviar" />
	</form>
	<div id="topBoxIMG"></div>
</div>