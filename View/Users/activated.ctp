<div id="caminho">
	<?php //$this->Html->addCrumb('Conta Ativada', '/ativado'); ?>
	<p><?php echo $this->element('crumb'); ?></p>
	<div style="clear: both;"></div>
</div><!-- caminho -->

<div id="conteudoTitulo">
	<h2>Parabéns!</h2>
	<div style="clear: both;"></div>
</div><!-- conteudoTitulo -->

<div class="topBox">
	<div class="topBoxMSG" style="left:30px;">Você acaba de ativar a sua conta no ApetreXo Leilão</div>
	<p class="s16 cinza" style="position: absolute; left: 30px; top: 70px;">Agora você já pode participar de todos os leilões do ApetreXo Leilão<br />
		e arrematar os produtos dos seus sonhos!</p>
	<div id="topBoxIMG"></div>
</div>