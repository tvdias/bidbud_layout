<div id="caminho">
	<?php //$html->addCrumb('Reenvio de E-mail de Ativação', '/ativacao'); ?>
	<p><?php echo $this->element('crumb'); ?></p>
	<div style="clear: both;"></div>
</div><!-- caminho -->

<div id="conteudoTitulo">
	<h2>Reenvio do E-mail de Ativação</h2>
	<div style="clear: both;"></div>
</div><!-- conteudoTitulo -->

<div class="topBox">
	<p class="s14 cinza" style="position: absolute; left: 30px; top: 30px; width: 460px;">Preencha o formulário abaixo para solicitar o reenvio do e-mail de ativação.</p>
	<?php echo $this->Form->create('User', array('action' => 'request_activation', 'style' => "position: absolute; left: 50px; top: 160px;")); ?>
		<p class="passwordTXT">E-mail</p>
		<?php echo $this->Form->input('User.email', array('label' => false, 'div' => false, 'class' => "inputText", 'style' => "width: 300px")); ?>
		<input type="submit" value="" class="enviar" style="position: absolute; left: 0px; top: 85px;" />
	<?php echo $this->Form->end(); ?>
</div>