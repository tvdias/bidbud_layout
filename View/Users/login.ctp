<?php
$this->Html->script(array('jquery.validate', 'jquery.form'), array('inline' => false));
//echo $session->flash();//All error messages output in every view
?>

<div id="conteudoTitulo">
    <h2>Faça o seu login para entrar no sistema</h2>
    <div style="clear: both;"></div>
</div>

<div class="box"> 
	<?php echo $this->Form->create('User', array('id' => 'Login', 'name' => 'Login', 'action' => 'login'));?>
		<div class="blueHeader s12">
			<span style="  display: inline-block; text-align: center;   padding-left: 10px;">Login</span>
		</div>
        <?php echo $this->Session->flash('auth'); ?>
		<style type="text/css">td {padding: 5px 0px 5px 0px;}</style>
		<table cellspacing="0" style="width: 100%; padding: 20px;">
			<tbody>
			<tr>
				<td class="s14 bold cinza">Usuário</td>
				<td class="s14 bold cinza">Senha</td>
			</tr>
			<tr>
				<td class="s14 bold cinza"><?php echo $this->Form->input('User.username', array('id' => 'userLogin', 'label' => false, 'div' => false, 'class' => "inputText", 'style' => "width: 200px;")); ?></td>
				<td class="s14 bold cinza"><?php echo $this->Form->input('User.password', array('id' => 'userpassword', 'type' => 'password', 'label'=> false, 'div' => false, 'class' => "inputText", 'style' => "width: 200px;")); ?> </td>
			</tr>
			<tr>
				<td class="s14 bold cinza" colspan="2"><input type="submit" value="" class="confirmar" style="margin-left:250px" /></td>
			</tr>
			<?php /*
			<tr>
				<td class="s14 bold cinza" colspan="2">
					<php echo $this->Html->link("Esqueceu sua senha?", array('controller' => 'users', 'action' => "forgot_password"), array('escape' => false, 'style' => 'text-decoration:underline; color: #41b1e3; font-size:12px; position: absolute; top: 170px; left: 105px;')); php> | 
					<php echo $this->Html->link("Não recebeu o e-mail de cofirmação de cadastro?", array('controller' => 'users', 'action' => "request_activation"), array('escape' => false, 'style' => 'text-decoration:underline; color: #41b1e3; font-size:12px; position: absolute; top: 170px; left: 105px;')); php>
				</td>
			</tr>
			*/ ?>
			</tbody>
		</table>
        <?php //echo $form->hidden('referer', array('value' => $referer)); ?>
	</form>
</div>
<script type="text/javascript">
    $("#Login").validate({
        rules: {
            "data[User][email]": "required",
            "data[User][password]": {
                required: true
            }
        }
    });
    
</script>