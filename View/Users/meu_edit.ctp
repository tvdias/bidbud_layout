<?php //echo $javascript->link('jquery.validate', array('inline' => false)); ?>
<?php echo $this->Html->script('http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js', array('inline' => false)); ?>

<?php echo $this->Html->scriptStart(array('inline' => false)); ?>
jQuery.validator.addMethod("notEqual", function(value, element, param) {
  return this.optional(element) || value != $(param).val();
}, "Please specify a different value");

$(document).ready(function() {
	// validate signup form on keyup and submit
	var validator = $("#UserMeuEditForm").validate({
		rules: {
			"data[User][password]": { required: true, rangelength: [6, 12], notEqual: "#UserPasswordOld" },
			"data[User][password_confirm]": { required: true, rangelength: [6, 12], equalTo: "#UserPassword" },
			"data[User][password_old]": { required: true, rangelength: [6, 12] },
		},
		messages: {
			"data[User][password]": '<div class="error-message">Escolha uma senha diferente da atual com 6 a 12 caracteres</div>',
			"data[User][password_confirm]": '<div class="error-message">Confirme sua nova senha</div>',
			"data[User][password_old]": '<div class="error-message">Digite sua senha atual</div>'
		},
		// the errorPlacement has to take the table layout into account
		errorPlacement: function(error, element) {
			error.appendTo( element.parent() );
			//error.insertAfter( element );
			//error.insertAfter( element.prev() );
		},
        invalidHandler: function(e, validator) {
			var errors = validator.numberOfInvalids();
			if (errors) {
				alert('Verifique as mensagens de erro no formulário.');
			}
		}
	}); //EOF Validation
});
<?php echo $this->Html->scriptEnd(); ?>

<div id="caminho">
    <?php $this->Html->addCrumb('Meu Cadastro', '/meu-bidbud'); ?>
	<p><?php echo $this->element('crumb'); ?></p>
	<div style="clear: both;"></div>
</div><!-- caminho -->

<div id="conteudoTitulo" style="display:none;">
	<h2>Editar Informações Pessoais</h2>
    <div style="clear: both;"></div>
</div><!-- conteudoTitulo -->

<div class="topBox" style="height:160px; margin-bottom:40px; display:none;">
	<div class="topBoxMSG" style="top: 30px; left:30px;">Atenção:</div>
	<p class="s16 cinza" style="position: absolute; left: 30px; top: 60px; margin-right: 30px;">Para a sua segurança, qualquer alteração nos seus dados cadastrais deve ser solicitada pelo endereço <a href="mailto:sac@bidbud.com.br">sac@apetrexoleilao.com.br</a>. A mensagem deve ser enviada <u>do seu e-mail de cadastro</u> e deve ser <u>informado o seu login, CEP e CPF</u>.</p>
</div>

<div class="box">
	<div class="blueHeader s12">
		<span style="display: inline-block; text-align: center; padding-left: 10px;">EDITAR INFORMAÇÕES PESSOAIS</span>
	</div>
	<div style="padding: 20px 20px 0px 20px; margin-bottom:20px;">
		<p class="s16 cinza" style="margin-bottom: 30px;">Para a sua segurança, qualquer alteração nos seus dados cadastrais deve ser solicitada pelo endereço <a href="mailto:sac@apetrexoleilao.com.br">sac@apetrexoleilao.com.br</a>. A mensagem deve ser enviada <u>do seu e-mail de cadastro</u> e deve ser <u>informado o seu login, CEP e CPF</u>. Para a troca de senha, utilize o formulário abaixo.</p>
	</div>
</div>

<div class="box">
	<div class="blueHeader s12">
		<span style="display: inline-block; text-align: center; padding-left: 10px;">ALTERAR SENHA</span>
	</div>
	<div style="padding: 20px 20px 0px 20px; margin-bottom:20px;">
		<?php
			echo $this->Form->create('User');
			//echo $this->Form->hidden('User.id');
		?>
		<table cellspacing="0" style="width: 100%;">
			<tbody>
			<tr>
				<td class="s14 bold cinza">Senha Atual</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr style="height: 60px;">
				<td valign="top" class="s14 bold cinza"><?php echo $this->Form->input('password_old', array('type' => 'password', 'label'=> false, 'div' => false, 'class' => "inputText", 'style' => "width: 188px; margin-right: 30px", "MAXLENGTH" => 12)); ?> </td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td class="s14 bold cinza">Nova Senha <span class="normal s12">(entre 6 e 12 caracteres)</span></td>
				<td class="s14 bold cinza">Confirmação de senha</td>
				<td>&nbsp;</td>
			</tr>
			<tr style="height: 60px;">
				<td valign="top" class="s14 bold cinza"><?php echo $this->Form->input('password', array('type' => 'password', 'label'=> false, 'div' => false, 'class' => "inputText", 'style' => "width: 188px; margin-right: 30px", "MAXLENGTH" => 12)); ?> </td>
				<td valign="top" class="s14 bold cinza"><?php echo $this->Form->input('password_confirm', array('type' => 'password', 'label' => false, 'div' => false, 'class' => "inputText", 'style' => "width: 188px;", "MAXLENGTH" => 12)); ?> </td>
				<td valign="top"><input type="submit" value="" class="enviar" /></td>
			</tr>
			</tbody>
		</table>
		<?php echo $this->Form->end(); ?>
	</div>
</div>

<div class="box">
	<div class="blueHeader s12">
		<span style="display: inline-block; text-align: center; padding-left: 10px;">RECEBER E-MAILS PROMOCIONAIS</span>
	</div>
	<div style="padding: 20px 20px 0px 20px; margin-bottom:20px;">
		<?php
			echo $this->Form->create('User', array('url' => array('action' => "receive_newsletters", 'meu' => true)));
			//echo $this->Form->hidden('Profile.id');
		?>
		<table cellspacing="0" style="width: 100%;">
			<tr style="height: 30px;">
				<td valign="top" class="s14 bold cinza"><?php echo $this->Form->input('Profile.receive_newsletter', array('type' => 'checkbox', 'label'=> "Sim", 'div' => false, 'style' => "width: 30px")); ?> </td>
			</tr>
			<tr>
				<td valign="top"><input type="submit" value="" class="enviar" /></td>
			</tr>
			</tbody>
		</table>
		<?php echo $this->Form->end(); ?>
	</div>
</div>