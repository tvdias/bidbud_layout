<?php
$this->Html->script(array('swfupload/swfupload.js', 'swfupload_registration/fileprogress.js', 'swfupload_registration/activation_handlers.js'), array('inline' => false));

echo $this->Html->scriptStart(array('inline' => false));
?>
var swfu;

window.onload = function () {
	swfu = new SWFUpload({
		// Backend settings
		upload_url: "<?php echo Router::url(array('controller' => 'images', 'action' => 'newupload')); ?>",
		post_params: {"PHPSESSID" : "<?php echo session_id(); ?>", "userAgent":"<?php echo $this->Session->read('Config.userAgent'); ?>", "dir_to_save":"profile", "model":"Profile", "model_id":"<?php echo $this->data['Profile']['id'];?>"},
		file_post_name: "Filedata",
		
		// Flash file settings
		file_size_limit : "2 MB",
		file_types : "*.jpg;*.png;*.gif",
		file_types_description : "All Images",
		file_upload_limit : "0",
		file_queue_limit : "1",
		
		// Event handler settings
		//swfupload_loaded_handler : swfUploadLoaded,
		
		file_dialog_start_handler: fileDialogStart,
		file_queued_handler : fileQueued,
		file_queue_error_handler : fileQueueError,
		file_dialog_complete_handler : fileDialogComplete,
		
		//upload_start_handler : uploadStart,// I could do some client/JavaScript validation here, but I don't need to.
		upload_progress_handler : uploadProgress,
		upload_error_handler : uploadError,
		upload_success_handler : uploadSuccess,
		upload_complete_handler : uploadComplete,
		
		// Button Settings
		//button_image_url : "/img/XPButtonUploadText_61x22.png",
		button_image_url : "https://c14985174.ssl.cf2.rackcdn.com/img/btn_selecionar.png",
		button_placeholder_id : "spanButtonPlaceholder",
		button_width: 120,
		button_height: 32,
		
		// Flash Settings
		flash_url : "/js/swfupload/swfupload.swf",
		
		custom_settings : {
		progress_target : "fsUploadProgress",
		upload_successful : false
		},
		
		// Debug settings
		debug: false
	});
};



function onlyNumbers(evt)
{
	var e = event || evt; // for trans-browser compatibility
	var charCode = e.which || e.keyCode;

	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;

	return true;
}

function check() {
	var all_checked = $('[type="checkbox"]:checked').length > 4;
	if (all_checked) {
		//$('#confirmar').attr('type', "submit");
		$('#confirmar').attr('class', "confirmar");
		$('#registration_confirm').hide();
		
		return true;
	}
	else {
		//$('#confirmar').attr('type', "button");
		$('#confirmar').attr('class', "confirmar_disabled");
		$('#registration_confirm').show();
	}
	
	return false;
}

function checkSubmit() {
	if(!check()) {
	$("#UserCheck1").focus();
		alert("Você precisa concordar com os termos de uso do site.");
		return false;
	}
	return true;
}
$(document).ready(function(){
	$("#zip").blur(function() {
		$typed = $("#zip").val();
		if ($typed.length > 7) {
			$.get('/users/get_address/'+$typed,
				function(data) {
					if(data.state == undefined || $.trim(data.state).length == 0)
					{
						alert("Não conseguimos identificar o seu CEP. Antes de prosseguir com o cadastro verifique se ele foi digitado corretamente.");
					}
					$("#address").val(data.street);
					$("#neighborhood").val(data.neighborhood);
					$("#city").val(data.city);
					$("#state").val(data.state);
					$("#number").focus();
				},
				"json"
			);
			return;
		}
	});
	check();
	$('[type="checkbox"]').change(function() {
		check();
	});
});
<?php
echo $this->Html->scriptEnd();

echo $this->Form->create('User', array( 'id' => 'Registration', 'name' => 'Registration', 'url' => array('controller' => "users", 'action' => "activation"), 'onsubmit' => 'return checkSubmit();'));
echo $this->Form->hidden('User.id');
echo $this->Form->hidden('Profile.id');
echo $this->Form->hidden('User.referer');
echo $this->Form->hidden('User.receive_newsletter');
?>
<style type="text/css">td {padding: 5px 0px; vertical-align: top;}</style>

<?php
//http://www.sohtanaka.com/web-design/inline-modal-window-w-css-and-jquery/


/*
JS
//When you click on a link with class of poplight and the href starts with a # 
$('a.poplight[href^=#]').click(function() {
var popID = $(this).attr('rel'); //Get Popup Name
var popURL = $(this).attr('href'); //Get Popup href to define size

//Pull Query & Variables from href URL
var query= popURL.split('?');
var dim= query[1].split('&');
var popWidth = dim[0].split('=')[1]; //Gets the first query string value

//Fade in the Popup and add close button
$('#' + popID).fadeIn().css({ 'width': Number( popWidth ) }).prepend('<a href="#" class="close"><?php echo $this->Html->image("images_tvd/popup_close.png", array('class' => "btn_close", 'title' => "Close Window", 'alt' => "Close")); ?></a>');

//Define margin for center alignment (vertical horizontal) - we add 80px to the height/width to accomodate for the paddingand border width defined in the css
var popMargTop = ($('#' + popID).height() + 80) / 2;
var popMargLeft = ($('#' + popID).width() + 80) / 2;

//Apply Margin to Popup
$('#' + popID).css({
'margin-top' : -popMargTop,
'margin-left' : -popMargLeft
});

//Fade in Background
$('body').append('<div id="fade"></div>'); //Add the fade layer to bottom of the body tag.
$('#fade').css({'filter' : 'alpha(opacity=80)'}).fadeIn(); //Fade in the fade layer - .css({'filter' : 'alpha(opacity=80)'}) is used to fix the IE Bug on fading transparencies 

return false;
});

//Close Popups and Fade Layer
$('a.close, #fade').live('click', function() { //When clicking on the close or fade layer...
$('#fade , .popup_block').fadeOut(function() {
$('#fade, a.close').remove();//fade them both out
});
return false;
});

<style>
#fade { /*--Transparent background layer--*--/
	display: none; /*--hidden by default--*--/
	background: #000;
	position: fixed; left: 0; top: 0;
	width: 100%; height: 100%;
	opacity: .80;
	z-index: 9999;
}
.popup_block{
	display: none; /*--hidden by default--*--/
	background: #fff;
	padding: 20px;
	border: 20px solid #ddd;
	float: left;
	font-size: 1.2em;
	position: fixed;
	top: 50%; left: 50%;
	z-index: 99999;
	/*--CSS3 Box Shadows--*--/
	-webkit-box-shadow: 0px 0px 20px #000;
	-moz-box-shadow: 0px 0px 20px #000;
	box-shadow: 0px 0px 20px #000;
	/*--CSS3 Rounded Corners--*--/
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
}
img.btn_close {
	float: right;
	margin: -55px -55px 0 0;
}
/*--Making IE6 Understand Fixed Positioning--*--/
*html #fade {
	position: absolute;
}
*html .popup_block {
	position: absolute;
}
</style>
<div id="popup_name" class="popup_block" style="width: 500px; margin-top: -159px; margin-left: -290px; display: none;">
<table cellspacing="0" style="width: 100%; padding: 20px;">
<tbody>
<tr>
<td class="s14 bold cinza">Enviar imagem </td>
</tr>
<tr>
<td class="s14 bold cinza">
<input type="text" id="txtFileName2" disabled="disabled" />
<span id="spanButtonPlaceholder2"></span>
<div class="flash" id="fsUploadProgress2">
<!-- This is where the file progress gets shown.SWFUpload doesn't update the UI directly.
The Handlers (in handlers.js) process the upload events and make the UI updates -->
</div>
<input type="hidden" name="hidFileID2" id="hidFileID2" value="" />
<!-- This is where the file ID is stored after SWFUpload uploads the file and gets the ID back from upload.php -->
</td>
</tr>
<tr>
<td class="s14 bold cinza"><div class="imginside" style="margin-left:20px"><?php echo $this->Image->avatar($this->data['Profile']['user_id'], "medium", array(), false); ?></div></td>
</tr>
</tbody>
</table>
</div>
*/
?>

<div class="box">
	<div class="blueHeader s12">
		<span style="display: inline-block; text-align: center; padding-left: 10px;">INFORMAÇÕES DE ACESSO</span>
	</div>
	<div style="padding: 20px 20px 0px 20px;">
		<table cellspacing="0" style="width: 100%;">
			<tbody>
			<tr>
				<td class="s14 bold cinza">Usuário <span class="normal s12">(máx. de 16 caracteres)</span></td>
				<td class="s14 bold cinza">Senha <span class="normal s12">(entre 6 e 12 caracteres)</span></td>
				<td class="s14 bold cinza">Confirmação de senha</td>
			</tr>
			<tr style="height: 60px;">
				<td class="s14 bold cinza"><?php echo $this->Form->input('username', array('label' => false, 'div' => false, 'class' => "inputText", 'style' => "width: 188px; margin-right: 30px")); ?> </td>
				<td class="s14 bold cinza"><?php echo $this->Form->input('password', array('type' => 'password', 'label'=> false, 'div' => false, 'class' => "inputText", 'style' => "width: 188px; margin-right: 30px", "MAXLENGTH" => 12)); ?> </td>
				<td class="s14 bold cinza"><?php echo $this->Form->input('password_confirm', array('type' => 'password', 'label' => false, 'div' => false, 'class' => "inputText", 'style' => "width: 188px;", "MAXLENGTH" => 12)); ?> </td>
			</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="box">
	<div class="blueHeader s12">
 		<span style="display: inline-block; text-align: center; padding-left: 10px;">ENDEREÇO</span>
	</div>
	<div style="padding: 20px 20px 0px 20px;">
		<table cellspacing="0" style="width: 100%;">
			<tr>
				<td class="s14 bold cinza">CEP <span class="normal s12">(somente números)</span></td>
				<td class="s14 bold cinza">Logradouro</td>
			</tr>
			<tr style="height: 60px;">
				<td class="s14 bold cinza"><?php echo $this->Form->input('Profile.address_zip', array('id' => "zip", 'label' => false, 'div' => false, 'maxlength' => 8, 'class' => "inputText", 'style' => "width: 150px; margin-right: 30px;", 'onkeypress' => "return onlyNumbers();")); ?></td>
				<td class="s14 bold cinza"><?php echo $this->Form->input('Profile.address', array('id' => "address", 'label' => false, 'div' => false, 'class' => "inputText", 'style' => "width: 456px;")); ?> </td>
			</tr>
		</table>
		<table cellspacing="0" style="width: 100%;">
			<tr>
				<td class="s14 bold cinza">Número</td>
				<td class="s14 bold cinza">Complemento</td>
				<td class="s14 bold cinza">Bairro</td>
			</tr>
			<tr style="height: 60px;">
				<td class="s14 bold cinza"><?php echo $this->Form->input('Profile.address_number', array('label' => false, 'div' => false, 'id' => 'number', 'class' => "inputText", 'style' => "width: 70px; margin-right: 30px;", 'onkeypress' => "return onlyNumbers();")); ?></td>
				<td class="s14 bold cinza"><?php echo $this->Form->input('Profile.address_compl', array('label' => false, 'div' => false, 'class' => "inputText", 'style' => "width: 250px; margin-right: 30px;")); ?> </td>
				<td class="s14 bold cinza"><?php echo $this->Form->input('Profile.address_neighborhood', array('id' => "neighborhood", 'label' => false, 'div' => false, 'class' => "inputText", 'style' => "width: 244px;")); ?></td>
			</tr>
			<tr>
				<td colspan="2" class="s14 bold cinza" style="width: 195px;">Cidade</td>
				<td class="s14 bold cinza" style="width: 170px;">Estado</td>
			</tr>
			<tr style="height: 60px;">
				<td colspan="2" class="s14 bold cinza"><?php echo $this->Form->input('Profile.address_city', array('id' => "city", 'label' => false, 'div' => false, 'class' => "inputText", 'style' => "width: 362px; margin-right: 30px;")); ?> </td>
				<td class="s14 bold cinza" style="width: 308px;">
				<?php
				$states =array("AC"=>"Acre", "AL"=>"Alagoas", "AM"=>"Amazonas", "AP"=>"Amapá","BA"=>"Bahia","CE"=>"Ceará","DF"=>"Distrito Federal","ES"=>"Espírito Santo","GO"=>"Goiás","MA"=>"Maranhão","MT"=>"Mato Grosso","MS"=>"Mato Grosso do Sul","MG"=>"Minas Gerais","PA"=>"Pará","PB"=>"Paraíba","PR"=>"Paraná","PE"=>"Pernambuco","PI"=>"Piauí","RJ"=>"Rio de Janeiro","RN"=>"Rio Grande do Norte","RO"=>"Rondônia","RS"=>"Rio Grande do Sul","RR"=>"Roraima","SC"=>"Santa Catarina","SE"=>"Sergipe","SP"=>"São Paulo","TO"=>"Tocantins");
				echo $this->Form->select('Profile.address_state', $states, array('id' => "state", 'label' => false, 'div' => false, 'class' => "inputText2", 'style' => "width: 254px;"), false); ?>
				</td>
			</tr>
		</table>
	</div>
</div>
<div class="box">
	<div class="blueHeader s12">
		<span style="display: inline-block; text-align: center; padding-left: 10px;">INFORMAÇÕES COMPLEMENTARES</span>
	</div>
	<div style="padding: 20px 20px 0px 20px;">
		<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr>
				<td><span class="s14 bold cinza">Avatar <span class="normal s12">(opcional)</span></span></td>
				<td>&nbsp;</td>
				<td colspan="2"><span class="s14 bold cinza">Celular</span></td>
				<td><span class="s14 bold cinza">Data Nascimento</span></td>
			</tr>
			<tr>
				<td rowspan="5" align="center">
					<div id="avatar" class="imginside" style="width: 130px; height: 130px; margin-bottom: 20px;">
						<?php echo $this->Image->avatar($this->request->data['User']['id'], 'medium', array('style' => "width: 130px; height: 130px;")); ?>
					</div>
					<div style="background: url(https://c14985174.ssl.cf2.rackcdn.com/img/btn_selecionar.png); width: 120px; height:32px; display: none;"></div>
					<a href="#?w=500" rel="popup_name" class="poplight" style="background: url(https://c14985174.ssl.cf2.rackcdn.com/img/btn_selecionar.png); width: 120px; height:32px; display: none;">Learn More</a>
					<span id="spanButtonPlaceholder"></span>
					<div class="flash" id="fsUploadProgress"></div>
				</td>
				<td rowspan="5" style="width: 20px;"></td>
				<td style="height: 40px;"><?php echo $this->Form->input('Profile.phone_code1', array('label' => false, 'div' => false, 'class' => "inputText2", 'style' => "width: 40px; text-align: center; margin-right: 10px;", 'maxlength' => 2, 'onkeypress' => "return onlyNumbers();")); ?></td>
				<td><?php echo $this->Form->input('Profile.phone1', array('label' => false, 'div' => false, 'class' => "inputText2", 'style' => "width: 110px; margin-right: 20px;", 'maxlength' => 8, 'onkeypress' => "return onlyNumbers();")); ?></td>
				<td><?php echo $this->Form->input('Profile.birthdate', array('dateFormat' => 'DMY', 'minYear' => date('Y') - 100, 'maxYear' => date('Y') - 18, 'monthNames' => Configure::read('monthFullNames'), 'separator' => '', 'empty' => true, 'label' => false, 'div' => false, 'class' => "inputText2")); ?></td>
			</tr>
			<tr>
				<td colspan="2"><span class="s14 bold cinza">Telefone <span class="normal s12">(opcional)</span></span></td>
				<td><span class="s14 bold cinza">Sexo</span></td>
			</tr>
			<tr>
				<td style="height: 40px;"><?php echo $this->Form->input('Profile.phone_code2', array('label' => false, 'div' => false, 'class' => "inputText2", 'style' => "width: 40px; text-align: center; margin-right: 10px;", 'maxlength' => 2, 'onkeypress' => "return onlyNumbers();")); ?></td>
				<td><?php echo $this->Form->input('Profile.phone2', array('label' => false, 'div' => false, 'class' => "inputText2", 'style' => "width: 110px; margin-right: 20px;", 'maxlength' => 8, 'onkeypress' => "return onlyNumbers();")); ?></td>
				<td><?php echo $this->Form->input('Profile.is_male', array('type' => 'select', 'options' => array('1' => 'Masculino', '0' => 'Feminino'), 'empty' => '', 'label' => false, 'div' => false, 'class' => "inputText2", 'style' => "width: 317px;")); ?></td>
			</tr>
			<tr>
				<td colspan="3"><span class="s14 cinza bold">Como conheceu o ApetreXo Leilão?</span></td>
			</tr>
			<tr>
				<td colspan="3" style="height: 60px;">
					<?php
					$inds = array(
						'amigo' => "Com Amigos",
						'google' => "No Google",
						'twitter' => "No Twitter",
						'facebook' => "No Facebook",
						'internet' => "Na Internet",
						'trabalho' => "No trabalho",
						'tv' => "Na TV",
						'jornal' => "No Jornal",
						'outros' => "Outros"
					);
					echo $this->Form->input('Profile.indication', array('type' => 'select', 'options' => $inds, 'empty' => '', 'label' => false, 'div' => false, 'class' => "inputText2", 'style' => "width: 521px;"));
					?>
				</td>
			</tr>
		</table>
	</div>
</div>
<div class="box">
	<div class="blueHeader s12">
		<span style="display: inline-block; text-align: center; padding-left: 10px;">QUESTÕES LEGAIS</span>
	</div>
	<table cellspacing="0" style="padding: 20px;">
		<tr>
			<td valign="top" style="width: 100px; ">
				<?php echo $this->Form->checkbox('check2', array('label' => false, 'div' => false, 'style' => "width: 20px;", 'checked' => 'checked')); ?>
				 <span class="boldBlack" style="display: inline;">SIM</span>
			</td>
			<td valign="top" class="cinza s12">Tenho ciência que há a possibilidade de gastar tempo e Lances e, mesmo assim, não arrematar o leilão.</td valign="top">
		</tr> 
		<tr>
			<td valign="top" style="width: 100px; ">
				<?php echo $this->Form->checkbox('check3', array('label' => false, 'div' => false, 'style' => "width: 20px;", 'checked' => 'checked')); ?>
 				<span class="boldBlack" style="display: inline;">SIM</span>
			</td>
			<td valign="top" class="cinza s12">Tenho ciência que se arrematar um leilão terei que pagar o valor do arremete e frete, conforme as regras do mesmo.</td valign="top">
		</tr>
		<tr>
			<td valign="top" style="width: 100px; ">
				<?php echo $this->Form->checkbox('check4', array('label' => false, 'div' => false, 'style' => "width: 20px;", 'checked' => 'checked')); ?>
				<span class="boldBlack" style="display: inline;">SIM</span>
			</td>
			<td valign="top" class="cinza s12">
				Tenho ciência que o ambiente de internet pode estar sujeito a falhas de acesso à internet, links, e outros fatores que escapam ao controle do ApetreXo Leilão que, eventualmente, podem afetar os leilões. Nesse caso, o leilão poderá ser suspenso e reiniciado quando possível.
			</td>
		</tr>
		<tr>
			<td valign="top" style="width: 100px; ">
				<?php echo $this->Form->checkbox('check5', array('label' => false, 'div' => false, 'style' => "width: 20px;", 'checked' => 'checked')); ?>
				<span class="boldBlack" style="display: inline;">SIM</span>
			</td>
			<td valign="top" class="cinza s12">
				Tenho ciência que, para manter a segurança dos usuários do ApetreXo Leilão, meus dados cadastrais podem ser verificados. A inconsistência das informações prestadas poderá acarretar no cancelamento do cadastro.
			</td>
		</tr>
		<tr>
			<td valign="top" style="width: 100px; ">
				<?php echo $this->Form->checkbox('check1', array('label' => false, 'div' => false, 'style' => "width: 20px;")); ?>
				<span class="boldBlack" style="display: inline;">SIM</span>
			</td>
			<td valign="top" class="cinza s12">
				Li, entendi e concordo com todo o disposto nos <?php echo $this->Html->link("Termos e Condições de Uso", array('controller' => 'pages', 'action' => 'display', 'agreement', 'meu' => false), array());?> do site Apetrexo Leilão.
			</td>
		</tr>
	</table>
</div>
<input id="confirmar" type="submit" value="" class="confirmar_disabled" style="margin-left: 250px;" />
<div id="registration_confirm" style="color:#C00; font-size: 12px; text-align: center; width: 100%;">Você precisa concordar com todas as questões legais do cadastro para prosseguir.</div> 
<?php
echo $this->Form->end();
//echo pr($this->validationErrors);
//echo pr($this->data);
?>