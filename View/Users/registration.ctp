<?php
//$javascript->codeBlock(null, array('inline' => false));
echo $this->Html->scriptStart(array('inline' => false));
?>
function onlyNumbers(evt)
{
	var e = event || evt; // for trans-browser compatibility
	var charCode = e.which || e.keyCode;

	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;

	return true;
}
<?php
//$javascript->blockEnd();
echo $this->Html->scriptEnd();
?>

<div id="caminho">
  <?php //$this->html->addCrumb('Cadastro', '/cadastro'); ?>
  <p><?php echo $this->element('crumb'); ?></p>
  <div style="clear: both;"></div>
</div><!-- caminho -->

<div id="conteudoTitulo">
	<h2>Cadastre-se agora e ganhe 5 lances!</h2>
  <p class="s14" style="color: black;">Depois, escolha o seu pacote preferido de Lances e tenha ainda mais chances nos nossos leilões!</p>
  <div style="clear: both;"></div>
</div><!-- conteudoTitulo -->

<?php
$referer_partner = CakeSession::read('partner');
if (!empty($referer_partner) ):
	switch($referer_partner) {
		case "convitevip":
			echo "<div class=\"box\" style=\"text-align:center; min-height: 50px; vertical-align:middle;\">";
			echo "<p style=\"color: red\">Olá, você é nosso convidado VIP. Fazendo o seu cadastro agora você ganhará mais 15 BIDs.</p>";
			echo "</div>";
			break;
		case "cadastrofb":
?>
	<div class="topBox" style="height: 250px; top: 0px; margin-bottom: 30px;">
		<div class="redefinirSenhaMSG" style="left: 20px; top: 20px;">Você é nosso convidado especial!</div>
		<p class="s16 cinza" style="position: absolute; left: 20px; top: 60px;">Fazendo o seu cadastro agora você ganhará mais 5 Lances.<br />
			São 10 Lances para você arrematar os melhores produtos!
		</p>
		<div id="topBoxIMG"></div>
        </div>
<?php
			break;
}
endif;

echo $this->Form->create('User', array( 'id' => 'RegistrationForm', 'url' => array('controller' => 'users', 'action' => 'registration')/*, 'onsubmit' => 'return confirm("Tenho ciência de que prestando informações inválidas poderei não receber meus produtos.");'*/));
?>
<div class="box" style="margin-bottom: 20px; padding-bottom: 10px;">
	<div class="blueHeader s12">
		<span style="display: inline-block; text-align: center; padding-left: 10px;">INFORMAÇÕES BÁSICAS</span>
	</div>
	<style type="text/css">td {padding: 5px 0px 5px 0px; vertical-align: top;}</style>
	<table cellspacing="0" style="width: 100%; padding: 0px 20px 20px 20px;">
	<tbody>
		<tr>
			<td class="s14 bold cinza">Nome</td>
		</tr>
		<tr>
			<td class="s14 bold cinza"><?php echo $this->Form->input('Profile.name', array('label' => false, 'div' => false, 'class' => "inputText", 'style' => "width: 200px;")); ?> </td>
		</tr>
		<tr>
			<td class="s14 bold cinza">Sobrenome</td>
		</tr>
		<tr>
			<td class="s14 bold cinza"><?php echo $this->Form->input('Profile.name_last', array('label' => false, 'div' => false, 'class' => "inputText", 'style' => "width: 200px;")); ?> </td>
		</tr>
		<tr>
			<td class="s14 bold cinza">CPF <span class="normal s12">(somente números)</span></td></td>
		</tr>
		<tr>
			<td class="s14 bold cinza"><?php echo $this->Form->input('Profile.cpf', array('label' => false, 'div' => false, 'class' => "inputText", 'style' => "width: 200px;", 'onkeypress' => "return onlyNumbers();")); ?> </td>
		</tr>
		<tr>
			<td class="s14 bold cinza">E-mail</td>
		</tr>
		<tr>
			<td class="s14 bold cinza"><?php echo $this->Form->input('email', array('label' => false, 'div' => false, 'class' => "inputText", 'style' => "width: 310px;")); ?></td>
		</tr>
		<tr>
			<td class="s14 bold cinza">Confirme seu e-mail</td>
		</tr>
		<tr>
			<td class="s14 bold cinza"><?php echo $this->Form->input('email_confirm', array('label' => false, 'div' => false, 'class' => "inputText", 'style' => "width: 310px;")); ?></td>
		</tr>
		<tr>
        	<td class="s14 bold" style="color: #EE6666">Ao enviar o seu cadastro TODAS as informações deverão estar corretas para evitar problemas de entrega. O ApetreXo Leilão não se responsabiliza pelos transtornos causador por problemas cadastrais.</td>
		</tr>
		<tr>
        	<td class="s14 bold cinza"><input id="confirmar" type="submit" value="" class="confirmar" /></td>
		</tr>
	</tbody>
	</table>
</div>
</form>