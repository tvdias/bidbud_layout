<div id="dialog" title="Novos Termos de Uso">
	Os Termos de Uso do BidBud foram atualizados. Clicando em "Aceito" ou acessando qualquer página do BidBud depois de entrar no sistema, você concorda com os novos termos dispostos <?php echo $this->Html->link("aqui", array('#'), array('target' => "_blank", 'style' => "text-decoration: underline;", 'onclick' => "$(\"#dialog-terms\").dialog(\"open\");	return false;"), false, false); ?>.
</div>
<div id="dialog-terms" title="Termos de Uso">
	<div class="box">

<p align="center"><strong><u>TERMOS E CONDIÇÕES GERAIS DE USO DO SITE APETREXO LEILÃO</u></strong></p>

<p><strong>BIDLIVRE SERVIÇOS DE INFORMÁTICA, ENTRETENIMENTO E PARTICIPAÇÕES LTDA.</strong>, sociedade limitada, proprietário do website: <a href="http://www.bidbud.com.br/">www.bidbud.com.br</a> ("BIDBUD"), estabelece, por meio do presente Termo e Condições Gerais de Uso ("Termo"), as condições para a utilização dos serviços e/ou aquisição de produtos, ou qualquer relação com o USUÁRIO, sendo certo que o presente Termo vigorará a partir do momento que o USUÁRIO preencher o formulário cadastral e clicar no seu campo "CONFIRMAR", o que significará plena aceitação e concordância com todos os termos e condições constantes do presente Termo, sem ressalvas:<br />
 <strong>1. OBJETO E FORMA UTILIZAÇÃO DO SITE</strong><br />
 1.1. O presente instrumento tem como objetivo estabelecer os termos e condições, política e normas para acesso ao site, compra de lances, prestação de serviços e demais condições gerais.<br />
 1.2. O website só poderá ser utilizado por pessoas jurídicas (na pessoa de seus representantes legais) ou pessoas físicas, maiores de idade e em sua plena capacidade ("USUÁRIO"), sendo proibido o cadastramento de pessoas que não observem os requisitos acima. São condições precedentes para a participação nos leilões, a realização do cadastro, leitura e aceitação dos termos e condições previstos no presente Termo. O USUÁRIO declara, sob as penas da lei, preencher todos os requisitos acima e que todos os dados fornecidos ao BIDBUD, para fins de cadastro, são verdadeiros. O USUÁRIO tem ciência que o fornecimento de dados incorretos ou incompletos, poderá acarretar em problemas na entrega do produto arrematado e/ou o cancelamento do cadastro.<br />
 1.2.1. Cada USUÁRIO terá o direito de utilizar apenas 1 (um) cadastro, sendo responsável pela sua guarda e da respectiva senha que são pessoais e intransferíveis, sendo terminantemente proibido o fornecimento dos dados a terceiros.<br />
 1.3. O USUÁRIO poderá escolher o seu próprio apelido ("Apelido") que aparecerá no momento do oferecimento dos lances, utilizando o bom senso e normas gerais de boa conduta, sendo terminantemente proibida a utilização de nomes preconceituosos, obscenos, agressivos ou que possa violar direitos de terceiros ou a dignidade. A utilização de nome que atente de forma direta ou indireta aos preceitos acima poderá acarretar no imediato cancelamento do cadastro pelo BIDBUD, sem prévio aviso e sem direito a restituição de quaisquer valores pagos para a aquisição de lances, além das cominações previstas na legislação brasileira.<br />
 <strong>1.3.1.</strong> O BIDBUD se reserva ao direito de solicitar que o USUÁRIO sugira um novo Apelido, caso verifique que o mesmo tem o objetivo de afetar o transcurso leilão, induzir outros USUÁRIOS a erro ou, ainda, se passar por outra pessoa.<br />
 1.4. O USUÁRIO autoriza o BIDBUD a tomar todas as medidas necessárias para, eventualmente, verificar as informações fornecidas pelo USUÁRIO.<br />
 <strong>1.5. </strong>O USUÁRIO autoriza o rastreamento de seu endereço IP ou a utilização de qualquer outro meio de controle com o fim único e específico de verificar a autenticidade e assegurar uma transação mais confiável e segura.<br />
 1.6. O USUÁRIO tem ciência de que deve tomar todas as medidas necessárias para proteger o seu computador, sendo certo que o BIDBUD protege as informações junto aos seus servidores, mas não pode assegurar a segurança durante o tráfego pela internet.<br />
 1.7. As informações fornecidas pelo USUÁRIO são armazenados em servidores com elevados padrões de segurança, sendo certo que o BIDBUD adota todas as medidas possíveis para manter a integridade e confidencialidade das informações, todavia não é responsável por prejuízos causados por terceiros que utilizem as redes públicas ou a internet para acessar as informações dos usuários.<br />
 1.8. É terminantemente proibida a modificação ou transmissão de qualquer conteúdo do site, modificado ou não, sem autorização por escrito do BIDBUD, ou a utilização para qualquer outro fim que não o de entretenimento e participação no leilão.<br />
 <strong>2. LEILÃO</strong><br />
 2.1. É condição precedente para a participação nos leilões do BIDBUD a aquisição de créditos que permitem dar lances ("BID" ou "BIDs"). O BID somente poderá ser adquirido no site BIDBUD, parceiros autorizados ou disponibilizado em promoções, conforme cláusula 2.2 abaixo. <br />
 2.1.1. O BID adquirido, seja por compra e/ou promoção, é pessoal e intransferível, não podendo ser cedido, negociado, transferido ou, ainda, realizar qualquer negócio que envolva o BID que não seja com o BIDBUD. A celebração de qualquer negócio com o BID ou transferência do mesmo será considerada nula de pleno direito.<br />
 2.2. <strong>Os BIDs terão validade de até 180 (cento e oitenta) dias contados da data de aquisição, salvo nos casos de pacotes promocionais que poderão ter seu prazo de validade reduzido, conforme as regras da respectiva promoção. Após transcorrido o prazo de validade os BIDs não utilizados serão automaticamente cancelados, sem outorgar direito a restituição de qualquer valor.</strong><br />
 2.2.1. O BIDBUD poderá disponibilizar promoções e descontos promocionais para a aquisição de BIDs, com condições específicas e por tempo determinado que constarão no regulamento da respectiva promoção. Os BIDs adquiridos com descontos promocionais poderão ter prazo de validade inferior ao prazo previsto na cláusula 2.2 acima, conforme as regras estipuladas na respectiva promoção.<br />
 2.2.2. Todos os BIDs, promocionais ou não, perderão a sua validade automaticamente no prazo estipulado na compra ou promoção, independente de prévia notificação. Nesse caso, os BIDs não poderão ser utilizados e tampouco o USUÁRIO terá direito a restituição dos valores despendidos para a aquisição dos BIDs não utilizados.<br />
 2.2.3. Ao oferecer um BID, o BIDBUD considerará sempre o BID com menor prazo a expirar, desde que válido.<br />
 2.3. O USUÁRIO poderá requerer a devolução dos valores despendidos na compra de créditos, desde que não tenham sido utilizados em leilões, e que a requisição seja realizada em prazo não superior a 7 (sete) dias corridos, a partir da data da respectiva aquisição, observado o disposto nos parágrafos abaixo.<br />
 2.3.1. A requisição de devolução será realizada por meio de carta contendo a assinatura do USUÁRIO, a ser encaminhada à sede do BIDBUD ou por meio de envio da referida carta, devidamente digitalizada, ao endereço eletrônico do BIDBUD: <a href="mailto:contato@bidbud.com.br">contato@bidbud.com.br</a>. A requisição de devolução deverá conter, obrigatoriamente: nome completo, Apelido, CPF, data da aquisição, valor despendido e dados bancários completos.<br />
 2.3.2. A devolução será realizada em até 30 (trinta) dias contados da data do recebimento da solicitação pelo BIDBUD, sendo descontado o percentual de 10% (dez por cento) a título de multa acrescidos de R$30,00 (trinta reais) referente aos custos bancários, operacionais e de manutenção do sistema.<br />
 2.4. Havendo créditos em BIDs em sua conta, o USUÁRIO poderá participar dos leilões oferecidos pelo BIDBUD. Cada BID equivalerá a 1 (um) click (salvo quando informado de forma diversa no leilão) que será descontado de sua conta, do total de BIDs que possuir. O BID valerá R$0,01 (um centavo) e, conforme o caso e a modalidade de leilão, o valor será adicionado ao preço do produto.<br />
 2.4.1. O valor do BID e a forma de acréscimo no preço poderão ser alterados, sendo certo que as condições serão devidamente informadas no respectivo leilão.<br />
 2.4.2. Os USUÁRIOS poderão oferecer BIDs a qualquer momento em qualquer leilão oferecido pelo BIDBUD, desde que enquadrados nos termos aqui dispostos e nas regras de cada leilão.<br />
 2.4.2.1. Sendo o BID ofertado abaixo do tempo estabelecido em segundos na página de cada leilão, o cronômetro regressivo retornará para o tempo indicado no mesmo.<br />
 2.4.3. O último USUÁRIO a dar o BID antes da conclusão do leilão será o vencedor deste leilão ("Vencedor"). O Vencedor terá a opção de compra do produto leiloado e deverá efetuar o pagamento do valor final do leilão acrescido do frete, que compreende não apenas o valor da transportadora, como também os serviços administrativos de solicitação, pagamento e envio do produto ao Vencedor ("Frete").<br />
 2.4.3.1 O Vencedor deverá efetuar o pagamento em até 5 (cinco) dias corridos contados da data do arremate, sob pena de perder o direito de adquirir o produto arrematado.<br />
 <strong>2.4.4. TODOS OS BIDs OFERECIDOS DURANTE O LEILÃO SERÃO CONSIDERADOS DEVIDAMENTE UTILIZADOS E NÃO SERÃO RESTITUÍDOS EM FORMA DE BIDs, PRODUTO OU DINHEIRO, INDEPENDENTE DE DESISTÊNCIA OU PERDA DO DIREITO DE ADQUIRIR O PRODUTO, SALVO SE PREVISTO NAS CONDIÇÕES DO LEILÃO.</strong> <br />
 2.4.5. No caso de perda do direito de adquirir o produto arrematado, conforme cláusula 2.4.3.1, o USUÁRIO não terá direito à restituição dos valores despendidos no leilão, bem como estará sujeito ao pagamento de multa de 20% (vinte por cento) do valor final do arremate, acrescidos do valor do Frete, conforme o caso. Na hipótese de o USUÁRIO possuir BIDs em sua conta, o mesmo desde já autoriza o BIDBUD a debitá-la e utilizar tantos BIDS quanto forem necessários para pagamento da multa mencionada acima. <br />
 2.4.6. No caso de perda do direito de adquirir o produto, conforme cláusula 2.4.3.1, o USUÁRIO que tiver oferecido o penúltimo BID terá a opção de compra do produto, ocasião em que o BIDBUD enviará email ao USUÁRIO outorgando-lhe o direito de adquirir o mesmo.<br />
 2.4.7. O produto arrematado será encaminhado à transportadora que se responsabilizará pela entrega do produto ao Vencedor em até 90 (noventa) dias corridos após o cumprimento de todas as regras do leilão, em especial as regras que se referem ao pagamento. <u>O prazo de recebimento do produto poderá variar de acordo com a transportadora e o local de entrega</u>.<br />
 2.4.8. Caso a loja não disponha do produto arrematado, o BIDBUD poderá, a seu exclusivo critério, não necessariamente nessa ordem, oferecer: i) outro produto similar ou superior; ii) crédito em BIDs na conta do vencedor; iii) adquirir outro produto na loja parceira ofertante do referido leilão, até o valor do produto arrematado.<br />
 <strong>3. MODALIDADES DE LEILÃO</strong><br />
 3.1. Os leilões poderão ter regras diferenciadas além das regras gerais previstas neste Termo. As regras específicas serão devidamente veiculadas no respectivo leilão, podendo, inclusive, o leilão ser realizado de forma mista, prevendo regras de diferentes modalidades a serem aplicadas de forma concomitante, conforme a seguir. Em caso de conflito entre as disposições do presente Termo e as regras específicas de determinado leilão, prevalecerão as regras determinadas no respectivo leilão.<br />
 3.1.1. Leilão Tradicional: todas as regras previstas na cláusula 2 acima, sem qualquer limitação de preço e com o pagamento do Frete, conforme divulgado no site.<br />
 3.1.2. Leilão Vale Desconto: Todos os USUÁRIOS que derem a quantidade mínima de BIDs especificada no respectivo leilão, terão direito a um desconto informado sobre o valor do produto ou na loja parceira, conforme as regras do leilão.<br />
 3.1.3. Leilão Frete Grátis: as mesmas regras do Leilão Tradicional, exceto o preço do Frete que será gratuito.<br />
 3.1.4. Leilão Preço Limitado: nesse caso será fixado um preço máximo de arremate e, quando o valor máximo for atingido, o preço final do produto não aumentará, mesmo que sejam dados novos BIDs, hipótese em que o leilão prosseguirá com o preço de arremate fixo até que um USUÁRIO vença o leilão.<br />
 3.1.5. Leilão Beneficente: o valor pago a título de arremate será beneficente, sendo pago diretamente para instituição beneficiada ou ao BIDBUD que efetuará o repasse, conforme estipulado nas regras do leilão.<br />
 <strong>4. LIMITAÇÕES</strong><br />
 4.1. No caso de oscilação na rede ou comprometimento da comunicação com o servidor, de forma a evitar que os USUÁRIOS sejam prejudicados, o leilão poderá ser suspenso ou cancelado, a exclusivo critério do BIDBUD. <br />
 4.1.1. No caso de suspensão, o leilão poderá ser interrompido pelo prazo que o BIDBUD julgar necessário. Nessa hipótese os BIDs não serão restituídos e o leilão reiniciará no mesmo preço.<br />
 <strong>4.1.2</strong><strong>. No caso de queda do servidor ou comprometimento de comunicação que acarrete na arrematação do produto, o leilão será automaticamente cancelado e, conseqüentemente, o produto arrematado será incluído em um novo leilão. Nesse caso, todos os BIDs utilizados pelos usuários serão restituídos e <u>o produto</u> <u>não será entregue.</u></strong><strong><br />
 <strong>4.2. O USUÁRIO poderá enfrentar problemas no acesso ao BIDBUD, por eventuais problemas técnicos de acesso à internet, que possam impossibilitar a conexão, falhas de acesso ou queda de conexão e/ou outros fatores externos, razão pela qual o BIDBUD não se responsabiliza, em nenhuma hipótese, por eventuais prejuízos decorrentes de falhas técnicas ou de acesso ao site</strong></strong>.<br />
 4.3. <strong>O USUÁRIO não poderá</strong> arrematar mais do que 5 (cinco) leilões no período de 30 (trinta) dias corridos ou 2 (dois) leilões no período de 24 (vinte e quatro) horas. <br />
 4.3.1. O USUÁRIO que atingir os limites estabelecidos acima terá o direito utilizar seus BIDs bloqueado, sendo o desbloqueio realizado em até 5 horas após satisfeitas as condições para participação em novos leilões.  <br />
 4.4. Nenhum dos sócios e funcionários do BIDBUD poderá adquirir BIDs, participar dos leilões ou arrematar produtos. A proibição é estendida aos cônjuges e parentes de primeiro grau dos sócios e funcionários.<br />
 4.5.<strong>A aquisição de BIDs deverá ser realizada diretamente pelo USUÁRIO. Caso seja verificado que os BIDs foram adquiridos por terceiros, o cadastro poderá ser suspenso ou bloqueado, a exclusivo critério do BIDBUD, sendo restituído o valor para compra dos BIDs,sendo observadas as mesmas regras dispostas para o cancelamento de compra de BIDs na cláusula 2.3.2.</strong><br />
 4.6. O BIDBUD é direcionado ao público brasileiro e só serão realizadas entregas de produtos em território nacional em áreas atendidas pelas transportadoras utilizadas pelos parceiros do BIDBUD.<br />
 4.6.1. Os parceiros BIDBUD se reservam ao direito de somente realizar entregas no endereço de cadastro do Vencedor do leilão.<br />
 4.6.2. O BIDBUD e os parceiros não se responsabilizam por problemas nas entregas devido a informações erradas ou incompletas no cadastro do Vencedor, bem como pela ausência de pessoa autorizada a receber o produto arrematado no ato da entrega<br />
 4.6.2.1. No caso de insucesso na entrega por qualquer problema externo ao controle das transportadoras, uma nova tentativa de entrega pode ser realizada, a entrega pode ser suspensa ou até mesmo cancelada. <br />
 4.6.2.2. No caso de nova tentativa de entrega, quando possível, poderá incorrer uma nova cobrança de Frete.<br />
 4.6.3. No caso do Vencedor não se encontrar em área atendida pelas transportadoras utilizadas pelo parceiro do leilão arrematado, devem ser observadas as mesmas regras para o caso de indisponibilidade do produto arrematado, estabelecidas na cláusula 2.4.8.<br />
 4.7. No caso de o USUÁRIO descumprir qualquer disposição prevista no Termo, incluindo mas não se limitando a: fornecimento de informações incorretas, prática de ações com o objetivo de fraudar ou impedir o correto transcurso do leilão, utilização de dois ou mais cadastros, sozinho ou em conluio com terceiros com objetivo de afastar a disputa legítima, o BIDBUD poderá, a seu exclusivo critério, suspender, de forma temporária ou definitiva, ou cancelar o cadastro do USUÁRIO, sem prejuízo da adoção de medidas judiciais cabíveis.<strong>5. DISPOSIÇÕES FINAIS</strong><br />
 5.1. O presente Termo é válido por prazo indeterminado, podendo, a critério do BIDBUD, ser modificado, com o objetivo de precípuo do aprimoramento dos serviços e do Termo que rege a relação entre as Partes. No caso de alteração do Termo, o BIDBUD disponibilizará o novo texto em vigor no site.<br />
 5.2. O presente Termo é regido pela legislação brasileira, e as Partes elegem o foro central da cidade do Rio de Janeiro/RJ, como competente para dirimir quaisquer questões.</p>
	</div><!-- box -->
</div>
<script>
$("#dialog-terms").dialog({
	autoOpen: false,
	modal: true,
	resizable: true,
	width: $("#mainContainer").width(),
	buttons: {
		'Não Aceito': function() {
			window.location = '<?php echo Router::url(array('controller' => 'users', 'action' => 'logout'), true); ?>';
		},
		'Aceito': function() {
			var $ret = {
				success: false,
				message: "Falha ao enviar dados."
			};

			$.ajax({
				type: 'POST',
				async: false,
				url: '<?php echo Router::url(array('controller' => 'users', 'action' => 'newagreement', 'meu' => true),true); ?>',
				dataType: 'JSON',
				success: function(data) {
					if (data.success) {
						$("#dialog-terms").dialog("close");
						$("#dialog").dialog("close");
					} else {
						alert(data.message);
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					alert("Falha ao enviar dados. Por favor tente novamente.");
				}
			});
		}
	}

});
$("#dialog").dialog({
	autoOpen: true,
	width: 330,
	modal: true,
	resizable: false,
	closeOnEscape: false,
	open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); },
	buttons: {
		'Não Aceito': function() {
			window.location = '<?php echo Router::url(array('controller' => 'users', 'action' => 'logout'), true); ?>';
		},
		'Aceito': function() {
			var $ret = {
				success: false,
				message: "Falha ao enviar dados."
			};

			$.ajax({
				type: 'POST',
				async: false,
				url: '<?php echo Router::url(array('controller' => 'users', 'action' => 'newagreement', 'meu' => true),true); ?>',
				dataType: 'JSON',
				success: function(data) {
					if (data.success) {
						$("#dialog").dialog("close");
					} else {
						alert(data.message);
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					alert("Falha ao enviar dados. Por favor tente novamente.");
				}
			});
		}
	}
});
</script>