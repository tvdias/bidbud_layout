<div id="caminho" style="display:none;">
    <?php $this->Html->addCrumb('Cadastro'); ?>
	<p><?php echo $this->element('crumb'); ?></p>
	<div style="clear: both;"></div>
</div><!-- caminho -->

<div id="conteudoTitulo" style="display:none;">
	<h2>Parabéns!</h2>
    <div style="clear: both;"></div>
</div><!-- conteudoTitulo -->

<div class="topBox">
	<div class="topBoxMSG" style="left: 30px;">ERRO</div>
	<p class="s16 cinza" style="position: absolute; left: 30px; top: 60px; margin-right: 30px;">Não foi possível completar o seu cadastro.<br />
		Persistindo o problema, favor entrar em contato pelo sac@apetrexoleilao.com.br.</p>
	<div id="topBoxIMG"></div>
</div>