<div id="caminho">
    <?php $this->Html->addCrumb('Cadastro Finalizado', '/cadastrofinalizado'); ?>
	<p><?php echo $this->element('crumb'); ?></p>
	<div style="clear: both;"></div>
</div><!-- caminho -->

<div id="conteudoTitulo" style="display:none;">
	<h2>Parabéns!</h2>
    <div style="clear: both;"></div>
</div><!-- conteudoTitulo -->

<div class="topBox">
	<div class="topBoxMSG" style="left: 30px;">Você acaba de se registrar no ApetreXo Leilão</div>
	<p class="s16 cinza" style="position: absolute; left: 30px; top: 60px; margin-right: 30px;">Não conseguimos enviar o seu e-mail de cadastro.<br />
		Favor entrar em contato pelo sac@apetrexoleilao.com.br solicitando o seu código de ativação.</p>
	<div id="topBoxIMG"></div>
</div>