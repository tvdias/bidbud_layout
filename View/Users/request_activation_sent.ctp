<div id="caminho">
	<?php //$html->addCrumb('Reenvio de E-mail de Ativação', '/ativacao'); ?>
	<p><?php echo $this->element('crumb'); ?></p>
	<div style="clear: both;"></div>
</div><!-- caminho -->

<div id="conteudoTitulo">
	<h2>Reenvio do E-mail de Ativação</h2>
	<div style="clear: both;"></div>
</div><!-- conteudoTitulo -->

<div class="topBox">
	<div class="topBoxMSG">Recebemos o seu pedido!</div>
	<p class="s14 cinza" style="position: absolute; left: 50px; top: 80px; width: 460px;">Você receberá um e-mail com instruções de ativação.</p>
	<div id="topBoxIMG"></div>
	<div class="no_spam"></div>
</div>