<div id="caminho">
	<?php //$html->addCrumb('Sobre o BidBud', '/sobre'); ?>
	<p>
		<?php echo $this->element('crumb'); ?>
	</p>
	<div style="clear: both;"></div>
</div><!-- caminho -->

<div id="conteudoTitulo">
	<h2>O que é o ApetreXo Leilão?</h2>
	<div style="clear: both;"></div>
</div><!-- conteudoTitulo -->

<div class="box">
	<div class="s16 cinza" style="margin: 20px; text-align: justify;">
		O <b>ApetreXo Leilão </b>foi criado com o objetivo de oferecer serviços de leilão de produtos no sistema de centavos, onde é facultado aos usuários adquirir produtos por valores inferiores aos praticados pelo mercado.<br />
		<br />
		Dependendo da modalidade do leilão, no <b>ApetreXo Leilão</b>, mesmo que você não arremate você poderá adquirir os produtos, com descontos nas lojas parceiras, conforme a regra do respectivo leilão que participar.
	</div>
	<div style="clear: both;"></div>
</div>

<div class="box">
	<div class="blueHeader">COMO FUNCIONA</div>

	<p style="font: 16px/20px Arial">1. <b><?php echo $this->Html->link("Como faço para me cadastrar no ApetreXo Leilão?", array('controller' => 'pages', 'action' => 'display', 'how_it_works', '#' => "cadastrar")); ?> </b></p>
	<p style="font: 16px/20px Arial">2. <b><?php echo $this->Html->link("Como funcionam os leilões?", array('controller' => 'pages', 'action' => 'display', 'how_it_works', '#' => "como")); ?> </b></p>
	<p style="font: 16px/20px Arial">3. <b><?php echo $this->Html->link("Quais são os tipos de leilões?", array('controller' => 'pages', 'action' => 'display', 'how_it_works', '#' => "tipos")); ?> </b></p>
</div>