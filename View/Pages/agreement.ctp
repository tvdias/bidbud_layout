<div id="caminho">
	<?php //$html->addCrumb('Termos e Condições de Uso', '/condicoes-de-uso'); ?>
	<p style="font: 10px Helvetica; color: black; border-bottom: 2px black solid;width: 700px; margin-top: 5px; padding-bottom: 5px; margin-bottom: 10px;">
		<?php echo $this->element('crumb'); ?>
	</p>
	<div style="clear: both;"></div>
</div><!-- caminho -->

<div id="conteudoTitulo">
	<h2>Termos e Condições de Uso do site Apetrexo Leilão</h2>
	<div style="clear: both;"></div>
</div><!-- conteudoTitulo -->

<div class="box">

<p>APTX GROUP LTDA., sociedade limitada inscrita no CNPJ/MF sob o nº 08.972.582/0001-80, proprietário do website: www.apetrexoleilao.com.br ("APETREXO LEILÃO"), estabelece, por meio do presente Termo e Condições Gerais de Uso ("Termo"), as condições para a utilização dos serviços e/ou aquisição de produtos, ou qualquer relação com o USUÁRIO, sendo certo que o presente Termo vigorará a partir do momento que o USUÁRIO preencher o formulário cadastral e clicar no seu campo "CONFIRMAR", o que significará plena aceitação e concordância com todos os termos e condições constantes do presente Termo, sem ressalvas:</p>
<h3>1. OBJETO E FORMA UTILIZAÇÃO DO  SITE</h3>
<p>1.1. O presente instrumento tem como objetivo estabelecer os termos e condições, política e normas para acesso ao site, compra de LANCES, prestação de serviços e demais condições gerais.
1.2. O website só poderá ser utilizado por pessoas jurídicas (na pessoa de seus representantes legais) ou pessoas físicas, maiores de idade e em sua plena capacidade ("USUÁRIO"), sendo proibido o cadastramento de pessoas que não observem os requisitos acima. São condições precedentes para a participação nos leilões, a realização do cadastro, leitura e aceitação dos termos e condições previstos no presente Termo. O USUÁRIO declara, sob as penas da lei, preencher todos os requisitos acima e que todos os dados fornecidos ao APETREXO LEILÃO, para fins de cadastro, são verdadeiros. O USUÁRIO tem ciência que o fornecimento de dados incorretos ou incompletos, poderá acarretar em 



problemas na entrega do produto arrematado e/ou o cancelamento do cadastro.
1.2.1. Cada USUÁRIO terá o direito de utilizar apenas 1 (um) cadastro, sendo responsável pela sua guarda e da respectiva senha que são pessoais e intransferíveis, sendo terminantemente proibido o fornecimento dos dados a terceiros.
1.3. O USUÁRIO poderá escolher o seu próprio apelido ("Apelido") que aparecerá no momento do oferecimento dos LANCES, utilizando o bom senso e normas gerais de boa conduta, sendo terminantemente proibida a utilização de nomes preconceituosos, obscenos, agressivos ou que possa violar direitos de terceiros ou a dignidade. A utilização de nome que atente de forma direta ou indireta aos preceitos acima poderá acarretar no imediato cancelamento do cadastro pelo APETREXO LEILÃO, sem prévio aviso e sem direito a restituição de quaisquer valores pagos para a aquisição de LANCES, além das cominações previstas na legislação brasileira.
1.3.1. O APETREXO LEILÃO se reserva ao direito de solicitar que o USUÁRIO sugira um novo Apelido, caso verifique que o mesmo tem o objetivo de afetar o transcurso leilão, induzir outros USUÁRIOS a erro ou, ainda, se passar por outra pessoa.
1.4. O USUÁRIO autoriza o APETREXO LEILÃO a tomar todas as medidas necessárias para, eventualmente, verificar as informações fornecidas pelo USUÁRIO.
1.5. O USUÁRIO autoriza o rastreamento de seu endereço IP ou a utilização de qualquer outro meio de controle com o fim único e específico de verificar a autenticidade e assegurar uma transação mais confiável e segura.
1.6. O USUÁRIO tem ciência de que deve tomar todas as medidas necessárias para proteger o seu computador, sendo certo que o APETREXO LEILÃO protege as informações junto aos seus servidores, mas não pode assegurar a segurança durante o tráfego pela internet.
1.7. As informações fornecidas pelo USUÁRIO são armazenados em servidores com elevados padrões de segurança, sendo certo que o APETREXO LEILÃO adota todas as medidas possíveis para manter a integridade e confidencialidade das informações, todavia não é responsável por prejuízos causados por terceiros que utilizem as redes públicas ou a internet para acessar as informações dos usuários.
1.8. É terminantemente proibida a modificação ou transmissão de qualquer conteúdo do site, modificado ou não, sem autorização por escrito do APETREXO LEILÃO, ou a utilização para qualquer outro fim que não o de entretenimento e participação no leilão.
</p>
<h3>2. LEILÃO</h3>
<p>2.1. É condição precedente para a participação nos leilões do APETREXO LEILÃO a  aquisição de créditos que permitem dar lances (&quot;LANCE&quot; ou &quot;LANCES&quot;) . O LANCE somente poderá ser  adquirido no site APETREXO LEILÃO, parceiros autorizados ou disponibilizado em  promoções, conforme cláusula 2.2 abaixo. <br />
  2.1.1. O LANCE  adquirido, seja por compra e/ou promoção, é pessoal e intransferível, não  podendo ser cedido, negociado, transferido ou, ainda, realizar qualquer negócio  que envolva o LANCE que não seja com o APETREXO LEILÃO. A celebração de  qualquer negócio com o LANCE ou transferência do mesmo será considerada nula de  pleno direito.<br />
  2.2. <strong>Os LANCES terão validade de até 180 (cento  e oitenta) dias contados da data de aquisição, salvo nos casos de pacotes  promocionais que poderão ter seu prazo de validade reduzido, conforme as regras  da respectiva promoção. Após transcorrido o prazo de validade os LANCES não  utilizados serão automaticamente cancelados, sem outorgar direito a restituição  de qualquer valor.</strong><br />
  2.2.1. O APETREXO  LEILÃO poderá disponibilizar promoções e descontos promocionais para a  aquisição de LANCES, com condições específicas e por tempo determinado que  constarão no regulamento da respectiva promoção. Os LANCES adquiridos com  descontos promocionais poderão ter prazo de validade inferior ao prazo previsto  na cláusula 2.2 acima, conforme as regras estipuladas na respectiva promoção.<br />
  2.2.2. Todos os LANCES,  promocionais ou não, perderão a sua validade automaticamente no prazo  estipulado na compra ou promoção, independente de prévia notificação. Nesse  caso, os LANCES não poderão ser utilizados e tampouco o USUÁRIO terá direito a restituição dos  valores despendidos para a aquisição dos LANCES não utilizados.<br />
  2.2.3. Ao oferecer um  LANCE, o APETREXO LEILÃO considerará sempre o LANCE com menor prazo a expirar,  desde que válido.<br />
  2.3. O USUÁRIO poderá requerer a devolução dos valores despendidos na compra de  créditos, desde que não tenham sido utilizados em leilões, e que a requisição  seja realizada em prazo não superior a 7 (sete) dias corridos, a partir da data  da respectiva aquisição, observado o disposto nos parágrafos abaixo.<br />
  2.3.1. A requisição de devolução será realizada por meio de carta contendo a  assinatura do USUÁRIO, a ser encaminhada à sede do APETREXO LEILÃO ou por meio  de envio da referida carta, devidamente digitalizada, ao endereço eletrônico do  APETREXO LEILÃO: <a href="mailto:sac@apetrexoleilao.com.br">sac@apetrexoleilao.com.br</a>. A requisição de  devolução deverá conter, obrigatoriamente: nome completo, Apelido, CPF, data da  aquisição, valor despendido e dados bancários completos.<br />
  2.3.2. A devolução será realizada em até 30 (trinta) dias contados da data do  recebimento da solicitação pelo APETREXO LEILÃO, sendo descontado o percentual  de 10% (dez por cento) a título de multa acrescidos de R$30,00 (trinta reais)  referente aos custos bancários, operacionais e de manutenção do sistema.<br />
  2.4. Havendo créditos em LANCES em sua conta, o USUÁRIO poderá participar dos  leilões oferecidos pelo APETREXO LEILÃO. Cada LANCE equivalerá a 1 (um) click  (salvo quando informado de forma diversa no leilão) que será descontado de sua  conta, do total de LANCES que possuir. O LANCE valerá R$0,01 (um centavo) e,  conforme o caso e a modalidade de leilão, o valor será adicionado ao preço do  produto.<br />
  2.4.1. O valor do LANCE e a forma de acréscimo no preço poderão ser alterados,  sendo certo que as condições serão devidamente informadas no respectivo leilão.<br />
  2.4.2. Os USUÁRIOS poderão oferecer LANCES a qualquer momento em qualquer  leilão oferecido pelo APETREXO LEILÃO, desde que enquadrados nos termos aqui  dispostos e nas regras de cada leilão.<br />
  2.4.2.1. Sendo o LANCE  ofertado abaixo do tempo estabelecido em segundos na página de cada leilão, o  cronômetro regressivo retornará para o tempo indicado no mesmo.<br />
  2.4.3. O último USUÁRIO a dar o LANCE antes da conclusão do leilão será o  vencedor deste leilão (&quot;Vencedor&quot;). O Vencedor terá a opção de compra do  produto leiloado e deverá efetuar o pagamento do valor final do leilão  acrescido do frete, que compreende não apenas o valor da transportadora, como  também os serviços administrativos de solicitação, pagamento e envio do produto  ao Vencedor (&quot;Frete&quot;).<br />
  2.4.3.1. O vencedor  deverá efetuar o pagamento em até 5 (cinco) dias corridos contados  da  data do arremate, sob pena de perder o direito de adquirir o produto  arrematado.<br />
  <strong>2.4.4. TODOS OS LANCES OFERECIDOS DURANTE O LEILÃO SERÃO  CONSIDERADOS DEVIDAMENTE UTILIZADOS E NÃO SERÃO RESTITUÍDOS EM FORMA DE LANCES,  PRODUTO OU DINHEIRO, INDEPENDENTE DE DESISTÊNCIA OU PERDA DO DIREITO DE  ADQUIRIR O PRODUTO, SALVO SE PREVISTO NAS CONDIÇÕES DO LEILÃO.</strong> <br />
  2.4.5. No caso de perda do direito de adquirir o produto arrematado, conforme  cláusula 2.4.3, o USUÁRIO não terá direito à restituição dos valores  despendidos no leilão, bem como estará sujeito ao pagamento de multa de 20%  (vinte por cento) do valor final do arremate, acrescidos do valor do Frete,  conforme o caso.  Na hipótese de o USUÁRIO possuir LANCES em sua conta, o  mesmo desde já autoriza o APETREXO LEILÃO a debitá-la e utilizar tantos LANCES  quanto forem necessários para pagamento da multa mencionada acima.  <br />
  2.4.6. O produto arrematado será encaminhado à transportadora que se  responsabilizará pela entrega do produto ao Vencedor em aproximadamente 10 (dez)  dias úteis após o cumprimento de todas as regras do leilão, em especial as  regras que se referem ao pagamento. <u>O prazo de recebimento do produto poderá variar de acordo com a  transportadora e o local de entrega</u>.<br />
2.4.7. Caso a loja  não disponha do produto arrematado, o APETREXO LEILÃO poderá, a seu exclusivo  critério, não necessariamente nessa ordem, oferecer: i) outro produto similar  ou superior; ii) crédito em LANCES na conta do vencedor.</p>
<h3>3. AQUISIÇÃO DO PRODUTO NÃO  ARREMATADO</h3>
<p>3.1. De acordo com a regra de cada leilão, o USUÁRIO poderá ter a opção de reverter em dobro, ou em sua totalidade, ou em parte, os lances dados nos leilões em crédito para a aquisição do produto ("COMPRE AGORA"). O USUÁRIO terá 24 horas para adquirir o produto do leilão que participou com preço especial. As informações do valor de crédito que cada usuário tem direito estarão disponíveis em até 1 hora do término do leilão, quando logado no sistema, na página de leilão de cada produto ou na área pessoal e só poderá ser adquirido pelo site do APETREXO LEILÃO. 
3.2. Cada usuário fica limitado a compra de 1 (um) produto por leilão com o desconto oferecido e não é cumulativo com outras promoções ou descontos.
3.3. O produto adquirido pela opção COMPRE AGORA será encaminhado à transportadora que se responsabilizará pela entrega do produto ao USUÁRIO em aproximadamente 30 (trinta) dias úteis após o cumprimento de todas as regras do leilão, em especial as regras que se referem ao pagamento. O prazo de recebimento do produto poderá variar de acordo com a transportadora e o local de entrega.
3.3.1. Caso a loja não disponha do produto, o APETREXO LEILÃO poderá, a seu exclusivo critério, não necessariamente nessa ordem, oferecer outro produto similar ou superior.
</p>
<h3>4. MODALIDADES DE LEILÃO</h3>
<p>4.1. Os leilões poderão ter regras diferenciadas além das regras gerais previstas neste Termo. As regras específicas serão devidamente veiculadas no respectivo leilão, podendo, inclusive, o leilão ser realizado de forma mista, prevendo regras de diferentes modalidades a serem aplicadas de forma concomitante. Em caso de conflito entre as disposições do presente Termo e as regras específicas de determinado leilão, prevalecerão as regras determinadas no respectivo leilão.</p>
<h3>5. LIMITAÇÕES</h3>
<p>5.1. No caso de oscilação na rede ou comprometimento da comunicação com o servidor, de forma a evitar que os USUÁRIOS sejam prejudicados, o leilão poderá ser suspenso ou cancelado, a exclusivo critério do APETREXO LEILÃO. 
5.1.1. No caso de suspensão, o leilão poderá ser interrompido pelo prazo que o APETREXO LEILÃO julgar necessário. Nessa hipótese os LANCES não serão restituídos e o leilão reiniciará no mesmo preço.
5.1.2. No caso de queda do servidor ou comprometimento de comunicação que acarrete na arrematação do produto, o leilão será automaticamente cancelado e, conseqüentemente, o produto arrematado será incluído em um novo leilão. Nesse caso, todos os LANCES utilizados pelos usuários serão restituídos e o produto não será entregue.
5.2. O USUÁRIO poderá enfrentar problemas no acesso ao APETREXO LEILÃO, por eventuais problemas técnicos de acesso à internet, que possam impossibilitar a conexão, falhas de acesso ou queda de conexão e/ou outros fatores externos, razão pela qual o APETREXO LEILÃO não se responsabiliza, em nenhuma hipótese, por eventuais prejuízos decorrentes de falhas técnicas ou de acesso ao site.
5.3. O USUÁRIO não poderá arrematar mais do que 5 (cinco) leilões no período de 30 (trinta) dias corridos ou 2 (dois) leilões no período de 24 (vinte e quatro) horas.
5.3.1. O USUÁRIO que atingir os limites estabelecidos acima terá seus LANCES bloqueado, sendo o desbloqueio realizado em até 5 horas após satisfeitas as condições para participação em novos leilões. 




5.3.2. O limite de arremates previsto na cláusula 5.3. poderá ser alterado, a qualquer tempo, conforme promoções ou limitações promovidas pelo APETREXO LEILÃO.
5.4. Nenhum dos sócios e funcionários do APETREXO LEILÃO poderá adquirir LANCES, participar dos leilões ou arrematar produtos. A proibição é estendida aos cônjuges e parentes de primeiro grau dos sócios e funcionários.
5.5. A aquisição de LANCES deverá ser realizada diretamente pelo USUÁRIO. Caso seja verificado que os LANCES foram adquiridos por terceiros, o cadastro poderá ser suspenso ou bloqueado, a exclusivo critério do APETREXO LEILÃO, sendo restituído 
o valor para compra dos LANCES, sendo observadas as mesmas regras dispostas para o cancelamento de compra de LANCES na cláusula 2.3.2.
5.6. O APETREXO LEILÃO é direcionado ao público brasileiro e só serão realizadas entregas de produtos em território nacional em áreas atendidas pelas transportadoras.
5.6.1. O APETREXO LEILÃO se reserva ao direito de somente realizar entregas no endereço de cadastro do Vencedor do leilão.
5.6.2. O APETREXO LEILÃO não se responsabiliza por problemas nas entregas devido a informações erradas ou incompletas no cadastro do Vencedor, bem como pela ausência de pessoa autorizada a receber o produto arrematado no ato da entrega
5.6.2.1. No caso de insucesso na entrega por qualquer problema externo ao controle das transportadoras, uma nova tentativa de entrega pode ser realizada, a entrega pode ser suspensa ou até mesmo cancelada. 
5.6.2.2. No caso de nova tentativa de entrega, quando possível, poderá incorrer uma nova cobrança de Frete.
5.6.3. No caso do Vencedor não se encontrar em área atendida pelas transportadoras utilizadas, devem ser observadas as mesmas regras para o caso de indisponibilidade do produto arrematado, estabelecidas na cláusula 2.4.7.
5.7. No caso de o USUÁRIO descumprir qualquer disposição prevista no Termo, incluindo mas não se limitando a: fornecimento de informações incorretas, prática de ações com o objetivo de fraudar ou impedir o correto transcurso do leilão, utilização de dois ou mais cadastros, sozinho ou em conluio com terceiros com objetivo de afastar a disputa legítima, o APETREXO LEILÃO poderá, a seu exclusivo critério, suspender, de forma temporária ou definitiva, ou cancelar o cadastro do USUÁRIO, sem prejuízo da adoção de medidas judiciais cabíveis.
</p>
<h3>6.  DISPOSIÇÕES FINAIS</h3>
<p>6.1. O presente Termo é válido por prazo indeterminado, podendo, a critério do APETREXO LEILÃO, ser modificado, com o objetivo de precípuo do aprimoramento dos serviços e do Termo que rege a relação entre as Partes. No caso de alteração do Termo, o APETREXO LEILÃO disponibilizará o novo texto em vigor no site.
5.2. O presente Termo é regido pela legislação brasileira, e as Partes elegem o foro central da cidade do Rio de Janeiro/RJ, como competente para dirimir quaisquer questões.</p>

</div><!-- box -->