<script>
// verifica se JS esta habilitado
window.location.replace("<?php echo Router::url('/'); ?>");
</script>

<div id="conteudoTitulo">
	<h2>ERRO</h2>
	<div style="clear: both;"></div>
</div><!-- conteudoTitulo -->

<div class="box">
	<div class="s16 cinza" style="margin: 20px;">
		Para acessar esta página você deve habilitar o JavaScript em seu navegador. <a href="http://support.google.com/bin/answer.py?hl=en&answer=23852" target="_blank">Clique aqui para saber como fazer isso.</a><br/>
		Para tentar acessar o site novamente, <?php echo $this->Html->link("clique aqui", '/'); ?>.
	</div>
	<div style="clear: both;"></div>
</div>