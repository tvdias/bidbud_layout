<div id="caminho">
	<?php //$html->addCrumb('Política de Privacidade', '/privacidade'); ?>
	<p>
		<?php echo $this->element('crumb'); ?>
	</p>
	<div style="clear: both;"></div>
</div><!-- caminho -->

<div id="conteudoTitulo">
	<h2>Política de Privacidade</h2>
	<div style="clear: both;"></div>
</div><!-- conteudoTitulo -->

<div class="box">

<p>O website <a href="http://www.apetrexoleilao.com.br">www.apetrexoleilao.com.br</a>, simplesmente denominado <strong>APETREXO LEILÃO</strong>, tem como objetivo proporcionar aos seus usuários diversão e serviços. Abaixo a política de privacidade esclarece como é realizado o tratamento das informações pessoais disponibilizadas ao <strong>APETREXO LEILÃO</strong> e demais aspectos técnicos no que se refere à coleta e armazenamento de dados.<br />
 <h3>Segurança e Prevenção</h3>
<p>O APETREXO LEILÃO toma todas as medidas razoáveis necessárias para manter a privacidade e a segurança dos dados fornecidos pelos nossos clientes, de forma a evitar a perda, má utilização, extração ou alteração das informações cadastradas no site. As informações são devidamente protegidas por sistemas de controle e segurança. O datacenter tem controle de acesso e monitoramento 24 horas por dia e 7 dias por semana. O acesso é realizado através de senha e login criado pelo usuário. Todas as senha são criptografadas, protegendo o acesso indevido. 
Para prevenir fraudes e proporcionar a todos os usuários maior segurança na participação dos leilões, o APETREXO LEILÃO coleta e verifica as informações pessoais do usuário.
Apesar de todo o sistema de segurança o usuário deve ser alertado que, independente das medidas de segurança, não há sistema integralmente infalível. Geralmente o acesso indevido às informações ocorre por meio de malware capazes de identificar senha do usuário, razão pela qual recomendamos o uso de antivírus atualizado. O usuário é responsável pela guarda de sigilo de sua senha de acesso, sendo terminantemente proibido o fornecimento da senha a terceiros. O APETREXO LEILÃO não se responsabiliza pela má utilização da senha.<br />
O APETREXO LEILÃO INFORMA QUE NÃO SOLICITA A SUA SENHA ATRAVÉS DE E-MAILS RECELANCEOS DE MODO INESPERADO OU NÃO SOLICITADO PELO PRÓPRIO USUÁRIO.
Recomenda-se, ainda, não deixar a janela do navegador com usuário "logada" devendo fechar o navegador de Internet no momento em que deixar o computador. Esse cuidado é mais importante no caso de utilização de máquinas em locais públicos ou compartilhamento de redes.
 <h3>Informações dos Usuários</h3>
 <p>As informações pessoais fornecidas serão de uso restrito.
As informações poderão ser utilizadas para estudos internos, com o objetivo de identificar o público que utiliza o APETREXO LEILÃO e melhor adequar o site aos usuários e, conseqüentemente, a melhoria dos serviços prestados. O APETREXO LEILÃO não divulga ou comercializa as informações sobre os seus clientes, a divulgação só é permitida em caso de ordem judicial.
<br />
 <h3>Disposições Gerais</h3>
 <p>O usuário poderá, a qualquer momento, solicitar que não seja enviado material com conteúdo informativo ou publicitário ao seu e-mail. O usuário também poderá, a qualquer tempo, atualizar ou retificar os dados cadastrais. Ambos os requerimentos serão atendidos prazo razoável.
O APETREXO LEILÃO pode, eventualmente, gravar arquivos de dados – cookies – no computador do usuário. Os cookies têm como objetivo tornar a navegação mais rápida. O usuário pode excluir ou desinstalar os cookies a qualquer momento.
A política de privacidade poderá ser alterada a exclusivo critério do APETREXO LEILÃO, todavia, qualquer alteração será devidamente comunicada mediante publicação no site.
</p>
</div><!-- box -->