<div id="caminho">
	<?php //$html->addCrumb('Como funciona', '/como-funciona'); ?>
	<p><?php echo $this->element('crumb'); ?></p>
	<div style="clear: both;"></div>
</div><!-- caminho -->

<div id="conteudoTitulo">
	<h2>Como funciona</h2>
	<div style="clear: both;"></div>
</div><!-- conteudoTitulo -->

<div class="box">
	<div class="blueHeader">COMO FUNCIONA</div>

	<div style="margin: 10px 5px 10px 30px;">
        <ol>
            <li style="font: 16px/20px Arial"><a href="#cadastrar" class="azul s16"><b>Como faço para me cadastrar no ApetreXo Leilão?</b></a></li>
            <li style="font: 16px/20px Arial"><a href="#leiloes" class="azul s16"><b>Como funcionam os leilões?</b></a></li>
            <li style="font: 16px/20px Arial"><a href="#compre_agora" class="azul s16"><b>O que é o "Compre Agora"?</b></a></li>
            <li style="font: 16px/20px Arial"><a href="#dicas" class="azul s16"><b>Dicas de como usar o ApetreXo Leilão</b></a></li>
            <?php /*<li style="font: 16px/20px Arial"><a href="#tipos" class="azul s16"><b>Quais são os tipos de leilões?</b></a></li>*/ ?>
            <?php /*<li style="font: 16px/20px Arial; display: none;"><a href="#autobid" class="azul s16"><b>Como funciona o AutoBid?</b></a></li>*/ ?>
        </ol>
	</div>
</div>

<a name="cadastrar"></a>
<div class="box">
	<div class="blueHeader">Como faço para me cadastrar no ApetreXo Leilão?</div>
	<p>Cadastrar-se no ApetreXo Leilão é muito fácil. Basta seguir 2 etapas!</p>
	<p>
		1 – Faça o cadastro gratuitamente no ApetreXo Leilão, na página inicial, na seção "Cadastre-se".<br />
		2 – Ative seu cadastro seguindo as instruções que serão enviadas por email. Você poderá utilizar o link enviado no email de confirmação ou usar o código de ativação, que também será enviado.
	</p>
	<p>Na confirmação do cadastro você receberá 5 lances gratuitos que serão creditados diretamente na sua conta.</p>
	<p>
		Caso não tenha recebido o email de confirmação de cadastro, é provável que ele tenha sido bloqueado pelo seu sistema de SPAM. Procure o email na sua pasta SPAM
		Se mesmo assim você não localizar o e-mail, entre em contato pelo <a href="sac.apetrexoleilao.com.br">SAC</a> solicitando o reenvio do mesmo.
	</p>
</div>

<a name="leiloes"></a>
<div class="box">
	<div class="blueHeader">Como funcionam os leilões?</div>
	<p>
		Para participar dos leilões, o Usuário deverá ter creditado em sua conta lances, os quais poderão ser adquiridos através do site ApetreXo Leilão ou disponibilizado sem promoções.<br />
		Os leilões são tradicionalmente iniciados com um preço, conforme divulgado nas suas respectivas regras.<br />
		Você e os outros usuários poderão dar lances que, automaticamente, aumentarão em R$0,01 (um centavo) o preço do produto. O cronômetro retornará à contagem regressiva de 10 a 30 segundos, conforme especificado em cada leilão.<br />
		O vencedor será o usuário que tiver dado o último lance quando aparecer a palavra ARREMATADO ao final da contagem regressiva. Deve-se notar que o cronômetro não exibe os milisegundos, podendo haver variações entre navegadores e redes, razão pela qual o usuário poderá ter a impressão de que o cronômetro chegou a zero quando ainda restam alguns milisegundos. Da mesma forma, a rede, acesso à internet e computador do usuário estão sujeitos a falhas, razão pela qual o usuário não deve deixar para dar o lance no último segundo para evitar o risco de seu lance não ser computado, uma vez que até mesmo a informação de um lance leva um certo tempo para chegar aos servidores do site e ser registrado.<br />
		Você e os outros usuários poderão dar lances que, automaticamente, aumentarão em R$0,01 (um centavo) o preço do produto. O cronômetro retornará à contagem regressiva de 10 a 30 segundos, conforme especificado em cada leilão.<br />
		O vencedor efetuará o pagamento do valor correspondente ao arremate e frete, conforme o caso. O produto será encaminhado ao vencedor no endereço de seu cadastro, desde que dentro do território nacional.
	</p>
</div>

<a name="compre_agora"></a>
<div class="box">
	<div class="blueHeader">O que é o "Compre Agora"?</div>
	<p>
		O "Compre Agora" é a opção que o usuário tem para reverter em dobro, ou em sua totalidade, ou em parte, os lances dados nos leilões em crédito para a compra do produto. O usuário terá 24 horas para adquirir o produto do leilão que participou com preço especial. As informações do valor de crédito que cada usuário tem direito estarão disponíveis, quando logado no sistema, na página "Meus Descontos" dentro da seção "Meu Cadastro".
	</p>
</div>

<a name="dicas"></a>
<div class="box">
	<div class="blueHeader">Dicas de como usar o ApetreXo Leilão</div>
    <p><strong>Lances</strong>: É recomendável adquirir com antecedência os pacotes de lances para utilização nos leilões. Como os pagamentos são feitos eletronicamente, caso você precise de mais lances durante um leilão pode não haver tempo hábil para aprovação e processamento de seu pedido antes do encerramento da disputa.* Prepare seus lances com antecedência!.</p>
    <p><strong>Cronômetro</strong>: Cada leilão tem um cronômetro regressivo e indica qual usuário deu o último lance e está vencendo o leilão. Quando, no lugar do cronômetro, aparecer "Arrematado!", o usuário que tiver dado o último lance é o vencedor. Caso algum usuário dê um lance nos segundos finais, o cronômetro retorna até o número de segundos indicados (10s, 15s, 20s ou 30s, tipicamente). Além disso, quando o cronômetro atinge menos de um segundo, poderá ser exibido "Verificando...". Isto não significa que o leilão se encerrou, o que ocorre somente quando é exibido "Arrematado!". No estado "Verificando..." os usuários ainda podem dar lances, pois o botão de lance não está bloqueado, mas não necessariamente haverá tempo suficiente para o lance ser computado por nossos servidores. Frisamos ainda que, devido a pequenos atrasos de comunicação entre nossos servidores e seus computadores, o "Verificando..." pode não chegar a ser exibido, mostrando-se diretamente o "Arrematado!". Ou seja, para sua segurança, aconselhamos que não deixe para dar o lance nos últimos segundos!</p>
    <p><strong>Internet</strong>: É importante frisar que não nos responsabilizamos pela Internet local de nossos usuários, que frequentemente apresenta falhas e irregularidades. Parte do software do sistema é executado em seu próprio computador e outra depende da Internet para funcionar. Assim, mesmo que você perca momentaneamente a conexão com a Internet o cronômetro continua funcionando localmente, podendo passar de 1s, sendo então exibida a palavra "Verificando...". Caso esta palavra seja exibida na tela por mais de um ou dois segundos, provavelmente houve alguma interrupção momentânea em sua Internet local. Neste caso, sugerimos pressionar F5 ou CTRL-R para recarregar a página no seu navegador e as informações serão atualizadas, caso sua conexão já tenha sido reestabelecida.</p>
    <p><strong>Estratégia</strong>: Você perceberá muitas estratégias diferentes empregadas pelos diversos usuários, e deve empregar a que melhor se adequar a seu estilo, personalidade e quantidade de lances. Muito cuidado com o "deixa que eu deixo"! Por vezes há muitos usuários participando de um leilão, e todos esperam que um dos demais dê um lance, o que às vezes não ocorre, encerrando-se o leilão e vencendo aquele que deu o último lance.</p>
    <p style="font-size: 10px">* Muito embora a maioria dos pagamentos leve poucos minutos para aprovação e processamento, em alguns casos pode demorar até 4 (quatro) dias úteis. Os pagamentos realizados por Boleto Bancário levam até 48 horas úteis para compensação bancária.</p>
</div>
<?php /*
<a name="tipos"></a>
<div class="box">
	<div class="blueHeader">Quais são os tipos de leilões?</div>

	<p>Além das características básicas acima mencionadas, os leilões poderão ser de <b>até</b> 4 tipos:</p>
	<p>Vale Desconto: Todos os usuários que derem a quantidade mínima de BIDs (lances) especificada no respectivo leilão terão o direito a um desconto a ser utilizado na aquisição de produtos junto ao parceiro do leilão ou junto ao ApetreXo Leilão, conforme o caso. Mais detalhes, como o percentual do desconto, são especificados em cada leilão.</p>
	<p>Frete Grátis: O usuário vencedor do leilão não precisará pagar pelo frete.</p>
	<p>Preço Limitado: estipula-se um valor máximo para o arremate. Quando o valor máximo for atingido, o preço final do produto não aumentará, mesmo que sejam dados novos BIDs (lances), hipótese em que o leilão continuará normalmente até que o cronômetro chegue a zero! O valor máximo é especificado em cada leilão desta modalidade.</p>
</div>

<a name="autobid"></a>
<div class="box">
	<div class="blueHeader">Como funciona o AutoBid?</div>
	<p>O AutoBid ainda está em fase de implementação pelo ApetreXo Leilão. No entanto, a idéia é que o AutoBid permita ao usuário dar BIDs (lances), de forma automática, sem que o mesmo esteja presente na hora do leilão. A quantidade máxima de lances e os preços de ativação e desligamento do AutoBid são escolhidos pelo usuário e são de sua inteira responsabilidade.</p>
</div>
*/ ?>