<div id="conteudoTitulo">
	<h2>Manutenção</h2>
    <div style="clear: both;"></div>
</div><!-- conteudoTitulo -->

<div class="topBox">
	<div class="topBoxMSG" style="position: absolute; left: 30px; top: 20px;">Prezados usuários,</div>
	<p class="s16 cinza" style="position: absolute; margin: 50px 30px;">
    	estamos realizando uma atualização preventiva em nossos servidores.<br />
        Estaremos de volta em breve.<br/>
        <br/>
        Atenciosamente,<br/>
        Equipe ApetreXo Leilão.</p>
	<div id="topBoxIMG"></div>
</div>