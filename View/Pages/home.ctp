<h2>Desenvovimento de telas</h2>

<p><span class="notice">Verifique se há algum erro na instalação pelas mensagens no final da página.</span></p>
<p>
	Para visualizar outras páginas, complete o endereço com o controlador e o método. (ex: /pages/how_it_works (controlador: pages, método: how_it_works)<br/>
	Ou utilize o formulário abaixo.
</p>
<?php
$views_path = App::path('View');
$view_path = $views_path[0];
$views = array();
if ($handle = opendir($view_path)) {
	while (false !== ($entry = readdir($handle))) {
		if (/*$entry != "." && $entry != ".." &&*/ !in_array($entry[0], array('.', '_')) && !in_array($entry, array('Elements', 'Emails', 'Helper', 'Layouts'))) {
			$view = array();
			if ($handle2 = opendir($view_path .DS. $entry)) {
				while (false !== ($entry2 = readdir($handle2))) {
					if (/*$entry2 != "." && $entry2 != ".." &&*/ !in_array($entry2[0], array('.', '_'))) {
						if (!is_dir($entry2)) {
							if (($dot_pos = strrpos($entry2, '.', 1)) !== FALSE && ($entry2_name = substr($entry2, 0, $dot_pos)) != "")
								//array_push($view, substr($entry2, 0, $dot_pos));
								$view[$entry ."___". $entry2_name] = Inflector::humanize($entry2_name);
							else
								//array_push($view, $entry2);
								$view[$entry ."___". $entry2] = Inflector::humanize($entry2);
						}
					}
				}
			}
			closedir($handle2);
			if (count($view))
				if (isset($views[$entry]))
					$views[$entry] += $view;
				else
					$views[$entry] = $view;
		}
	}
	closedir($handle);
}

// echo "<ul>";
// foreach ($views as $key => $value) {
// 	echo "<li>";
// 	echo $key;
// 	if (count($value)) {
// 		echo "<ul>";
// 		foreach ($value as $value2) {
// 			echo "<li>";
// 			echo $value2;
// 			echo "</li>";
// 		}
// 		echo "</ul>";
// 	}
// 	echo "</li>";
// }

echo $this->Form->create(NULL, array('url' => array('action' => 'test_view')));
echo $this->Form->input('Changeview.test_controller_action', array('label' => 'Controlador', 'options' => $views)); //ChangeviewTestControllerAction
//echo $this->Form->input('Changeview.test_controller', array('label' => 'Controlador', 'options' => array_keys($views))); //ChangeviewTestController
//echo $this->Form->input('Changeview.test_action', array('label' => 'Método', 'options' => array('Selecione um Controlador'))); //ChangeviewTestAction
echo $this->Form->input('Changeview.test_layout', array('label' => 'Layout', 'options' => array('default' => 'default', 'default2' => 'default2', 'error' => 'error', 'meu' => 'meu'), 'default' => 'default'));
echo $this->Form->input('Changeview.test_flash_message', array('label' => 'Incluir mensagem?', 'options' => array(0 => 'Não', 'flash_error' => 'erro', 'flash_success' => 'sucesso')));
echo $this->Form->input('Changeview.test_auth_message', array('label' => 'Incluir erro de login?', 'type' => 'checkbox'));
echo $this->Form->submit('Enviar');
echo $this->Form->end();
?>

<?php
App::uses('Debugger', 'Utility');
if (Configure::read('debug') > 0):
	Debugger::checkSecurityKeys();
endif;
?>
<p><?php
	if (version_compare(PHP_VERSION, '5.2.8', '>=')):
		echo '<span class="notice success">';
			echo __d('cake_dev', 'Your version of PHP is 5.2.8 or higher.');
		echo '</span>';
	else:
		echo '<span class="notice">';
			echo __d('cake_dev', 'Your version of PHP is too low. You need PHP 5.2.8 or higher to use CakePHP.');
		echo '</span>';
	endif;
?>
</p>
<p><?php
	if (is_writable(TMP)):
		echo '<span class="notice success">';
			echo __d('cake_dev', 'Your tmp directory is writable.');
		echo '</span>';
	else:
		echo '<span class="notice">';
			echo __d('cake_dev', 'Your tmp directory is NOT writable.');
		echo '</span>';
	endif;
?>
</p>
<p><?php
	$settings = Cache::settings('_cake_core_');
	if (!empty($settings)):
		echo '<span class="notice success">';
				echo __d('cake_dev', 'The %s is being used for core caching. To change the config edit APP/Config/core.php ', '<em>'. $settings['engine'] . 'Engine</em>');
		echo '</span>';
	else:
		echo '<span class="notice">';
			echo __d('cake_dev', 'Your cache is NOT working. Please check the settings in APP/Config/core.php');
		echo '</span>';
	endif;
?>
</p>
<p><?php
	$filePresent = null;
	if (file_exists(APP . 'Config' . DS . 'database.php')):
		echo '<span class="notice success">';
			echo __d('cake_dev', 'Your database configuration file is present.');
			$filePresent = true;
		echo '</span>';
	else:
		echo '<span class="notice">';
			echo __d('cake_dev', 'Your database configuration file is NOT present.');
			echo '<br/>';
			echo __d('cake_dev', 'Rename APP/Config/database.php.default to APP/Config/database.php');
		echo '</span>';
	endif;
?>
</p>
<?php
if (isset($filePresent)):
	App::uses('ConnectionManager', 'Model');
	try {
		$connected = ConnectionManager::getDataSource('default');
	} catch (Exception $e) {
		$connected = false;
	}
?>
<p><?php
		if ($connected && $connected->isConnected()):
			echo '<span class="notice success">';
	 			echo __d('cake_dev', 'Cake is able to connect to the database.');
			echo '</span>';
		else:
			echo '<span class="notice">';
				echo __d('cake_dev', 'Cake is NOT able to connect to the database.');
			echo '</span>';
		endif;
	?>
</p>
<?php endif;?>
<?php
	App::uses('Validation', 'Utility');
	if (!Validation::alphaNumeric('cakephp')) {
		echo '<p><span class="notice">';
		__d('cake_dev', 'PCRE has not been compiled with Unicode support.');
		echo '<br/>';
		__d('cake_dev', 'Recompile PCRE with Unicode support by adding <code>--enable-unicode-properties</code> when configuring');
		echo '</span></p>';
	}
?>
