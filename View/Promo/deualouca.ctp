<div id="caminho">
<?php
	$this->Html->addCrumb('Comprar Lances');
	//$this->Html->addCrumb('Comprar Bids', '/pacotes');
?>
	<p><?php echo $this->element('crumb'); ?></p>
	<div style="clear: both;"></div>
</div><!-- caminho -->

<div id="conteudoTitulo">
	<h2>Comprar Lances</h2>
    <p class="s14" style="color: black;">Não perca essa promoção!!!</p>    
    <div style="clear: both;"></div>
</div><!-- conteudoTitulo -->

<div id="destaques" style="margin-bottom: 30px;">
	<div class="slider-wrapper theme-default">
		<div id="slider" class="nivoSlider">
<?php
	echo $this->Html->image("https://c14985174.ssl.cf2.rackcdn.com/img/banner/promodeualouca.jpg", array(
		'alt' => "Melhores preços!",
		//'url' => array('controller' => 'packages', 'action' => 'index')
	));
?>
		</div>
	</div>
</div>

<div class="box" style="width: 290px; float: right; padding: 30px">
	Após a confirmação da compra, o crédito dos lances será realizado de forma automática. 
	Utilizamos o sistema <a href="http://moip.com.br" target="_blank">MoIP</a> para processamento e análise de todos os pagamentos. 
	Não temos qualquer ingerência neste processo e, caso a compra fique retida para análise, a aprovação e o crédito de lances poderá demorar até 48 (quarenta e oito) horas. 
	Sugerimos que a compra de lances seja programada com antecedência.
</div>

<div class="box" style="width: 340px; float: left;">
    <div class="blueHeader s12" style="width: 320px; margin-bottom: 20px;">
        <span style="  display: inline-block; text-align: center;   padding-left: 10px;">PACOTES PROMOCIONAIS</span>
    </div>
    <div class="pacote">
        <p class="s24 boldBlack">25 lances</p>
        <p class="s24 cinza">R$ 25<span class="centavos">,00</span></p>
        <?php echo $this->Html->link('<div class="comprar"></div>', array('controller' => 'payments', 'action' => 'moip_buy', 'package', 8, 'meu' => true), array('escape' => false)); ?>
    </div>
    <div class="pacote">
        <p class="s24 boldBlack">50 lances</p>
        <p class="s24 cinza">R$ 45<span class="centavos">,00</span></p>
        <?php echo $this->Html->link('<div class="comprar"></div>', array('controller' => 'payments', 'action' => 'moip_buy', 'package', 9, 'meu' => true), array('escape' => false)); ?>
    </div>   
    <div class="pacote">
        <p class="s24 boldBlack">100 lances</p>
        <p class="s24 cinza">R$ 80<span class="centavos">,00</span></p>
        <?php echo $this->Html->link('<div class="comprar"></div>', array('controller' => 'payments', 'action' => 'moip_buy', 'package', 10, 'meu' => true), array('escape' => false)); ?>
    </div>                  
    <div class="pacote">
        <p class="s24 boldBlack">200 lances</p>
        <p class="s24 cinza">R$ 150<span class="centavos">,00</span></p>
        <?php echo $this->Html->link('<div class="comprar"></div>', array('controller' => 'payments', 'action' => 'moip_buy', 'package', 11, 'meu' => true), array('escape' => false)); ?>
    </div>                
    <div class="pacote">
        <p class="s24 boldBlack">300 lances</p>
        <p class="s24 cinza">R$ 220<span class="centavos">,00</span></p>
        <?php echo $this->Html->link('<div class="comprar"></div>', array('controller' => 'payments', 'action' => 'moip_buy', 'package', 12, 'meu' => true), array('escape' => false)); ?>
    </div>                  
    <div class="pacote">
        <p class="s24 boldBlack">500 lances</p>
        <p class="s24 cinza">R$ 360<span class="centavos">,00</span></p>
        <?php echo $this->Html->link('<div class="comprar"></div>', array('controller' => 'payments', 'action' => 'moip_buy', 'package', 13, 'meu' => true), array('escape' => false)); ?>
    </div>
    <div class="pacote">
        <p class="s24 boldBlack">1000 lances</p>
        <p class="s24 cinza">R$ 700<span class="centavos">,00</span></p>
        <?php echo $this->Html->link('<div class="comprar"></div>', array('controller' => 'payments', 'action' => 'moip_buy', 'package', 14, 'meu' => true), array('escape' => false)); ?>
    </div>    
</div>