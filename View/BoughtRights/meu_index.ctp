<?php
$this->Number->addFormat('BRL', array('before' => 'R$ ', 'thousands'=>'.', 'decimals'=>',', 'places' => 2, 'zero' => "R$ 0,00"));
?>

<div id="caminho">
  <?php $this->Html->addCrumb('Meu Cadastro', array('controller' => 'users', 'action' => 'index', 'meu' => false)); ?>
  <p><?php echo $this->element('crumb'); ?></p>
  <div style="clear: both;"></div>
</div><!-- caminho -->

<div id="conteudoTitulo">
	<h2>Meus Descontos</h2>
	<div style="clear: both;"></div>
</div><!-- conteudoTitulo -->

<?php if($boughtRights): ?>
<div class="box" style="margin-top:-15px; margin-bottom: 20px; padding-bottom: 10px;">
	<table cellpadding="0" cellspacing="3" width="100%"  style="margin-left:20px; margin-top:6px;">
	<tr>
			<th>Leilão</th>
			<th>Frete</th>
			<th>&nbsp;</th>
	</tr>
	<?php
	$i = 0;
	foreach ($boughtRights as $boughtRight): ?>
		<tr>
			<td>
			<?php
				echo $this->Html->link($this->Text->truncate($boughtRight['Auction']['title'], 100, array('ending' => '...', 'exact' => true, 'html' => false)), array('controller' => 'auctions', 'action' => 'view', $boughtRight['Auction']['slug']));
				echo "<br/>" . number_format((1 - ($boughtRight['BoughtRight']['price'] / $boughtRight['BoughtRight']['price_store']))*100, 2, ',', '') . "% de desconto. De " . $this->Number->currency($boughtRight['BoughtRight']['price_store'], 'BRL') . " por " . $this->Number->currency($boughtRight['BoughtRight']['price'], 'BRL');
			?>
			</td>
			<td><?php echo $this->Number->currency($boughtRight['BoughtRight']['shipping_price'], 'BRL'); ?>&nbsp;</td>
			<td align="center">
			<?php
				if ($boughtRight['BoughtRight']['bought']) {
					echo $this->Html->link("Comprado", array('controller' => "payments", 'action' => "moip_checkout", $boughtRight['BoughtRight']['id'], 'meu' => true, 'admin' => false), array('target' => "_blank")) ."<br/><font color=\"#666666\" size=\"1\">(em ". $this->Time->format('d/m/Y H:i', $boughtRight['BoughtRight']['bought_date']) . ")</font>";
				} elseif ($boughtRight['BoughtRight']['expired']) {
					echo "Expirado<br/><font color=\"#666666\" size=\"1\">(em ". $this->Time->format('d/m/Y H:i', $boughtRight['BoughtRight']['due_date']) . ")</font>";
				} else {
					echo $this->Html->link($this->Html->image("https://c14985174.ssl.cf2.rackcdn.com/img/btn_comprar.png", array('width' => "90", "height" => "30")), array('controller' => 'payments', 'action' => 'moip_buy', 'bought_right', $boughtRight['BoughtRight']['id'], 'meu' => true), array('escape' => false)) . "<br/><font color=\"#666666\" size=\"1\">(até ". $this->Time->format('d/m/Y H:i', $boughtRight['BoughtRight']['due_date']) . ")</font>";
				}
			?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
</div>
<?php else: ?>
<div class="box">
	<p style="margin:15px;">Você não possui descontos.</p>
</div><!-- box -->
<?php endif; ?>