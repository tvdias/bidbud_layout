<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>BidBud :: O leilão amigo dos seus sonhos</title>
    <style>
		* {
			 font-family: Tahoma, Geneva, sans-serif;
			 font-size: 14px;
		}
	</style>
</head>

<body>
<center>
<table width="600" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
		<td height="50" style="min-height: 50px;">
			<a href="<?php echo empty($data['Email']['header_link']) ? "http://www.bidbud.com.br" : $data['Email']['header_link']; ?>" style="border-width: 0px; text-decoration: none;">
				<?php
				if(empty($data['Email']['header_img']))
					$data['Email']['header_img'] = "header.jpg";
				echo "<img src=\"http://www.bidbud.com.br/img/email/new/". $data['Email']['header_img'] ."\" alt=\"BidBud - O leilão amigo dos seus sonhos\" width=\"600\" border=\"0\" />";
				?>
			</a>
		</td>
	</tr>
	<?php if(!empty($data['Email']['tagline'])): ?>
	<tr>
		<td height="50" bgcolor="<?php echo empty($data['Email']['tagline_bgcolor']) ? "#3399CC" : $data['Email']['tagline_bgcolor']; ?>" style="min-height: 50px;">
			<?php echo $data['Email']['tagline']; ?>
		</td>
	</tr>
	<tr>
		<td height="15" style="height: 15px;">&nbsp;</td>
	</tr>
	<?php
	endif;
	if(!empty($data['Email']['message'])) {
	?>
	<tr>
		<td style="min-height: 150px;">
			<p>Olá [name],</p>
			<?php
			echo $data['Email']['message'];

			echo empty($data['Email']['ending']) ? "<p>Atenciosamente,<br />Equipe BidBud.</p>" : $data['Email']['ending'];
			?>
		</td>
	</tr>
	<tr>
		<td height="15" style="height: 15px;">&nbsp;</td>
	</tr>
	<?php
	} elseif(!empty($data['Email']['message_img'])) {
		echo '<tr><td><img src="'.$data['Email']['message_img'].'" alt="" width="600" border="0" /></td></tr>';
	}
	if(isset($data['auto_auctions'])):
	?>
	<tr>
		<td>
			<img src="http://www.bidbud.com.br/img/email/new/destaques.jpg" alt="Leilões em Destaque" width="600" height="40" border="0" />
		</td>
	</tr>
	<tr>
		<td>
			<table width="600" cellpadding="0" cellspacing="0" style="width:600px;">
				<tr>
<?php
$auctions_per_line = 3;
$auctions_i = 1;
foreach ($data['auto_auctions'] as $auction):
//for($auctions_i = 1; $auctions_i<5; $auctions_i++) {
	$auction_title = $text->truncate($auction['Auction']['title'], 50, "...", true, false);
	$image_prod = $image->image_out($auction['Item']['Image'], null, "medium", array( 'full_link' => true, 'border' => '0', 'width' => '100', 'height' => '75', 'clear' => true, 'alt' => '', 'empty_image' => 'photo1.jpg' )); //$image->image_out($auction['Item']['Image'][0], null, "medium", array('style' => 'width: 100px; height: 75px', 'alt' => $auction['Auction']['title'], 'title' => $auction['Auction']['title'], 'clear' => true, 'empty_image' => 'photo1.jpg' ));
	$image_seller = $image->avatar($auction['Item']['user_id'], 'seller_small', array( 'full_link' => true, 'clear' => true, 'style' => 'width: 90px; height: 30px', 'alt' => $auction['Item']['User']['username'], 'empty_image' => 'photo1.jpg' ));
	$auction_link_array = Router::url(array('controller' => 'auctions', 'action' => 'view', $auction['Auction']['slug']), true);
	$auction_date = strtotime($auction['Auction']['end_time']);

	if ($auctions_i%$auctions_per_line == 0) { echo '<td height="250" width="194" align="right" style="width:190px; height: 250px; text-align: right;">';
	} else {
?>
					<td height="250" width="203" style="width:203px; height: 250px">
<? } ?>
						<div class="box" style="border: 1px solid gray; border-radius: 10px; width: 190px; margin: 0px;">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td align="center" valign="middle" height="30" style="height:30px; color: #006699; font: bold 12px Arial; text-decoration: none; padding: 5px;"><?php echo $html->link($auction_title, $auction_link_array, array('style' => "color: #006699; font: bold 12px Arial; text-decoration: none"), false, false); ?></td>
								</tr>
								<tr>
									<td align="center" valign="middle">
										<?php echo $html->link($image_prod, $auction_link_array, array(), false, false); ?>
									</td>
								</tr>
								<tr>
									<td align="center" valign="middle" style="padding-bottom: 10px;">
										<?php echo $html->link($image_seller, $auction_link_array, array('width' => 90, 'height' => 30), false, false); ?>
									</td>
								</tr>
								<tr>
									<td align="center" valign="middle" style="background-color: #CCC; height: 35px; color:#333; font-size: 12px; font-weight: bold; margin: 0px;">
										<?php echo $html->link(date('d/m/Y - H:i\h\s', $auction_date), $auction_link_array, array('style' => "font: 14px Arial; color: #777777; text-decoration: none; padding: 5px;"), false, false); ?>
									</td>
								</tr>
								<tr>
									<td align="center" valign="middle" style="padding: 10px;">
										<?php echo $html->image("https://c14985174.ssl.cf2.rackcdn.com/img/btn_ver_no_site.png", array('alt' => 'Ver no site', 'url' => $auction_link_array, 'style' => "width:107px;height: 27px;")); ?></td>
								</tr>
							</table>
						</div>
					</td>
<?php
	if ($auctions_i%$auctions_per_line == 0) { echo "</tr><tr>";}
	$auctions_i++;
endforeach;
$auctions_i--;
if ($auctions_i%$auctions_per_line > 0) { echo '<td colspan="'. ($auctions_per_line-($auctions_i%$auctions_per_line)) .'">&nbsp;</td></tr>';}
else '<td colspan="'.$auctions_per_line.'">&nbsp;</td></tr>';
?>
			</table>
		</td>
	</tr>
	<tr>
		<td height="10" style="height:10px;">&nbsp;</td>
	<tr>
	<?php endif; ?>
	<tr>
		<td>
			<a href="http://www.bidbud.com.br/parceiros" style="border-width: 0px; text-decoration: none;"><img src="http://www.bidbud.com.br/img/email/new/conheca.jpg" width="600" alt="Nao arrematou? Aproveite os descontos especiais nos nossos parceiros!" /></a>
		</td>
	</tr>
	<tr>
		<td height="30" style="height: 30px">&nbsp;</td>
	</tr>
	<?php if(isset($data['Email']['show_categories']) & $data['Email']['show_categories'] == 1): ?>
	<tr>
		<td>
			<table width="600" cellpadding="0" cellspacing="0">
				<tbody>
				<tr>
					<td><h2 style="color:#3399ff; font-weight: bold; font-family: Tahoma, Geneva, sans-serif;">Não encontrou o que estava procurava?</h2></td>
				</tr>
				<tr>
					<td align="justify" style="text-align: justify; color: #666;">Acesse o BidBud e procure pelo produto dos seus sonhos! São diversas categorias: <a href="http://www.bidbud.com.br/informatica">Informática</a>, <a href="http://www.bidbud.com.br/eletronicos">Eletrônicos</a>, <a href="http://www.bidbud.com.br/games">Games</a>, <a href="http://www.bidbud.com.br/eletrodomesticos">Eletrodomésticos</a>, <a href="http://www.bidbud.com.br/cine-e-foto">Cine e Foto</a>, <a href="http://www.bidbud.com.br/turismo">Turismo</a>, <a href="http://www.bidbud.com.br/artigos-pessoais">Artigos pessoais</a>, <a href="http://www.bidbud.com.br/alimentos-e-bebidas">Bebidas</a> e <a href="http://www.bidbud.com.br">muito mais</a>!</td>
				</tr>
				</tbody>
			</table>
		</td>
	</tr>
	<tr>
		<td height="10" style="height:10px;">&nbsp;</td>
	<tr>
	<?php
	endif;
	if(isset($data['Email']['show_how_to_buy']) & $data['Email']['show_how_to_buy'] == 1):
	?>
	<tr>
		<td>
			<table width="600" cellpadding="0" cellspacing="0">
				<tbody>
				<tr>
					<td><h2 style="color:#3399ff; font-weight: bold; font-family: Tahoma, Geneva, sans-serif;">Como comprar no BidBud</h2></td>
				</tr>
				<tr>
					<td align="justify" style="text-align: justify; color: #666;">O BidBud funciona como o canal de vendas das lojas que você já conhece, mas no sistema de leilão de centavos. Ou seja, você sabe que pode confiar!<br />
Aqui você participa de leilões de produtos em que cada lance, ou BID, corresponde a um centavo de acréscimo. O sistema dá a possibilidade de você adquirir produtos por valores muito inferiores aos praticados no mercado. E se não conseguir arrematar, pode ganhar desconto* para efetuar a compra <span style="text-decoration: underline;">direto no site do parceiro</span>.</td>
				</tr>
				</tbody>
			</table>
		</td>
	</tr>
	<tr>
		<td height="10" style="height:10px;">&nbsp;</td>
	<tr>
	<?php
	endif;
	if(isset($data['Email']['show_can_i_trust']) & $data['Email']['show_can_i_trust'] == 1):
	?>
	<tr>
		<td>
			<table width="600" cellpadding="0" cellspacing="0">
				<tbody>
				<tr>
					<td><h2 style="color:#3399ff; font-weight: bold; font-family: Tahoma, Geneva, sans-serif;">Posso Confiar no BidBud</h2></td>
				</tr>
				<tr>
					<td align="justify" style="text-align: justify; color: #666;">
						O BidBud funciona como o canal de vendas das lojas que você já conhece, mas no sistema de leilão de centavos. Ou seja, você sabe que pode confiar!<br />
						Aqui você participa de leilões de produtos em que cada lance, ou BID, corresponde a um centavo de acréscimo. O sistema dá a possibilidade de você adquirir produtos por valores muito inferiores aos praticados no mercado. E se não conseguir arrematar, pode ganhar desconto* para efetuar a compra <font style="text-decoration: underline;">direto no site do parceiro</font>.
				  </td>
				</tr>
				</tbody>
			</table>
		</td>
	</tr>
	<tr>
		<td height="10" style="height:10px;">&nbsp;</td>
	<tr>
	<?php
	endif;
	if(isset($data['Feedback'])):
		$feedback = $data['Feedback'];
		$feedback_image = $image->image_out($feedback['Image']['dir'], $feedback['Image']['filename'], 'medium', array('width' => "200", 'height' => "200", 'style' => 'width: 200px; height: 200px'));
	?>
	<tr>
		<td>
			<table width="600" cellpadding="0" cellspacing="0" style="width:600px;">
				<tr>
					<td>
						<div style="float:left;width:200px;height:200px;margin: 0 10px 10px 0;">
							<?php echo $feedback_image; ?>
                        </div>
						<h2 style="color:#3399ff; font-weight: bold; font-family: Tahoma, Geneva, sans-serif;"><?php echo $feedback['User']['username'] ?></h2>
                        <p style="text-align: justify; color: #666;"><?php echo $feedback['Feedback']['text'] ?></p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td height="10" style="height:10px;">&nbsp;</td>
	<tr>
	<?php
	endif;
	if(!empty($data['Email']['footer_img'])):
	?>
	<tr>
		<td height="50" style="min-height: 50px;">
			<a href="<?php echo empty($data['Email']['footer_link']) ? "http://www.bidbud.com.br" : $data['Email']['footer_link']; ?>" style="border-width: 0px; text-decoration: none;">
				<?php
				if ($data['Email']['footer_img'] == 'sah')
					$data['Email']['footer_img'] = $data['Email']['header_img'];
				echo '<img src="http://www.bidbud.com.br/img/email/new/'. $data['Email']['footer_img'] .'" alt="BidBud - O leilão amigo dos seus sonhos" width="600" border="0" />'; ?>
			</a>
		</td>
	</tr>
	<?php
	endif;
	?>
	<tr>
		<td>
		    <a href="http://www.bidbud.com.br/" style="border-width: 0px; text-decoration: none;"><img src="http://www.bidbud.com.br/img/email/new/footer.jpg" width="600" height="40" alt="BidBud - O leilão amigo dos seus sonhos" /></a></td>
	</tr>
	<tr>
		<td align="justify" style="font-size: 9px; text-align:justify;">
			O BidBud oferece descontos especiais para produtos não arrematados direto nas lojas parceiras para alguns dos produtos leiloados no site. Verifique se o produto desejado se enquadra nessa modalidade de oferta. Todos os produtos são novos e com nota fiscal. Alguns pacotes promocionais estão limitados a apenas UMA compra por usuário. Horários sujeito à alterações. Imagens meramente ilustrativas. 
		</td>
	</tr>
	</tbody>
</table>
</center>
</body>
</html>