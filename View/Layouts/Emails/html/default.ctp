<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ApetreXo Leilão</title>
    <style>
		* {
			 font-family: Tahoma, Geneva, sans-serif;
			 font-size: 14px;
		}
	</style>
</head>

<body>
<table width="600" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
		<td height="50" style="min-height: 50px;">
			<a href="<?php echo (!isset($email_options['header_link']) || empty($email_options['header_link'])) ? "http://www.apetrexoleilao.com.br" : $email_options['header_link']; ?>" style="border-width: 0px; text-decoration: none;">
				<?php
				if (!isset($email_options['header_img']) || empty($email_options['header_img'])) {
					$email_options['header_img'] = "topo.jpg";
				}
				echo "<img src=\"https://c14985174.ssl.cf2.rackcdn.com/img/email/". $email_options['header_img'] ."\" alt=\"ApetreXo Leilão\" width=\"600\" border=\"0\" />";
				?>
			</a>
		</td>
	</tr>
	<?php if (isset($email_options['tagline']) && !empty($email_options['tagline'])) { ?>
	<tr>
		<td height="50" bgcolor="<?php echo (!isset($email_options['tagline_bgcolor']) || empty($email_options['tagline_bgcolor'])) ? "#3399CC" : $email_options['tagline_bgcolor']; ?>" style="min-height: 50px;">
			<?php echo $email_options['tagline']; ?>
		</td>
	</tr>
	<tr>
		<td height="15" style="height: 15px;">&nbsp;</td>
	</tr>
	<?php } ?>
	<tr>
		<td style="min-height: 150px;">
			<?php
				echo $content_for_layout;

				echo (!isset($email_options['ending']) || empty($email_options['ending'])) ? "<p>Atenciosamente,<br />Equipe ApetreXo Leilão.</p>" : $email_options['ending'];
			?>
		</td>
	</tr>
	<tr>
		<td height="15" style="height: 15px;">&nbsp;</td>
	</tr>
	<?php if(isset($auto_auctions) && !empty($auto_auctions)) { ?>
	<tr>
		<td>
			<table width="600" cellpadding="0" cellspacing="0" style="width:600px;">
				<tr>
<?php
$auctions_per_line = 3;
$auctions_i = 1;
foreach($auto_auctions as $auction) {
	$auction_title = $this->Text->truncate($auction['Auction']['title'], 50, array('ending' => "...", 'exact' => true, 'html' => false));
	$image_prod = $this->Image->image_out($auction['Item']['Image'], null, "medium", array( 'full_link' => true, 'border' => '0', 'width' => '100', 'height' => '75', 'clear' => true, 'alt' => '', 'empty_image' => 'photo1.jpg' )); //$image->image_out($auction['Item']['Image'][0], null, "medium", array('style' => 'width: 100px; height: 75px', 'alt' => $auction['Auction']['title'], 'title' => $auction['Auction']['title'], 'clear' => true, 'empty_image' => 'photo1.jpg' ));
	//$image_seller = $this->Image->store($auction['Auction']['store_id'], 'small', array( 'clear' => true, 'style' => 'width: 90px; height: 30px', 'alt' => $auction['Store']['name'], 'empty_image' => 'photo1.jpg' ));
	//$auction_link_array = Router::url(array('controller' => 'auctions', 'action' => 'view', $auction['Auction']['slug'], 'admin' => false, 'meu' => false), true);
	$auction_link_array = "http://www.apetrexoleilao.com.br" . Router::url(array('controller' => 'auctions', 'action' => 'view', $auction['Auction']['slug'], 'admin' => false, 'meu' => false), false);
	$auction_date = strtotime($auction['Auction']['end_time']);
?>
					<td <?php echo ($auctions_i%$auctions_per_line == 0) ? 'height="250" width="194" align="right" style="width:190px; height: 250px; text-align: right;"' : 'height="250" width="203" style="width:203px; height: 250px"'; ?>>
						<div class="box" style="border: 1px solid gray; border-radius: 10px; width: 190px; margin: 0px;">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td align="center" valign="middle" height="40" style="height:40px; color: #006699; font: bold 12px Arial; text-decoration: none; padding: 5px;"><?php echo $this->Html->link($auction_title, $auction_link_array, array('escape' => false, 'style' => "color: #006699; font: bold 12px Arial; text-decoration: none")); ?></td>
								</tr>
								<tr>
									<td align="center" valign="middle">
										<?php echo $this->Html->link($image_prod, $auction_link_array, array('escape' => false)); ?>
									</td>
								</tr>
								<?php /*
								<tr>
									<td align="center" valign="middle" style="padding-bottom: 10px;">
										<?php echo $this->Html->link($image_seller, $auction_link_array, array('escape' => false, 'width' => 90, 'height' => 30)); ?>
									</td>
								</tr>
								*/ ?>
								<tr>
									<td align="center" valign="middle" style="background-color: #CCC; height: 35px; color:#333; font-size: 12px; font-weight: bold; margin: 0px;">
										<?php echo $this->Html->link(date('d/m/Y - H:i\h\s', $auction_date), $auction_link_array, array('escape' => false, 'style' => "font: 14px Arial; color: #777777; text-decoration: none; padding: 5px;")); ?>
									</td>
								</tr>
								<tr>
									<td align="center" valign="middle" style="padding: 10px;">
										<?php echo $this->Html->image("https://c14985174.ssl.cf2.rackcdn.com/img/btn_ver_no_site.png", array('escape' => false, 'alt' => 'Ver no site', 'url' => $auction_link_array, 'style' => "width:107px;height: 27px;")); ?></td>
								</tr>
							</table>
						</div>
					</td>
<?php
	if ($auctions_i%$auctions_per_line == 0) {
		echo "</tr><tr>";
	}
	$auctions_i++;
}
$auctions_i--;
if ($auctions_i%$auctions_per_line > 0) {
	echo '<td colspan="'. ($auctions_per_line-($auctions_i%$auctions_per_line)) .'">&nbsp;</td></tr>';
} else {
	'<td colspan="'.$auctions_per_line.'">&nbsp;</td></tr>';
}
?>
			</table>
		</td>
	</tr>
	<tr>
		<td height="10" style="height:10px;">&nbsp;</td>
	</tr>
	<?php } ?>
	<tr>
		<td height="30" style="height: 30px">&nbsp;</td>
	</tr>
	<?php
	if(isset($auto_feedbacks) && !empty($auto_feedbacks)) {
		$feedback = $auto_feedbacks[0];
		$feedback_image = $image->image_out($feedback['Image']['dir'], $feedback['Image']['filename'], 'medium', array('width' => "200", 'height' => "200", 'style' => 'width: 200px; height: 200px'));
	?>
	<tr>
		<td>
			<table width="600" cellpadding="0" cellspacing="0" style="width:600px;">
				<tr>
					<td>
						<div style="float:left;width:200px;height:200px;margin: 0 10px 10px 0;">
							<?php echo $feedback_image; ?>
                        </div>
						<h2 style="color:#3399ff; font-weight: bold; font-family: Tahoma, Geneva, sans-serif;"><?php echo $feedback['User']['username'] ?></h2>
                        <p style="text-align: justify; color: #666;"><?php echo $feedback['Feedback']['text'] ?></p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td height="10" style="height:10px;">&nbsp;</td>
	<tr>
	<?php
	}

	if (isset($email_options['footer_img']) && !empty($email_options['footer_img'])) {
	?>
	<tr>
		<td height="50" style="min-height: 50px;">
			<a href="<?php echo (!isset($email_options['footer_link']) || empty($email_options['footer_link'])) ? "http://www.apetrexoleilao.com.br" : $email_options['footer_link']; ?>" style="border-width: 0px; text-decoration: none;">
				<?php
				if ($email_options['footer_img'] == 'sah') {
					$email_options['footer_img'] = $email_options['header_img'];
				}
				echo '<img src="https://c14985174.ssl.cf2.rackcdn.com/img/email/'. $email_options['footer_img'] .'" alt="ApetreXo Leilão" width="600" border="0" />'; ?>
			</a>
		</td>
	</tr>
	<?php } ?>
	<tr>
		<td>
		    <a href="http://www.apetrexoleilao.com.br/" style="border-width: 0px; text-decoration: none;"><img src="https://c14985174.ssl.cf2.rackcdn.com/img/email/rodape.jpg" width="600" height="40" alt="ApetreXo Leilão" /></a></td>
	</tr>
	<tr>
		<td align="justify" style="font-size: 9px; text-align:justify;">
			O ApetreXo Leilão oferece descontos especiais para produtos não arrematados. Verifique se o produto desejado se enquadra nessa modalidade de oferta. Todos os produtos são novos e com nota fiscal. Alguns pacotes promocionais estão limitados a apenas UMA compra por usuário. Horários sujeito à alterações. Imagens meramente ilustrativas. 
		</td>
	</tr>
	</tbody>
</table>

</body>
</html>