<?php
$isUserLogged = false;
if ($isUserLogged) {
	$user = array(
			'User' => array(
					'username' => "Nome_do_Usuário",
					'adm_access' => false,
					'AdditionalData' => array(
							'balance' => 9999,
							'balance_exp' => 9999,
					)
	));
	$admin = array(
			'User' => array(
					'username' => "Nome_do_Usuário",
					'adm_access' => true,
					'AdditionalData' => array(
							'balance' => 9999,
							'balance_exp' => 9999,
					)
	));
	$authUser = $user;
}
//$sidebarLayout = 'sidebar_auction';
$sidebar_auctions = array();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<?php echo $this->Html->charset(); ?>
		<title>ApetreXo Leilão :: <?php echo empty($title_for_layout) ? "Celular, Smartphone, Iphone, Ipad, Notebook, TV LED e muito mais" : htmlspecialchars($title_for_layout); ?></title>
		<meta name="alexaVerifyID" content="" />
		<meta name="language" content="PT-BR" />
		<meta name="title" content="<?php echo isset($pagemeta['title']) ? htmlspecialchars($pagemeta['title']) : "ApetreXo Leilão :: Celular, Smartphone, Iphone, Ipad, Notebook, TV LED e muito mais"; ?>" />
		<meta name="description" content="<?php echo isset($pagemeta['description']) ? htmlspecialchars($pagemeta['description']) : "O ApetreXo Leilão é o site de leilão de centavos do ApetreXo. Aqui você pode arrematar produtos incríveis por um preço muito baixo e tem a confiança que só o ApetreXo pode dar. Cadastre-se e ganhe 5 lances!"; ?>" />
		<meta NAME="comment" content="<?php echo isset($pagemeta['comment']) ? htmlspecialchars($pagemeta['comment']) : "O ApetreXo Leilão é o site de leilão de centavos do ApetreXo. Aqui você pode arrematar produtos incríveis por um preço muito baixo e tem a confiança que só o ApetreXo pode dar. Cadastre-se e ganhe 5 lances!"; ?>" />
		<meta name="keywords" content="<?php if(isset($pagemeta['keywords'])) echo htmlspecialchars($pagemeta['keywords'])." "; ?>apetrexo site de leilão de centavos site de leilao de centavos site de leilão online site de leiloes site de leilões online leilão virtual 1 centavo leilao virtual 1 centavo lance de 1 centavo leilão 1 centavo leilao 1 centavo leilão de 1 centavo sites de leilão 1 centavo sites de leilao 1 centavo leilão na internet leilão 0.01 leiloes online leilão on line leilao on line leilões on line leiloes on line" />
		<meta name="revisit-after" content="1 day" />
		<meta name="robots" content="index, follow" />
		<meta name="googlebot" content="index, follow" />
	<?php
		echo $this->Html->meta('favicon.ico', 'https://c14985174.ssl.cf2.rackcdn.com/favicon.ico', array('type' => 'icon')); //echo $this->Html->meta('icon');

		echo $this->Html->css('style.css');
		echo $this->Html->script(array('https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js', 'scripts.js', 'ease.min.js', 'plugins.color.min.js'));

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
	</head>
	<body>
		<div id="mainContainer">
			<div id="header">
<div id="categoriasSubMenu"  style="display: none;">
	<div class="btn_sub" id="todasCategorias">
		<div>
			<div class="divisao">
				<h5>
                    <?php echo $this->Html->link("Beleza e Saúde", array("controller" => "auctions", "action" => "category", "beleza-saude", "meu" => false), array('escape' => false)); ?>
				</h5>
                <p></p>
					<h5 class="unico">
                 <?php echo $this->Html->link("Cine & Foto", array("controller" => "auctions", "action" => "category", "cinefoto", "meu" => false), array('escape' => false)); ?>
				</h5>
				<p></p>
				<h5 class="unico">
                 <?php echo $this->Html->link("Eletrônicos", array("controller" => "auctions", "action" => "category", "eletronicos", "meu" => false), array('escape' => false)); ?>
				</h5>
				<p></p>
                <h5 class="unico">
                 <?php echo $this->Html->link("Eletroportáteis", array("controller" => "auctions", "action" => "category", "eletroportateis", "meu" => false), array('escape' => false)); ?>
				</h5>
                <p></p>
                </div>
                <div class="divisao">
				<h5 class="unico">
                 <?php echo $this->Html->link("Esporte e Lazer", array("controller" => "auctions", "action" => "category", "esportelazer", "meu" => false), array('escape' => false)); ?>
				</h5>
                <p></p>
                <h5 class="unico">
                 <?php echo $this->Html->link("Games", array("controller" => "auctions", "action" => "category", "games", "meu" => false), array('escape' => false)); ?>
				</h5>
				<p></p>
                <h5 class="unico">
                 <?php echo $this->Html->link("Informática", array("controller" => "auctions", "action" => "category", "informatica", "meu" => false), array('escape' => false)); ?>
				</h5>
				<p></p><h5 class="unico">
                 <?php echo $this->Html->link("Relógios", array("controller" => "auctions", "action" => "category", "relogios", "meu" => false), array('escape' => false)); ?>
				</h5>
                <p></p>
                </div>
                <div class="divisao">
                <h5 class="unico">
                 <?php echo $this->Html->link("Telefonia", array("controller" => "auctions", "action" => "category", "telefonia", "meu" => false), array('escape' => false)); ?>
				</h5>
				<p></p>
                <h5 class="unico">
                 <?php echo $this->Html->link("Utilidades Domésticas", array("controller" => "auctions", "action" => "category", "utilidadesdomesticas", "meu" => false), array('escape' => false)); ?>
				</h5>
				<p></p>
                <h5 class="unico">
                 <?php echo $this->Html->link("Bebês e Gestantes", array("controller" => "auctions", "action" => "category", "bebegestante", "meu" => false), array('escape' => false)); ?>
				</h5>
				<p></p>
                <h5 class="unico">
                 <?php echo $this->Html->link("Diversão", array("controller" => "auctions", "action" => "category", "diversao", "meu" => false), array('escape' => false)); ?>
				</h5>
				<p></p>
			</div>
		</div>
	</div>
</div>

<div id="menu">
<?php
	echo $this->Html->link($this->Html->image("https://c14985174.ssl.cf2.rackcdn.com/img/menu/inicio.png", array("id" => "inicio", 'class' => "menuitem", 'alt' => 'Início')), "/", array('escape' => false));
	echo $this->Html->link($this->Html->image("https://c14985174.ssl.cf2.rackcdn.com/img/menu/categorias.png", array("id" => "categorias", 'class' => "menuitem", 'alt' => 'Categorias')), '#', array('escape' => false));
	echo $this->Html->link($this->Html->image("https://c14985174.ssl.cf2.rackcdn.com/img/menu/depoimentos.png", array("id" => "depoimentos", 'class' => "menuitem", 'alt' => 'Depoimentos')), array('controller' => 'feedbacks', 'action' => "index", 'meu' => false), array('escape' => false));
	echo $this->Html->link($this->Html->image("https://c14985174.ssl.cf2.rackcdn.com/img/menu/comofunciona.png", array("id" => "comofunciona", 'class' => "menuitem", 'alt' => 'Como Funciona')), array('controller' => 'pages', 'action' => 'display', 'how_it_works', 'meu' => false), array('escape' => false));
	echo $this->Html->link($this->Html->image("https://c14985174.ssl.cf2.rackcdn.com/img/menu/arrematados.png", array("id" => "arrematados", 'class' => "menuitem", 'alt' => 'Arrematados')), array('controller' => 'auctions', 'action' => 'endeds', 'meu' => false), array('escape' => false));
?>
</div>
<?php echo $this->Html->link("<div id=\"logo\">&nbsp;</div>", "/", array('escape' => false, "style" => "width: 350px; height: 95px")); ?>

<?php echo $this->Form->create(false, array('url' => array('controller' => 'auctions', 'action' => 'search', 'meu' => false), 'id' => 'mainSearch', 'type' => 'get'));?>
    <input type="text" id="AuctionTitle" name="text" value="<?php echo isset($auctionsSearchBox) ? $auctionsSearchBox : "Procurar um Leilão"; ?>" maxlength="255" />
	<input class="enviar" value="" type="submit" style="display:none" />
<?php echo $this->Form->end(); ?>

<!--nocache--><?php
echo $this->Html->scriptStart(array('inline' => true));
echo "var user_logged_in = true;";
echo $this->Html->scriptEnd();
?>
	<div id="comprarBids">
		<?php echo $this->Html->link($this->Html->image("https://c14985174.ssl.cf2.rackcdn.com/img/btn_comprar_lances.png", array('alt' => 'Comprar Lances')), array('controller' => 'packages', 'action' => 'index', 'meu' => false), array('escape' => false)); ?>
	</div>
	<?php if($this->layout != "meu"): ?>
	<div id="userlogged">
		<div class="userIMG">
			<?php //echo $this->Image->avatar($loggedUser['User']['id'], 'small', array( 'clear' => true, 'alt' => '')); ?>
			<?php //echo $this->Image->avatar_logged_user('small'); ?>
		</div>
		<div class="userLogin"><?php echo strtoupper($authUser['User']['username']); ?></div>
		<div id="nBids" class="nBids"><span class="commonBids"><?php echo $authUser['User']['AdditionalData']['balance']; ?></span> LANCES<?php if($authUser['User']['AdditionalData']['balance_exp']) echo " (<span class=\"expressBids\">". $authUser['User']['AdditionalData']['balance_exp'] ."</span> expirando)"; ?></div>
		<?php if($authUser['User']['adm_access']) echo $this->Html->link("Adm", array('admin' => true, 'controller' => 'users', 'action' => 'home'), array('class' => "meuBidBud", 'escape' => false));?>
		<?php echo $this->Html->link("Meu Cadastro", array('meu' => true, 'controller' => 'users', 'action' => 'index'), array('class' => "meuBidBud", 'escape' => false));?>
		<?php echo $this->Html->link("Sair", '/logout', array('class' => "meuBidBud", 'escape' => false));?>
	</div>
	<?php endif; ?>
	<div id="fl-bids" style="display: none;">
		<?php if($authUser['User']['adm_access']):
			echo $this->Html->link("Administração", array('admin' => true, 'controller' => 'users', 'action' => 'home'), array('class' => "fl-bids-meuBidBud", 'escape' => false));
			echo "<br />";
		else:
		?>
		<div id="fl-nBids" class="fl-bids-nBids">
			<span class="commonBids"><?php echo $authUser['User']['AdditionalData']['balance']; ?></span> LANCES<?php if($authUser['User']['AdditionalData']['balance_exp']) echo " (<span class=\"expressBids\">". $authUser['User']['AdditionalData']['balance_exp'] ."</span> expirando)"; ?>
		</div>
		<?php endif;
			echo $this->Html->link("Meu Cadastro", array('meu' => true, 'controller' => 'users', 'action' => 'index'), array('class' => "fl-bids-meuBidBud", 'escape' => false));
			echo " ";
			echo $this->Html->link("Topo", '#', array('class' => "fl-bids-meuBidBud topo", 'escape' => false));
		?>
	</div>
<?php
// echo $this->Html->scriptStart(array('inline' => true));
// $.ajax({
// 	url: '<php echo Router::url(array('controller' => 'bids', 'action' => 'balance', 'meu' => true), true); php>',
// 	dataType: 'text',
// 	timeout: 1000,
// 	async: true,
// 	global: false,
// 	cache: false,
// 	success: function(data){
//     	var userBalance = data.split('|');
//     	$(".commonBids").html(userBalance[0]);
//     	<php if($expBids) echo "$(\".expressBids\").html(userBalance[1]);"; php>
// 	},
// 	error: function(XMLHttpRequest, textStatus, errorThrown){
// 	}
// });
// echo $this->Html->scriptEnd();
?>
<!--/nocache-->
			</div><!-- header -->

			<div id="conteudoPrincipal">
				<?php echo $this->Session->flash(); ?>

				<?php echo $content_for_layout; ?>
			</div><!-- conteudoPrincipal -->

			<div id="sidebarMeuBidBud">
<div id="sideBarTop"></div>
<div id="sideBarMiddleMeuBidBud">
	<span style="display: inline-block; ">
		<div class="nomedousuarioMeuBidBud"><?php echo strtoupper($loggedUser['User']['username']); ?></div>
		<!--nocache-->
		<p id="nBids" class="s12 vermelho"><span class="commonBids"><?php echo $this->Session->read('Auth.User.AdditionalData.balance'); ?></span> LANCES<?php if($this->Session->read('Auth.User.AdditionalData.balance_exp')) echo " (<span class=\"expressBids\">". $this->Session->read('Auth.User.AdditionalData.balance_exp') ."</span> expirando)"; ?></p>
		<?php if($this->Session->read('Auth.User.adm_access')) echo $this->Html->link("Adm", array('admin' => true, 'meu' => false, 'controller' => 'users', 'action' => 'home'), array('class' => "underline s12", 'escape' => false));?>
		<?php echo $this->Html->link("Meu Cadastro", array('admin' => false, 'meu' => true, 'controller' => 'users', 'action' => 'index'), array('class' => "underline s12", 'escape' => false));?>
		<?php echo $this->Html->link("Sair", '/logout', array('class' => "underline s12", 'escape' => false));?>
		<div class="userIMGlarge">
			<?php echo $this->Image->avatar_logged_user('big', array( 'clear' => true,'alt' => '')); ?>
		</div>
<?php
// 		<script>
// 		$.ajax({
// 			url: '<php echo Router::url(array('controller' => 'bids', 'action' => 'balance', 'meu' => true), true); php>',
// 			dataType: 'text',
// 			timeout: 10000,
// 			async: true,
// 			global: false,
// 			success: function(data){
// 		    	var userBalance = data.split('|');
// 		    	$("#nBids").html('<span class="commonBids">'+ userBalance[0] +'</span> LANCES (<span class="expressBids">'+ userBalance[1] +'</span> expirando)');
// 			},
// 			error: function(XMLHttpRequest, textStatus, errorThrown){
// 			}
// 		});
// 		</script>
?>
		<div class="sideBarCat"><?php echo $this->Html->link("Perfil", array('meu' => true, 'controller' => 'users', 'action' => 'edit'), array('class' => "", 'escape' => false)); ?></div>
		<div class="sideBarCat"><?php echo $this->Html->link("Avatar", array('meu' => true, 'controller' => 'users', 'action' => 'avatar'), array('class' => "", 'escape' => false)); ?></div>
		<div class="sideBarCat"><?php echo $this->Html->link("Meus Descontos", array('meu' => true, 'controller' => 'bought_rights', 'action' => 'index'), array('class' => "", 'escape' => false)); ?></div>
		<div class="sideBarCat"><?php echo $this->Html->link("Leilões Arrematados", array('meu' => true, 'controller' => 'auctions', 'action' => 'wons'), array('class' => "", 'escape' => false)); ?></div>
		<div class="sideBarCat"><?php echo $this->Html->link("Leilões não pagos", array('meu' => true, 'controller' => 'auctions', 'action' => 'unpaid'), array('class' => "", 'escape' => false)); ?></div>
		<div class="sideBarCat"><?php echo $this->Html->link("Meus Lances", array('meu' => true, 'controller' => 'bids', 'action' => 'index'), array('class' => "", 'escape' => false)); ?></div>
		<div class="sideBarCat"><?php echo $this->Html->link("Meus Pagamentos", array('meu' => true, 'controller' => 'payments', 'action' => 'index'), array('class' => "", 'escape' => false)); ?></div>
		<?php /*<div class="sideBarCat"><php echo $this->Html->link("Lances Creditados", array('meu' => true, 'controller' => 'bid_credits', 'action' => 'index'), array('class' => "", 'escape' => false)); php></div> */ ?>
		<?php /*<div class="sideBarCat"><php echo $this->Html->link("Parceiros Favoritos", array('meu' => true, 'controller' => 'stores', 'action' => 'index'), array('class' => "", 'escape' => false)); php></div> */ ?>
		<?php /*<div class="sideBarCat"><php echo $this->Html->link("Leilões dos Parceiros", array('meu' => true, 'controller' => 'auctions', 'action' => 'following'), array('class' => "", 'escape' => false)); php></div> */ ?>
		<?php /*<div class="sideBarCat"><php echo $this->Html->link("Produtos Favoritos", array('meu' => true, 'controller' => 'items', 'action' => 'index'), array('class' => "", 'escape' => false)); php></div> */ ?>
		<?php /*<div class="sideBarCat"><php echo $this->Html->link("Leilões Favoritos", array('meu' => true, 'controller' => 'auctions', 'action' => 'favorite'), array('class' => "", 'escape' => false)); php></div> */ ?>
		<?php /*<div class="sideBarCat"><php echo $this->Html->link("Convidar Amigos", "/", array('class' => "", 'escape' => false)); php></div> */ ?>
		<!--/nocache-->
	</span>
</div>
<div id="sideBarBottom"></div>
			</div><!-- sidebar -->
		</div>

		<?php echo $this->element('footer');?>
	</body>
</html>
