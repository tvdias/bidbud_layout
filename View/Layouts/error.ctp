<?php
$isUserLogged = false;
if ($isUserLogged) {
	$user = array(
			'User' => array(
					'username' => "Nome_do_Usuário",
					'adm_access' => false,
					'AdditionalData' => array(
							'balance' => 9999,
							'balance_exp' => 9999,
					)
	));
	$admin = array(
			'User' => array(
					'username' => "Nome_do_Usuário",
					'adm_access' => true,
					'AdditionalData' => array(
							'balance' => 9999,
							'balance_exp' => 9999,
					)
	));
	$authUser = $user;
}
//$sidebarLayout = 'sidebar_auction';
$sidebar_auctions = array();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<?php echo $this->Html->charset(); ?>
		<title>ApetreXo Leilão :: <?php echo empty($title_for_layout) ? "Celular, Smartphone, Iphone, Ipad, Notebook, TV LED e muito mais" : htmlspecialchars($title_for_layout); ?></title>
		<meta name="alexaVerifyID" content="" />
		<meta name="language" content="PT-BR" />
		<meta name="title" content="<?php echo isset($pagemeta['title']) ? htmlspecialchars($pagemeta['title']) : "ApetreXo Leilão :: Celular, Smartphone, Iphone, Ipad, Notebook, TV LED e muito mais"; ?>" />
		<meta name="description" content="<?php echo isset($pagemeta['description']) ? htmlspecialchars($pagemeta['description']) : "O ApetreXo Leilão é o site de leilão de centavos do ApetreXo. Aqui você pode arrematar produtos incríveis por um preço muito baixo e tem a confiança que só o ApetreXo pode dar. Cadastre-se e ganhe 5 lances!"; ?>" />
		<meta NAME="comment" content="<?php echo isset($pagemeta['comment']) ? htmlspecialchars($pagemeta['comment']) : "O ApetreXo Leilão é o site de leilão de centavos do ApetreXo. Aqui você pode arrematar produtos incríveis por um preço muito baixo e tem a confiança que só o ApetreXo pode dar. Cadastre-se e ganhe 5 lances!"; ?>" />
		<meta name="keywords" content="<?php if(isset($pagemeta['keywords'])) echo htmlspecialchars($pagemeta['keywords'])." "; ?>apetrexo site de leilão de centavos site de leilao de centavos site de leilão online site de leiloes site de leilões online leilão virtual 1 centavo leilao virtual 1 centavo lance de 1 centavo leilão 1 centavo leilao 1 centavo leilão de 1 centavo sites de leilão 1 centavo sites de leilao 1 centavo leilão na internet leilão 0.01 leiloes online leilão on line leilao on line leilões on line leiloes on line" />
		<meta name="revisit-after" content="1 day" />
		<meta name="robots" content="index, follow" />
		<meta name="googlebot" content="index, follow" />
	<?php
		echo $this->Html->meta('favicon.ico', 'https://c14985174.ssl.cf2.rackcdn.com/favicon.ico', array('type' => 'icon')); //echo $this->Html->meta('icon');

		echo $this->Html->css('style.css');
		echo $this->Html->script(array('https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js', 'scripts.js', 'ease.min.js', 'plugins.color.min.js'));

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
	</head>
	<body>
		<div id="mainContainer">
			<div id="header">
<div id="categoriasSubMenu"  style="display: none;">
	<div class="btn_sub" id="todasCategorias">
		<div>
			<div class="divisao">
				<h5>
                    <?php echo $this->Html->link("Beleza e Saúde", array("controller" => "auctions", "action" => "category", "beleza-saude", "meu" => false), array('escape' => false)); ?>
				</h5>
                <p></p>
					<h5 class="unico">
                 <?php echo $this->Html->link("Cine & Foto", array("controller" => "auctions", "action" => "category", "cinefoto", "meu" => false), array('escape' => false)); ?>
				</h5>
				<p></p>
				<h5 class="unico">
                 <?php echo $this->Html->link("Eletrônicos", array("controller" => "auctions", "action" => "category", "eletronicos", "meu" => false), array('escape' => false)); ?>
				</h5>
				<p></p>
                <h5 class="unico">
                 <?php echo $this->Html->link("Eletroportáteis", array("controller" => "auctions", "action" => "category", "eletroportateis", "meu" => false), array('escape' => false)); ?>
				</h5>
                <p></p>
                </div>
                <div class="divisao">
				<h5 class="unico">
                 <?php echo $this->Html->link("Esporte e Lazer", array("controller" => "auctions", "action" => "category", "esportelazer", "meu" => false), array('escape' => false)); ?>
				</h5>
                <p></p>
                <h5 class="unico">
                 <?php echo $this->Html->link("Games", array("controller" => "auctions", "action" => "category", "games", "meu" => false), array('escape' => false)); ?>
				</h5>
				<p></p>
                <h5 class="unico">
                 <?php echo $this->Html->link("Informática", array("controller" => "auctions", "action" => "category", "informatica", "meu" => false), array('escape' => false)); ?>
				</h5>
				<p></p><h5 class="unico">
                 <?php echo $this->Html->link("Relógios", array("controller" => "auctions", "action" => "category", "relogios", "meu" => false), array('escape' => false)); ?>
				</h5>
                <p></p>
                </div>
                <div class="divisao">
                <h5 class="unico">
                 <?php echo $this->Html->link("Telefonia", array("controller" => "auctions", "action" => "category", "telefonia", "meu" => false), array('escape' => false)); ?>
				</h5>
				<p></p>
                <h5 class="unico">
                 <?php echo $this->Html->link("Utilidades Domésticas", array("controller" => "auctions", "action" => "category", "utilidadesdomesticas", "meu" => false), array('escape' => false)); ?>
				</h5>
				<p></p>
                <h5 class="unico">
                 <?php echo $this->Html->link("Bebês e Gestantes", array("controller" => "auctions", "action" => "category", "bebegestante", "meu" => false), array('escape' => false)); ?>
				</h5>
				<p></p>
                <h5 class="unico">
                 <?php echo $this->Html->link("Diversão", array("controller" => "auctions", "action" => "category", "diversao", "meu" => false), array('escape' => false)); ?>
				</h5>
				<p></p>
			</div>
		</div>
	</div>
</div>

<div id="menu">
<?php
	echo $this->Html->link($this->Html->image("https://c14985174.ssl.cf2.rackcdn.com/img/menu/inicio.png", array("id" => "inicio", 'class' => "menuitem", 'alt' => 'Início')), "/", array('escape' => false));
	echo $this->Html->link($this->Html->image("https://c14985174.ssl.cf2.rackcdn.com/img/menu/categorias.png", array("id" => "categorias", 'class' => "menuitem", 'alt' => 'Categorias')), '#', array('escape' => false));
	echo $this->Html->link($this->Html->image("https://c14985174.ssl.cf2.rackcdn.com/img/menu/depoimentos.png", array("id" => "depoimentos", 'class' => "menuitem", 'alt' => 'Depoimentos')), array('controller' => 'feedbacks', 'action' => "index", 'meu' => false), array('escape' => false));
	echo $this->Html->link($this->Html->image("https://c14985174.ssl.cf2.rackcdn.com/img/menu/comofunciona.png", array("id" => "comofunciona", 'class' => "menuitem", 'alt' => 'Como Funciona')), array('controller' => 'pages', 'action' => 'display', 'how_it_works', 'meu' => false), array('escape' => false));
	echo $this->Html->link($this->Html->image("https://c14985174.ssl.cf2.rackcdn.com/img/menu/arrematados.png", array("id" => "arrematados", 'class' => "menuitem", 'alt' => 'Arrematados')), array('controller' => 'auctions', 'action' => 'endeds', 'meu' => false), array('escape' => false));
?>
</div>
<?php echo $this->Html->link("<div id=\"logo\">&nbsp;</div>", "/", array('escape' => false, "style" => "width: 350px; height: 95px")); ?>

<?php echo $this->Form->create(false, array('url' => array('controller' => 'auctions', 'action' => 'search', 'meu' => false), 'id' => 'mainSearch', 'type' => 'get'));?>
    <input type="text" id="AuctionTitle" name="text" value="<?php echo isset($auctionsSearchBox) ? $auctionsSearchBox : "Procurar um Leilão"; ?>" maxlength="255" />
	<input class="enviar" value="" type="submit" style="display:none" />
<?php echo $this->Form->end(); ?>

<!--nocache--><?php
if($isUserLogged):
echo $this->Html->scriptStart(array('inline' => true));
echo "var user_logged_in = true;";
echo $this->Html->scriptEnd();
?>
	<div id="comprarBids">
		<?php echo $this->Html->link($this->Html->image("https://c14985174.ssl.cf2.rackcdn.com/img/btn_comprar_lances.png", array('alt' => 'Comprar Lances')), array('controller' => 'packages', 'action' => 'index', 'meu' => false), array('escape' => false)); ?>
	</div>
	<?php if($this->layout != "meucadastro"): ?>
	<div id="userlogged">
		<div class="userIMG">
			<?php //echo $this->Image->avatar($loggedUser['User']['id'], 'small', array( 'clear' => true, 'alt' => '')); ?>
			<?php //echo $this->Image->avatar_logged_user('small'); ?>
		</div>
		<div class="userLogin"><?php echo strtoupper($authUser['User']['username']); ?></div>
		<div id="nBids" class="nBids"><span class="commonBids"><?php echo $authUser['User']['AdditionalData']['balance']; ?></span> LANCES<?php if($authUser['User']['AdditionalData']['balance_exp']) echo " (<span class=\"expressBids\">". $authUser['User']['AdditionalData']['balance_exp'] ."</span> expirando)"; ?></div>
		<?php if($authUser['User']['adm_access']) echo $this->Html->link("Adm", array('admin' => true, 'controller' => 'users', 'action' => 'home'), array('class' => "meuBidBud", 'escape' => false));?>
		<?php echo $this->Html->link("Meu Cadastro", array('meu' => true, 'controller' => 'users', 'action' => 'index'), array('class' => "meuBidBud", 'escape' => false));?>
		<?php echo $this->Html->link("Sair", '/logout', array('class' => "meuBidBud", 'escape' => false));?>
	</div>
	<?php endif; ?>
	<div id="fl-bids" style="display: none;">
		<?php if($authUser['User']['adm_access']):
			echo $this->Html->link("Administração", array('admin' => true, 'controller' => 'users', 'action' => 'home'), array('class' => "fl-bids-meuBidBud", 'escape' => false));
			echo "<br />";
		else:
		?>
		<div id="fl-nBids" class="fl-bids-nBids">
			<span class="commonBids"><?php echo $authUser['User']['AdditionalData']['balance']; ?></span> LANCES<?php if($authUser['User']['AdditionalData']['balance_exp']) echo " (<span class=\"expressBids\">". $authUser['User']['AdditionalData']['balance_exp'] ."</span> expirando)"; ?>
		</div>
		<?php endif;
			echo $this->Html->link("Meu Cadastro", array('meu' => true, 'controller' => 'users', 'action' => 'index'), array('class' => "fl-bids-meuBidBud", 'escape' => false));
			echo " ";
			echo $this->Html->link("Topo", '#', array('class' => "fl-bids-meuBidBud topo", 'escape' => false));
		?>
	</div>
<?php
// echo $this->Html->scriptStart(array('inline' => true));
// $.ajax({
// 	url: '<php echo Router::url(array('controller' => 'bids', 'action' => 'balance', 'meu' => true), true); php>',
// 	dataType: 'text',
// 	timeout: 1000,
// 	async: true,
// 	global: false,
// 	cache: false,
// 	success: function(data){
//     	var userBalance = data.split('|');
//     	$(".commonBids").html(userBalance[0]);
//     	<php if($expBids) echo "$(\".expressBids\").html(userBalance[1]);"; php>
// 	},
// 	error: function(XMLHttpRequest, textStatus, errorThrown){
// 	}
// });
// echo $this->Html->scriptEnd();

else:
echo $this->Html->scriptStart(array('inline' => true));
echo "var user_logged_in = false;";
echo $this->Html->scriptEnd();
?>
    <div id="cadastro">
    	<?php echo $this->Html->link($this->Html->image("https://c14985174.ssl.cf2.rackcdn.com/img/cadastro.png", array('alt' => 'Cadastro')), "/cadastro", array('escape' => false));?>
    </div>
    <div id="login"></div>
    <div id="loginOver">
		<?php echo $this->Form->create('User', array('id' => 'Login', 'name' => 'Login', 'url' => '/login'));?>
            <?php echo $this->Form->input('User.username',array('label'=> false, 'div' => false, 'id'=> 'userLogin', 'tabindex' => '1', 'value' => "Usuário", 'style' => "position: absolute; top: 68px; left: 40px; width: 180px;")); ?>
            <?php echo $this->Form->input('User.password',array('type'=>'text', 'label'=> false, 'div' => false, 'id' => 'headeruserpassword', 'tabindex' => '2', 'value' => "Senha", 'style' => "position: absolute; top: 115px; left: 40px; width: 180px;")); ?>
			<?php echo $this->Html->link("Esqueceu sua senha?", array('controller' => 'users', 'action' => "forgot_password"), array('escape' => false, 'style' => 'text-decoration:underline; color: #41b1e3; font-size:12px; position: absolute; top: 170px; left: 105px;'));?>
            <?php echo $this->Form->submit('https://c14985174.ssl.cf2.rackcdn.com/img/btn_ok.png', array('alt' => "Entrar", 'style' => "position: absolute; top: 160px; left: 30px; cursor: pointer; width: 55px; height: 32px")); ?>
        </form>
    </div>
<?php endif;?>
<!--/nocache-->
			</div><!-- header -->

			<div id="sidebar">
			<?php
				if(isset($sidebarLayout))
					if ($sidebarLayout == 'sidebar_auction' && isset($sidebar_auctions))
						echo $this->element('sidebar_auction', array('auctions' => $sidebar_auctions));
					else
						echo $this->element($sidebarLayout);
				else
					echo $this->element('sidebar');
			?>
			</div><!-- sidebar -->

			<div id="conteudoPrincipalFull">
			<?php
				echo $this->Session->flash();
				//echo $this->Session->flash('auth');

				echo $content_for_layout;
			?>
			</div><!-- conteudoPrincipalFull -->
		</div>

		<?php echo $this->element('footer');?>
	</body>
</html>