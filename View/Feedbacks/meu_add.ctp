<div class="feedbacks form">
<?php echo $this->Form->create('Feedback', array('url' => array('controller' => 'feedbacks', 'action' => 'add', $auction['Auction']['id'], 'meu' => true)));?>
	<fieldset>
		<legend>Enviar Depoimento para leilão <i><?php echo $auction['Auction']['title']; ?></i></legend>
		<?php
		echo $this->Form->input('text', array('label' => false));
		?>
	</fieldset>
<?php echo $this->Form->end("Enviar");?>
</div>
