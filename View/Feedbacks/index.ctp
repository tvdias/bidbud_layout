<?php
	$this->Html->addCrumb("Depoimentos", array('controller' => 'feedbacks', 'action' => 'index'));
?>
<div id="caminho">
	<p>
		<?php echo $this->element('crumb'); ?>
	</p>
	<div style="clear: both;"></div>
</div><!-- caminho -->

<div id="conteudoTitulo">
	<h2>Depoimentos</h2>
    <div style="clear: both;"></div>
</div><!-- conteudoTitulo -->

<?php if (!empty($feedbacks)): ?>
<div class="roundedBorder azulBg" style="height: 30px;padding-top: 10px;">
	<span class="s12 branco" style="margin-left: 20px;">USUÁRIO</span>
	<span class="s12 branco" style="margin-left: 50px;">DEPOIMENTO</span>
	<span class="s12 branco" style="margin-left: 225px;">PREÇO/DESCONTO</span>
	<span class="s12 branco" style="margin-left: 130px;">PRODUTO</span>
</div>
<br />
<?php //echo pr($feedbacks); exit();
	foreach($feedbacks as $feedback):
		$auction_title = $this->Text->truncate($feedback['Auction']['title'], 60, array('ending' => "...", 'exact' => true, 'html' => false));
		$feedback_image = $this->Image->image_out($feedback['Image'], null, 'small', array('class' => 'left', 'style' => 'width: 90px; height: 90px'));
		$prod_image = $this->Image->image_out($feedback['Auction']['Item']['Image'], null, 'medium', array('style' => 'width: 100px; height: 75px', 'alt' => $feedback['Auction']['title']));
		
		$site = isset($feedback['Auction']['slug']) ? array('controller' => "auctions", 'action' => "view", $feedback['Auction']['slug']) : NULL;
		$prod_site = isset($feedback['Auction']['AuctionDetail']['site']) ? $feedback['Auction']['AuctionDetail']['site'] : NULL;
		
		$price = 0;
		if($feedback['Auction']['is_price_limited']) {
			$price = min($feedback['Auction']['price'], $feedback['Auction']['price_max']);
		} else {
			$price = $feedback['Auction']['price'];
		}
		$price = $price / 100;

		$discount = ($feedback['Auction']['price_store'] > 0) ? number_format((1 - $price / $feedback['Auction']['price_store'])*100, 2, ',', '') : 100;
		//$saving = ($auction['Auction']['price_store'] > 0) ? number_format($auction['Auction']['price_store'] - $price, 2, ',', '') : 0;
		
		$price = number_format($price, 2, ',', '');
		$price_store = number_format( ($feedback['Auction']['price_store'] > 0) ? $feedback['Auction']['price_store'] : 0, 2, ',', '');
?>
<div style="padding: 10px;" class="roundedBorder brancoBg simpleClearFix" style="padding: 20px;">
	<div style="margin-top:6px; margin-left:6px; margin-bottom:6px;" class="userIMGdepoimento floatLeft">
		<?php echo $feedback_image; ?>
    </div>
	<div class="w375 floatLeft" style="margin-left: 15px; margin-top:6px; width:300px;">
		<span class="azul bold"><?php echo $feedback['User']['username']; ?></span>
		<p class="s12 cinza"><?php echo $feedback['Feedback']['text']; ?></p>
	</div>
	<center style="margin-left: 15px; margin-top:6px; border-right: 1px #ccc dotted; border-left: 1px #CCC dotted; padding-right: 15px; padding-left: 15px; margin-right: 10px;" class="floatLeft">
		<div class="s24 azul bold">R$ <?php echo $price; ?></div>
		<div class="s12 cinza">ao invés de R$ <?php echo $price_store; ?></div>
		<div class="s24 vermelho bold"><?php echo $discount; ?>%</div>
		<div class="s12 cinza">de desconto</div>
	</center>
	<div style="margin-top:14px; margin-right:10px;" class="floatLeft produto100x75">
		<?php //echo $image->image_out($feedback['Image']['dir'], $feedback['Image']['filename'], 'small', array('class' => 'left', 'style' => "width: 100px; height: 75px; display: inline; opacity: 1;")); ?>
		<?php echo $prod_image; ?>
    </div>
	<div style="margin-top:13px; height: 40px;" class="s12 azul bold"><?php echo $this->Html->link($auction_title, $site, array('class' => "s12 azul bold", 'escape' => false)); ?></div>
	<?php if($prod_site) { echo $this->Html->link($this->Html->image("https://c14985174.ssl.cf2.rackcdn.com/img/btn_ver_no_site.png", array('style' => "width: 122px; height: 31px;")), $prod_site, array('target' => "_blank", 'style' => "width: 122px; height: 31px;", 'escape' => false)); } ?>
</div>
<br />
<?php
	endforeach;
	echo $this->element('search_pagination');
else:
?>
<div class="topBox" style="width: 100%;">
	<div class="topBoxMSG" style="left:30px">Desculpe, não há depoimentos no momento</div>
	<p class="s16 cinza" style="position: absolute; left: 30px; top: 80px;">
		Já arrematou e recebeu um produto do Apetrexo Leilão?<br />
		Envie-nos o seu depoimento e ganhe lances para arrematar mais produtos!
	</p>
	<div id="topBoxIMG"></div>
</div>
<?php endif; ?>