<div id="caminho">
  <?php $this->Html->addCrumb('Meu BidBud', array('controller' => 'users', 'action' => 'index', $this->Session->read('Auth.User.id'))); ?>
  <p><?php echo $this->element('crumb'); ?></p>
  <div style="clear: both;"></div>
</div><!-- caminho -->

<div id="conteudoTitulo">
	<h2>BIDs Creditados</h2>
	<div style="clear: both;"></div>
</div><!-- conteudoTitulo -->

<div class="box" style="margin-bottom: 20px; padding-bottom: 10px;">
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('created', 'Data');?></th>
			<th><?php echo $this->Paginator->sort('credit', 'Crédito');?></th>
			<th><?php echo $this->Paginator->sort('description', 'Descrição');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($bidCredits as $bidCredit): ?>
	<tr>
		<td><?php echo h($bidCredit['BidCredit']['created']); ?>&nbsp;</td>
		<td><?php echo h($bidCredit['BidCredit']['credit']); ?>&nbsp;</td>
		<td><?php echo h($bidCredit['BidCredit']['description']); ?>&nbsp;</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< Anterior', array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next('Próxima >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>