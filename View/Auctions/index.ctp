<?php
// //$javascript->codeBlock(null, array('inline' => false));
// echo $this->Html->scriptStart(array('inline' => false));
// //echo 'session="'.session_id() .'"';
// //echo "current_sid = '". session_id() ."'";
// echo "user_logged_in = ". ((!isset($loggedUser) || $loggedUser === false) ? "false" : "true") .";";

// //$javascript->blockEnd();
// echo $this->Html->scriptEnd();

$this->Html->script('https://c14985174.ssl.cf2.rackcdn.com/js/jquery.nivo.slider.pack.js', array('inline' => false));
?>

<div id="destaques" style="margin-bottom: 30px;">
	<div class="slider-wrapper theme-default">
		<div id="slider" class="nivoSlider">
<?php
	echo $this->Html->image("https://c14985174.ssl.cf2.rackcdn.com/img/banner/promonamorados.jpg", array(
		'alt' => "Melhores preços!",
		'url' => array('controller' => 'promo', 'action' => 'display', 'namorados')
	));
	echo $this->Html->image("https://c14985174.ssl.cf2.rackcdn.com/img/banner/destaque1.png", array(
		'alt' => "Bem-vindo",
		//'url' => array('controller' => 'users', 'action' => 'registration')
	));
	echo $this->Html->link(
			$this->Html->image("https://c14985174.ssl.cf2.rackcdn.com/img/banner/fb.jpg", array('alt' => "Siga o ApetreXo Leilão face!")), 
			"http://www.facebook.com/ApetreXoLeilao",
			array('target' => "_blank", "escape" => false)
	);
	//echo $this->Html->image("https://c14985174.ssl.cf2.rackcdn.com/img/banner/destaque2.png", array(
	//	'alt' => "Melhores preços!",
	//	//'url' => array('controller' => 'packages', 'action' => 'index')
	//));
?>
		</div>
	</div>
</div>
<script type="text/javascript">
$('head').append('<?php echo $this->Html->css('https://c14985174.ssl.cf2.rackcdn.com/css/nivo/apetrexo/default.min.css'); ?>').append('<?php echo $this->Html->css('https://c14985174.ssl.cf2.rackcdn.com/css/nivo/nivo-slider.css'); ?>');
$(window).load(function() {
	$('#slider').nivoSlider({
		effect: 'random', // Specify sets like: 'fold,fade,sliceDown'
		pauseTime: 6000, // How long each slide will show
		randomStart: false, // Start on a random slide
		startSlide: 0, // Set starting Slide (0 index)
		directionNavHide: true, // Only show on hover
		controlNav: true, // 1,2,3... navigation
		pauseOnHover: true, // Stop animation while hovering
		prevText: 'Próx', // Prev directionNav text
		nextText: 'Ant', // Next directionNav text
	});
});
</script>

<!--nocache-->
<?php if (empty($auctions) && empty($ended_auctions)): ?>
<div class="topBox">
	<div class="topBoxMSG">Desculpe</div>
	<p class="s16 cinza" style="position: absolute; left: 50px; top: 80px;">Infelizmente não há leilões neste momento.</p>
	<div id="topBoxIMG"></div>
</div>
<?php
else:
	if (!empty($auctions)):
?>
<div id="leiloesaovivo">
	<p style="float:left;font-weight: bold; font-size: 18px;color: black;">Leilões ao vivo</p>
	<p style="float:right;font-weight: bold; font-size: 14px;color: black;"><?php echo $this->Html->link("ver mais", array('controller' => 'auctions', 'action' => 'index2'), array(), false, false); ?> »</p>
	<div style="clear: both;"></div>
	<div id="produtosHolder">
<?php
//echo $this->element('auction_items', array('auctions' => $auctions, 'cache' => "+10 minutes"));
	$auction_ids = array();

	foreach($auctions as $auction):
		$auction_ids[] = $auction['Auction']['id'];

		$auction_title = $this->Text->truncate($auction['Auction']['title'], 46, array('ending' => '...', 'exact' => true, 'html' => false));

		$image_tag = $this->Image->image_out($auction['Item']['Image'], null, "medium", array('style' => 'width: 128px; height: 96px', 'alt' => $auction['Auction']['title'], 'title' => $auction['Auction']['title'], 'clear' => true, 'empty_image' => 'photo1.jpg' ));
		$auction_link_array = array('controller' => 'auctions', 'action' => 'view', $auction['Auction']['slug']);

		//$isClosed = !(!isset($auction['Auction']['closed']) || $auction['Auction']['closed'] == 0 );
		$isClosed = isset($auction['Auction']['closed']) && $auction['Auction']['closed'] == 1;
		// TODO : get status_id from cache/config/DB
		$isSuspended = ($auction['Auction']['status_id'] == 19);

		$uprice = 0;
		if($auction['Auction']['is_price_limited']) {
			$uprice = min($auction['Auction']['price'], $auction['Auction']['price_max']);
		} else {
			$uprice = $auction['Auction']['price'];
		}
		$uprice = $uprice/100;
		$price = number_format($uprice, 2, ',', '');
		$discount = ($auction['Auction']['price_store'] > 0) ? number_format((1 - ($uprice / $auction['Auction']['price_store']))*100, 2, ',', '') : 0;
?>
<div class="produto" name="auction_element" id="auction_<?php echo $auction['Auction']['id'];?>">
	<div class="produtoBoxTop">
		<div class="icons" style="width: 10px; float: left;"></div>

		<?php if($auction['Auction']['is_price_limited'] == 1) { echo "<img class=\"icons\" src=\"https://c14985174.ssl.cf2.rackcdn.com/img/ico_cifra_white.png\">"; } ?>
		<?php if($auction['Auction']['has_discounts'] == 1) { echo "<img class=\"icons\" src=\"https://c14985174.ssl.cf2.rackcdn.com/img/ico_porcento_white.png\">"; } ?>
		<?php if($auction['Auction']['is_free_ship'] == 1) { echo "<img class=\"icons\" src=\"https://c14985174.ssl.cf2.rackcdn.com/img/ico_frete_white.png\">"; } ?>
		<?php if($auction['Auction']['is_charity'] == 1) { echo "<img class=\"icons\" src=\"https://c14985174.ssl.cf2.rackcdn.com/img/ico_beneficente_white.png\">"; } ?>

		<img class="icons closeBox" style="float: right;" src="https://c14985174.ssl.cf2.rackcdn.com/img/ico_close_white.png">
	</div>
	<div class="produtoBoxBody">
		<h3><?php echo $this->Html->link($auction_title, $auction_link_array); ?></h3>
		<div class="fotoProd">
			<div style="width: 190px; height: 100px; margin-left: 15px;">
				<?php echo $this->Html->link($image_tag, $auction_link_array, array('escape' => false)); ?>
			</div>
		</div>
		<?php if (!$isClosed && !$isSuspended): ?>
		<h4><span id="timer_<?php echo $auction['Auction']['id'];?>" class="countdown">--:--:--</span></h4>
		<?php elseif ($isSuspended): ?>
		<h4><span id="timer_<?php echo $auction['Auction']['id'];?>" class="countdown suspended arrematado">Suspenso!</span></h4>
		<?php else: ?>
		<h4><span id="timer_<?php echo $auction['Auction']['id'];?>" class="arrematado">Arrematado!</span></h4>
		<?php endif; ?>
		<div class="valor">R$ <span id="price_<?php echo $auction['Auction']['id'];?>" class="price"><?php echo $price; ?></span></div>
		<div class="prodUser" style="width: 100%; text-align: center;"><?php if($discount > 0): ?><span id="discount_<?php echo $auction['Auction']['id'];?>"><?php echo $discount; ?></span>% de desconto!<?php else: echo "&nbsp"; endif; ?></div>
		<div class="bidmainbtn" id="bidmainbtn_<?php echo $auction['Auction']['id'];?>" style="height:35px; margin: 5px auto;"><a id="bidlnk_<?php echo $auction['Auction']['id'];?>" class="bidlnk" href="#">&nbsp;</a></div>
		<div class="prodUser" id="leader_<?php echo $auction['Auction']['id'];?>" style="width: 100%; text-align: center;"><?php echo $auction['Winner']['username'];?>&nbsp;</div>
	</div>
	<div id="cl_<?php echo $auction['Auction']['id'];?>" style="display:none;"><?php echo $isClosed || $isSuspended;?></div>
	<div id="et_<?php echo $auction['Auction']['id'];?>" style="display:none;"><?php echo $isSuspended ? 999999999999 : $auction['Auction']['end_timestamp'];?></div>
	<div id="tl_<?php echo $auction['Auction']['id'];?>" style="display:none;"><?php echo $auction['Auction']['timestamp_ms'];?></div>
    <div id="ti_<?php echo $auction['Auction']['id'];?>" style="display:none;"><?php echo $auction['Auction']['time_increase'];?></div>
	<div id="pr_<?php echo $auction['Auction']['id'];?>" style="display:none;"><?php echo $uprice; ?></div>
</div>
<?php
endforeach;

//$this->Html->script(array('https://c14985174.ssl.cf2.rackcdn.com/js/gettime.min.js', 'https://c14985174.ssl.cf2.rackcdn.com/js/plugins.color.min.js'), array('inline' => false));
$this->Html->script('https://c14985174.ssl.cf2.rackcdn.com/js/plugins.color.min.js', array('inline' => false));
echo $this->Html->scriptStart(array('inline' => true));
// $(document).ready(function(){
// 	//setInterval('clkCycle()',clkCycleTime);
// 	clkCycle();
// 	gettime();
// });
echo "gettime();";
echo $this->Html->scriptEnd();
?>
		<div id="timestamp" style="display: none">0</div>
	</div>
	<div style="clear: both;"></div>
	<p style="float:right;font-weight: bold; font-size: 14px;color: black;"><?php echo $this->Html->link("ver mais", array('controller' => 'auctions', 'action' => 'index2'), array(), false, false); ?> »</p>
</div>
<?php
	endif;
	if (!empty($ended_auctions)):
?>
<div id="leiloesareematados">
	<p style="float:left;font-weight: bold; font-size: 18px;color: black;">Leilões Arrematados</p>
	<p style="float:right;font-weight: bold; font-size: 14px;color: black; display: none;"><?php echo $this->Html->link("ver mais", array('controller' => 'auctions', 'action' => 'endeds')); ?> »</p>
	<div style="clear: both;"></div>
	<div id="arrematadosHolder">
<?php
		//echo $this->element('auction_ended_items', array('auctions' => $ended_auctions, 'cache' => "+10 minutes"));
//$number_format = array('places' => 2, 'before' => 'R$', 'escape' => false, 'decimals' => ',', 'thousands' => '.');
	foreach($ended_auctions as $auction):
		$auction_title = $this->Text->truncate($auction['Auction']['title'], 22, array('ending' => "...", 'exact' => true, 'html' => false));
		$image_tag = $this->Image->image_out($auction['Item']['Image'], null, "medium", array('alt' => $auction['Auction']['title'], 'title' => $auction['Auction']['title'], 'clear' => true, 'empty_image' => 'photo1.jpg' ));
		$auction_link_array = array('controller' => 'auctions', 'action' => 'view', $auction['Auction']['slug']);
//		$price = 0;
//		if($auction['Auction']['is_price_limited']) {
//			$price = min($auction['Auction']['price'], $auction['Auction']['price_max']);
//		} else {
//			$price = $auction['Auction']['price'];
//		}
//		
//		$saving = number_format($auction['Auction']['price_store'] - $price, 2, ',', '');
//		$discount = number_format((1 - $price / $auction['Auction']['price_store'])*100, 2, ',', '');
//		$price = number_format($price/100, 2, ',', '');


		$price = 0;
		if($auction['Auction']['is_price_limited']) {
			$price = min($auction['Auction']['price'], $auction['Auction']['price_max']);
		} else {
			$price = $auction['Auction']['price'];
		}
		$price = $price/100;

		//$discount = ($auction['Auction']['price_store'] > 0) ? number_format((1 - $price / $auction['Auction']['price_store'])*100, 2, ',', '') : 100;
		//$saving = ($auction['Auction']['price_store'] > 0) ? number_format($auction['Auction']['price_store'] - $price, 2, ',', '') : 0;
		$saving_percent = ($auction['Auction']['price_store'] > 0) ? number_format((1 - $price / $auction['Auction']['price_store'])*100, 2, ',', '') : 0;
			
		$price = number_format($price, 2, ',', '');
		//$price_store = ($auction['Auction']['price_store'] > 0) ? number_format($auction['Auction']['price_store'], 2, ',', '') : 0;
?>
<div class="produtoArrematado">
    <div class="fotArrematado">
    	<?php echo $this->Html->link($image_tag, $auction_link_array, array('escape' => false)); ?>
    </div>
    <div class="arremateTitulo"><?php echo $this->Html->link($auction_title, $auction_link_array, array('title' => $auction['Auction']['title'], 'escape' => false)); ?></div>
    <div class="arremateValor">R$<?php echo $price; ?> <?php if($saving_percent > 0) echo"<span class=\"arremateDesconto\">". $saving_percent ."% de desconto</span>"; ?></div>
</div>
<?php
	endforeach;
?>
	</div>
	<div style="clear: both;"></div>
	<p style="float:right;font-weight: bold; font-size: 14px;color: black;"><?php echo $this->Html->link("ver mais", array('controller' => 'auctions', 'action' => 'endeds')); ?> »</p>
</div>
<?php
	endif;
endif;
?>
<!--/nocache-->