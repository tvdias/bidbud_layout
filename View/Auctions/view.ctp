<style>
.sel {
	border-color: #069;
}
</style>
<?php
//echo pr($auction); exit();

$isClosed = isset($auction['Auction']['closed']) && $auction['Auction']['closed'] == 1;
// TODO : get status_id from cache/config/DB
$isSuspended = ($auction['Auction']['status_id'] == 19);

echo $this->Html->scriptStart(array('inline' => false));
echo "images = new Array();\n";
echo "imageObj = new Image();\n";
foreach($thumbs as $index => $item_image) {
	echo "imageObj.src=\"".$item_image["image"]."\";\nimages.push(imageObj.src);\n";
}
echo "item_to_like = ". $auction["Item"]["id"] .";\n";
echo "current_auction_id = ". $auction['Auction']['id'] .";\n";
?>
$(document).ready(function() {
	$("#item_thumbs a").click(function (){
		image_index = $(this).attr("id").split("_")[1];
		$("#item_thumbs a").parent().removeClass('sel');
        $(this).parent().addClass('sel');
        $("#item_image").attr("src", images[image_index])
        return false;
    });

});
<?php
// 	$("#follow_button").click(function() {
//         $.post("<php echo Router::url( array("controller" => "followers", "action" => "addFollowerAjax") ); php>",
// 				{user_id:"<?php echo $auction["Item"]['user_id']; php>"},
// 				function (data) {
// 					if(data == 1) {
// 						//incrementCounter($("#followers_counter"));
// 						//$("#seguirParceiro").html("<a class=\"seguindo\" id=\"unfollow_button\" href=\"/\">Deixar de seguir este parceiro</a>");
// 				    	$("#follow_button").hide();
// 				    	$("#unfollow_button").show();
// 					}
// 				}
// 		);
// 		return false;
// 	});
	
// 	$("#unfollow_button").click(function() {
//         $.post("<php echo Router::url( array("controller" => "followers", "action" => "removeFollowerAjax") ); php>",
// 			{user_id:"<php echo $auction["Item"]['user_id']; php>", reverse:true},
// 			function (data) {
// 				if(data == 1) {
// 					//incrementCounter($("#followers_counter"));
// 					//$("#seguirParceiro").html("<a class=\"seguindo\" id=\"unfollow_button\" href=\"/\">Deixar de seguir este parceiro</a>");
// 			    	$("#unfollow_button").hide();
// 			    	$("#follow_button").show();
// 				}
// 			}
// 		);
// 		return false;
// 	});

echo $this->Html->scriptEnd();

$auction_title = $this->Text->truncate(strip_tags($auction['Auction']['title']), 60, array('ending' => "...", 'exact' => false, 'html' => false));
$auction_tagline = $this->Text->truncate(strip_tags($auction['Item']['tagline']), 95, array('ending' => "...", 'exact' => false, 'html' => false));
//$image_seller = $this->Image->store($auction["Store"]['id'], 'medium', array('style' => 'width: 170px; height: 60px', 'alt' => $auction['Store']['name'], 'clear' => true, 'empty_image' => 'photo1.jpg' ));

$uprice = 0;
if($auction['Auction']['is_price_limited']) {
	$uprice = min($auction['Auction']['price'], $auction['Auction']['price_max']) / 100;
	$uprice_max = $auction['Auction']['price_max'] / 100;
} else {
	$uprice_max = $uprice = $auction['Auction']['price'] / 100;
}
$saving = ($auction['Auction']['price_store'] > 0) ? number_format($auction['Auction']['price_store'] - $uprice, 2, ',', '') : 0;
$saving_percent = ($auction['Auction']['price_store'] > 0) ? number_format((1 - ($uprice / $auction['Auction']['price_store']))*100, 2, ',', '') : 0;

$price = number_format($uprice, 2, ',', '');
$price_store = ($auction['Auction']['price_store'] > 0) ? number_format($auction['Auction']['price_store'], 2, ',', '') : 0;
$delivery_cost = number_format( ((isset($auction['AuctionDetail']['delivery_cost']) && $auction['AuctionDetail']['delivery_cost'] > 0) ? $auction['AuctionDetail']['delivery_cost'] : "0"), 2, ',', '' );

switch ($auction['Auction']['time_increase']) {
	case 15:
		$clock_img = "relogio_15.png";
		break;
	case 20:
		$clock_img = "relogio_20.png";
		break;
	case 30:
	default:
		$clock_img = "relogio_30.png";
		break;
}
?>
<!--nocache-->
<?php
echo $this->Html->scriptStart(array('inline' => true));
if ($this->Session->check('Auth.User.id')) {
//	echo "user_logged_in = true;";
	App::import('Model', 'Bid');
	$bidModel = new Bid();
	$userBidsOnAuction = $bidModel->userBidsOnAuction($this->Session->read('Auth.User.id'), $auction['Auction']['id']);
	echo "user_bids_on_auction = ". $userBidsOnAuction .";";
} else {
//	echo "user_logged_in = false;";
	echo "user_bids_on_auction = 0;";
}
echo $this->Html->scriptEnd();
?>
<!--/nocache-->
<div id="caminho">
	<?php $this->Html->addCrumb( $auction['Item']['Category']['name'], "/" . $auction['Item']['Category']['slug']); ?>
	<p><?php echo $this->element('crumb'); ?></p>

	<div class="blackIcons" style="position: absolute; top: 0px; left: 600px; width: 100px; text-align:right;">
		<?php if($auction['Auction']['is_price_limited'] == 1) { echo "<a href=\"#\"><img src=\"https://c14985174.ssl.cf2.rackcdn.com/img/ico_cifra_black.png\"></a>"; } ?>
		<?php if($auction['Auction']['has_discounts'] == 1) { echo "<a href=\"#\"><img src=\"https://c14985174.ssl.cf2.rackcdn.com/img/ico_porcento_black.png\"></a>"; } ?>
		<?php if($auction['Auction']['is_free_ship'] == 1) { echo "<a href=\"#\"><img src=\"https://c14985174.ssl.cf2.rackcdn.com/img/ico_frete_black.png\"></a>"; } ?>
		<?php if($auction['Auction']['is_charity'] == 1) { echo "<a href=\"#\"><img src=\"https://c14985174.ssl.cf2.rackcdn.com/img/ico_beneficente_black.png\"></a>"; } ?>
	</div>

	<div style="clear: both;"></div>
</div><!-- caminho -->


<div id="conteudoTitulo">
	<h2 style="float:left;font-weight: bold; font-size: 20px;color: black;  margin-bottom: 0px; margin-left: 0px; margin-top: 0px; width: 100%;">
		<span style="float: left;"><?php echo $auction_title;?></span><a href="#" style="display:none"><img src="https://c14985174.ssl.cf2.rackcdn.com/img/ico_estrela.png" style="float: right;"></a>
	</h2>
	<div style="clear: both;"></div>
	<p style="color: black; font: 12px Arial; width: 700px;"><?php echo $auction_tagline;?>&nbsp;<a href="#anchor_item_description"><b class="azul">Descrição completa</b>&nbsp;<span class="downArrow" style="font-size: 17px; position: relative; display: inline-block; top: 6px;"></span></a></p>
</div><!-- conteudoTitulo -->

<div class="leilaoProdutoHolder" name="auction_element" id="auction_<?php echo $auction['Auction']['id'];?>">
    <div class="leilaoProduto">
        <div class="produtoLeilaoIMG">
            <?php echo $this->Image->image_out($auction["Item"]["Image"], null, "big", array('id' => 'item_image', 'alt' => $auction['Auction']['title'], 'title' => $auction['Auction']['title'], 'clear' => true, 'empty_image' => 'photo1.jpg' )); ?>
        </div>
        <div class="leilaoThumbs" id="item_thumbs">
        <?php
			foreach($thumbs as $index => $item_image) {
				$class = "produtoLeilaoThumb";
				if ($index == 0) $class .= " sel";
				echo '<div class="'. $class .'"><a href="#" id="image_'. $index .'"><img alt="" src="'. $item_image["thumb"] .'" /></a></div>';
			}
			while ($index++ < 2) {
				echo '<div class="produtoLeilaoThumb"><img alt="" src="https://c14985174.ssl.cf2.rackcdn.com/img/bg_foto.jpg" style="width: 75px; height: 60px;" /></div>';
			}
		?>
		</div>
<?php
// 		<div id="seguirParceiro">
// if(isset($loggedUserData['User']['id'])) {
// 	if($isFollowing) {
// 		echo "<a class=\"seguir\" id=\"follow_button\" href=\"/\" style=\"display: none\">Seguir este parceiro</a>";
// 		echo "<a class=\"seguindo\" id=\"unfollow_button\" href=\"/\">Deixar de seguir este parceiro</a>";
// 	} else {
// 		echo "<a class=\"seguir\" id=\"follow_button\" href=\"/\">Seguir este parceiro</a>";
// 		echo "<a class=\"seguindo\" id=\"unfollow_button\" href=\"/\" style=\"display: none\">Deixar de seguir este parceiro</a>";
// 	}
// }
// 		</div>
// 		<div class="seguir" style="width: 100%; text-align: center; display:none"><span id="followers_counter">0<php echo $seller_profile['additional_data']['numberOfFollowers']; php></span> úsuários já seguem.</div>
?>
	</div><!-- leilaoProduto -->

	<div id="leilaoAtualiza">
	<div class="leilaoInfo">
		<div class="leilaoInfoRight">
            <p class="boldBlack s12 m10" style="margin-top: 15px;">ÚLTIMOS LANCES:</p>
			<table border="0" cellspacing="0" cellpadding="0" id="bidHistoryTable_<?php echo $auction['Auction']['id'];?>" style="margin: 3px; width:224px;">
				<thead>
				<tr>
					<td class="leilaoTd cinza s12" style="width:154px" align="center"><b>Usuário</b></td>
					<td class="leilaoTd cinza s12" style="width:70px" align="center"><b>Tipo</b></td>
				</tr>
				</thead>
				<tbody>
                <?php
					if( isset($auction['Bid']) && count($auction['Bid']) > 0):
						//$price_temp = $auction['Auction']['price'];
						foreach($auction['Bid'] as $bid):
				?>
				<tr>
					<td class="leilaoTd cinza s12" align="center"><?php echo $bid['User']['username']; ?></td>
					<td class="leilaoTd cinza s12" align="center"><?php echo $bid['is_auto'] ? "AutoBID" : "Normal"; ?></td>
				</tr>
                <?php
							//$price_temp -= 0.01;
						endforeach;
                	else:
						if (!$isClosed):
				?>
                <tr>
                    <td colspan="2" class="leilaoTd cinza s12"><b>Saia na frente. Seja o primeiro a dar um LANCE!</b></td>
                </tr>
                <?php
						else:
				?>
                <tr>
                    <td colspan="2" class="leilaoTd cinza s12"><b>Infelizmente este leilão já terminou!</b></td>
                </tr>
				<?php
                		endif;
                	endif;
				?>
                </tbody>
            </table>
		</div>
        <div class="leilaoInfoLeft">
            <p class="s12 cinza">PREÇO DO LEILÃO:</p>
            <p class="azul2 s30"><b><span class="cinza">R$</span> <span id="price_<?php echo $auction['Auction']['id'];?>"><?php echo $price; ?></span></b></p>
            <div style="height: 13px">&nbsp;</div>
            <p class="s12 cinza">TEMPO RESTANTE:</p>
            <?php if (!$isClosed && !$isSuspended): ?>
			<p class="s24 vermelho">
				<span style="width: 37px; height: 40px; display: inline-block; position: relative; left: 0px; top: 10px;">
					<img src="https://c14985174.ssl.cf2.rackcdn.com/img/<?php echo $clock_img; ?>" width="37" height="40" />
				</span>
				<span id="timer_<?php echo $auction['Auction']['id'];?>" class="countdown" title="<?php echo $auction['Auction']['end_time'];?>" style="display: inline-block; position: relative; left: 0px; top: -12px;">--:--:--</span>
			</p>
            <div style="height: 15px">&nbsp;</div>
            
            <div class="budLarge" id="bidmainbtn_<?php echo $auction['Auction']['id'];?>"><a id="bidlnk_<?php echo $auction['Auction']['id'];?>" class="bidlnk" href="#">&nbsp;</a></div>
			<?php elseif ($isSuspended): ?>
            <p class="s24 vermelho">
				<span style="width: 37px; height: 40px; display: inline-block; position: relative; left: 0px; top: 10px;">
					<img src="https://c14985174.ssl.cf2.rackcdn.com/img/<?php echo $clock_img; ?>" width="37" height="40" />
				</span>
				<span id="timer_<?php echo $auction['Auction']['id'];?>" class="countdown suspended arrematado" title="<?php echo $auction['Auction']['end_time'];?>" style="display: inline-block; position: relative; left: 0px; top: -12px;">Suspenso!</span>
			</p>
            <div style="height: 15px">&nbsp;</div>
            <div class="budLarge" id="bidmainbtn_<?php echo $auction['Auction']['id'];?>"><a id="bidlnk_<?php echo $auction['Auction']['id'];?>" class="bidlnk" href="#">&nbsp;</a></div>
			<?php else: ?>
            <p class="s24 vermelho">
				<span style="width: 37px; height: 40px; display: inline-block; position: relative; left: 0px; top: 10px;">
					<img src="https://c14985174.ssl.cf2.rackcdn.com/img/<?php echo $clock_img; ?>" width="37" height="40" />
				</span>
				<span id="timer_<?php echo $auction['Auction']['id'];?>" class="arrematado" title="<?php echo $auction['Auction']['end_time'];?>" style="display: inline-block; position: relative; left: 0px; top: -12px;">Arrematado!</span>
			</p>
            <div style="height: 15px">&nbsp;</div>
            <div class="budLarge"><a class="ended" href="#">&nbsp;</a></div>
			<?php endif; ?>

            <p class="s10 cinza" style="margin-left: -12px; margin-top: 10px; margin-bottom: 10px; text-align:center">
				<span id="leader_<?php echo $auction['Auction']['id'];?>" class="leader" style="font-size: 15px;"><?php if(isset($auction['Winner'])) echo $auction['Winner']['username']; ?>&nbsp;</span>
            </p>

			<p class="s10 cinza" style="position:relative; top: 15px; margin-left: -8px; margin-top: 10px; margin-bottom: 10px; display:none;">Cada Lance aumenta o preço em R$0,01</p>

            <div class="compreAgora" style="background: url(images/btn_compre_agora.png);width: 145px; height: 31px; float: left; display:none"></div>
        </div><!-- leilaoInfoLeft -->
        <div style="clear: both"></div>
		<div id="cl_<?php echo $auction['Auction']['id'];?>" style="display:none;"><?php echo $isClosed || $isSuspended;?></div>
		<div id="et_<?php echo $auction['Auction']['id'];?>" style="display:none;"><?php echo $isSuspended ? 999999999999 : $auction['Auction']['end_timestamp'];?></div>
		<div id="tl_<?php echo $auction['Auction']['id'];?>" style="display:none;"><?php echo $auction['Auction']['timestamp_ms'];?></div>
		<div id="ti_<?php echo $auction['Auction']['id'];?>" style="display:none;"><?php echo $auction['Auction']['time_increase'];?></div>
		<div id="pr_<?php echo $auction['Auction']['id'];?>" style="display:none;"><?php echo $uprice; ?></div>
	</div><!-- leilaoInfo -->
	</div><!-- leilaoAtualiza -->

	<div class="leilaoRegras" style="min-height: 150px; width: 492px">
	    <div class="grayHeader">REGRAS DO LEILÃO</div>
    	<div style="position: relative;">
    		<table cellspacing="0" style="padding: 10px 5px;">
			<?php
			$hasAnyRule = false;
			if($auction['Auction']['is_free_ship']) {
				$hasAnyRule = true;
				echo "<tr><td valign=\"middle\" style=\"padding: 0px 5px 5px 0px;\"><img src=\"https://c14985174.ssl.cf2.rackcdn.com/img/ico_frete.png\" /></td><td valign=\"middle\" class=\"s12 cinza\" style=\"padding-bottom: 5px;\"><b class=\"boldBlack\">Frete Grátis.</b>&nbsp;</td></tr>"; //$auction['Auction']['free_ship_text']
			} elseif ($auction['AuctionDetail']['delivery_cost'] > 0)  {
				$hasAnyRule = true;
				echo "<tr><td valign=\"top\" style=\"padding: 0px 5px 10px 0px;\"><img src=\"https://c14985174.ssl.cf2.rackcdn.com/img/ico_frete.png\" /></td><td valign=\"middle\" class=\"s12 cinza\" style=\"padding-bottom: 5px;\"><b class=\"boldBlack\">Frete</b>: O valor do frete deste leilão é de R$". $delivery_cost .".&nbsp;</td></tr>";
//			} else {
//				$hasAnyRule = true;
//				echo "<tr><td valign=\"top\" style=\"padding: 0px 5px 10px 0px;\"><img src=\"https://c14985174.ssl.cf2.rackcdn.com/img/ico_frete.png\" /></td><td valign=\"middle\" class=\"s12 cinza\" style=\"padding-bottom: 5px;\"><b class=\"boldBlack\">Este leilão não tem frete.</b>&nbsp;</td></tr>";
			}
			if($auction['Auction']['has_discounts'] && $auction['AuctionDiscount']) {
				$hasAnyRule = true;
				echo "<tr><td valign=\"top\" style=\"padding: 0px 5px 10px 0px;\"><div style=\"height:37px; width: 36px; background: url(https://c14985174.ssl.cf2.rackcdn.com/img/ico_porcento.png);\"></div></td><td valign=\"top\" class=\"s12 cinza\" style=\"padding-bottom: 5px;\"><b class=\"boldBlack\">Desconto</b>: <span id=\"discounts_rotator\">";
				foreach ($auction['AuctionDiscount'] as $discount):
					echo "<span id=\"discount_". $discount['min_bids'] ." class=\"discounts_rotate\" style=\"display: none;\">". $discount['discount'] ."% na compra deste produto para quem der ". $discount['min_bids'] ." lances ou mais.</span>";
				endforeach;
				echo "</span></td></tr>";
			}
			if($auction['Auction']['is_price_limited']) { $hasAnyRule = true; echo "<tr><td valign=\"top\" style=\"padding: 0px 5px 10px 0px;\"><div style=\"height:35px; width: 36px; background: url(https://c14985174.ssl.cf2.rackcdn.com/img/ico_cifra.png);\"></div></td><td valign=\"top\" class=\"s12 cinza\" style=\"padding-bottom: 5px;\"><b class=\"boldBlack\">Preço Limitado</b>: O valor máximo deste leilão é R$". $auction['Auction']['price_max'] .".</td></tr>"; }
			if($auction['Auction']['is_charity']) { $hasAnyRule = true; echo "<tr><td valign=\"top\" style=\"padding: 0px 5px 10px 0px;\"><div style=\"height:35px; width: 36px; background: url(https://c14985174.ssl.cf2.rackcdn.com/img/ico_beneficente.png);\"></div></td><td valign=\"top\" class=\"s12 cinza\" style=\"padding-bottom: 5px;\"><b class=\"boldBlack\">Beneficente</b>: ". $auction['Auction']['charity_text'] ."&nbsp;</td></tr>"; }

			if(!$hasAnyRule) { echo "<tr><td valign=\"top\" align=\"center\" class=\"s12 cinza\"><b class=\"boldBlack\">Este leilão segue todas as regras normais do site.</b></td></tr>"; }

			if($isClosed && $this->Session->check('Auth.User') && $auction['Auction']['has_discounts'] && $auction['AuctionDiscount']) {
				echo "<tr><td colspan=\"2\" valign=\"top\" align=\"center\" class=\"s12 cinza\"><b class=\"boldBlack\">Participou do leilão e não arrematou?</b><br/>";
				echo $this->Html->image("https://c14985174.ssl.cf2.rackcdn.com/img/btn_compre_agora.png", array(
						'url' => array('controller' => "bought_rights", 'action' => "index", 'admin' => false, 'meu' => true),
						'style' => "width: 178px; height: 32px;"
				));
				echo "</td></tr>";
			}
			?>
		    </table>
		</div>
	</div><!-- leilaoRegras -->
	<script type="text/javascript">
		$('#discounts_rotator span').hide();
		$('#discounts_rotator span:last').show(); 
		Rotate();

		function Rotate(){
			var current = $('#discounts_rotator span:visible');
			var next = current.next().length ? current.next() : current.parent().children(':first'); 
			current.hide();
			next.css({'background-color': 'gray'}).show().animate({backgroundColor: '#FFF'}, 1000);
			setTimeout(Rotate, 5000);
		}
	</script>

	<div class="leilaoVantagens">
        <div class="grayHeader">INFORMAÇÕES DO LEILÃO</div>
        <table cellspacing="0" style="margin: 10px;">
            <tbody>
            <tr>
                <td style="text-align: right;" class="p5 s12 boldBlack">Preço:</td>
                <td class="cinza">R$<span id="store_price_<?php echo $auction['Auction']['id'];?>"><?php echo $price_store;?></span></td>
            </tr>
            <tr>
                <td style="text-align: right;" class="p5 s12 boldBlack">Economia:</td>
                <td class="vermelho"><b>R$<span id="saving_<?php echo $auction['Auction']['id'];?>"><?php echo $saving;?></span></b></td>
            </tr>
            <tr>
                <td style="text-align: right;" class="p5 s12 boldBlack">Desconto:</td>
                <td class="azul"><b><span id="discount_<?php echo $auction['Auction']['id'];?>"><?php echo $saving_percent;?></span>%</b></td>
            </tr>
            <!--nocache-->
            <?php if (isset($loggedUser['User']['id'])): ?>
            <tr>
                <td style="text-align: right;" class="p5 s12 boldBlack">Seus Lances:</td>
                <td class="cinza"><span id="myBidsCount_<?php echo $auction['Auction']['id'];?>" class="myBidsCount"><?php echo $userBidsOnAuction; ?></span></td>
            </tr>
            <?php endif; ?>
            <!--/nocache-->
			</tbody>
		</table>
	</div><!-- leilaoVantagens -->

<?php
//echo $this->Html->scriptStart(array('inline' => true));
//$.ajax({
//	url: '<?php echo Router::url(array('controller' => 'auctions', 'action' => 'updateinfo', $auction['Auction']['id'], 'admin' => false, 'meu' => false), true); ? >',
//	dataType: 'html',
//	timeout: 1000,
//	async: true,
//	global: false,
//	cache: false,
//	success: function(data){
//    	$("#leilaoAtualiza").html(data);
//	},
//	error: function(XMLHttpRequest, textStatus, errorThrown){
//	}
//});
//<?php echo $this->Html->scriptEnd();
/*
	<div class="leilaoChat">
		<div class="grayHeader">CHAT<span class="s10 branco" style="margin-left: 40px;">O Chat será aberto quando faltar 1 hora para o cronometro zerar</span></div>
		<div id="chatTXTaguarde" class="chatTXT">
			Aguarde!!!
		</div>
		<div id="chatTXT" class="chatTXT" style="display:none">
			<div style="display:none">
            	<b>19:33:14</b> |  <span>joão_carlos </span>disse para <span>Todos</span>: Olá. Boa sorte!<br>
			</div>
        </div>
        <form action="" method="post" style=" background: #eeeeee; height: 48px;  border-top: 1px solid gray;   border-radius: 0px 0px 10px 10px; position: relative;">
            <label>Para:</label>
            <input type="text" class="inputText2" name="" value="TODOS" style="width: 100px; position: absolute; left: 55px; top: 8px; height: auto; padding-top: 7px; padding-bottom: 6px;">
            <select name="" style="width: 200px; font: 12px Arial; position: absolute; top: 8px; left: 175px;">
                <option value="">Selecione uma mensagem</option>
                <option>Olá, Boa sorte!</option>
                <option>Não vou desistir!</option>
                <option>Quero muito esse produto!</option>
                <option>Vou comprar mais lances!</option>
                <option>Estou com uma tática imbatível!</option>
                <option>Tenho muitos lances ainda!</option>
                <option>Economize seus lances, esse eu vou levar!</option>
                <option>Chega! Desisto!</option>
                <option>Seus lances não acabam?</option>
                <option>Cheguei para ganhar!</option>
            </select>
            <div class="OK" style="position: absolute; top: 9px; left: 380px;"></div>
        </form>
    </div><!-- leilaoChat -->
*/
?>
	<div class="leilaDesc">
    	<a name="item_description" id="anchor_item_description"></a>
		<div class="grayHeader">DESCRIÇÃO DO PRODUTO</div>
			<div class="s12 cinza leilaoDescricaoTXT" style="padding: 10px;">
				<?php echo $auction['Item']['description'];?>
        </div><!-- leilaoDescricaoTXT -->
    </div><!-- leilaDesc -->
</div><!-- leilaoProdutoHolder -->
<div id="timestamp" style="display: none">0</div>

<?php
//$this->Html->script(array('https://c14985174.ssl.cf2.rackcdn.com/js/gettime.min.js', 'https://c14985174.ssl.cf2.rackcdn.com/js/plugins.color.min.js'), array('inline' => false));
$this->Html->script('https://c14985174.ssl.cf2.rackcdn.com/js/plugins.color.min.js', array('inline' => false));
echo $this->Html->scriptStart(array('inline' => true));
//$(document).ready(function(){
	//setInterval('clkCycle()',clkCycleTime);
	//var timestamp = 0;
	//clkCycle();
//});
echo "gettime();";
echo $this->Html->scriptEnd();
?>