<?php
if (!empty($auction)):

$title = $this->Text->truncate($auction['Auction']['title'], 60, array('ending' => "...", 'truncate' => true, 'html' => false));
$image_tag = $this->Image->image_out($auction['Item']['Image'], null, "medium", array('style' => 'width: 128px; height: 96px', 'alt' => $auction['Auction']['title'], 'title' => $auction['Auction']['title'], 'clear' => true, 'empty_image' => 'photo1.jpg' ));
$site = empty($auction['AuctionDetail']['site']) ? $auction['Store']['site'] : $auction['AuctionDetail']['site'];

$price_store = number_format( (($auction['Auction']['price_store'] > 0) ? $auction['Auction']['price_store'] : 0) , 2, ',', '');

$price = number_format($price, 2, ',', '');
$shipment = number_format($shipment, 2, ',', '');
$total = number_format($total, 2, ',', '');
$saving =  number_format($saving, 2, ',', '');
$discount = number_format($discount, 2, ',', '');
?>

<div id="conteudoTitulo">
	<h2>Parabéns, você venceu!</h2>
    <div style="clear: both;"></div>
</div><!-- conteudoTitulo -->


<div id="leilaoVencedorInPage">
    <div id="parabens"></div>
    <div id="descricao">
        <p class="lightGrey">Leilão</p>
        <p class="nome"><?php echo $title; ?></p>
        <p class="lightGrey">Total a Pagar:</p>
        <p class="valor" style="width: 130px; text-align:left; font-size: 24px">R$ <span class="verde"><?php echo $total; ?></span></p>

		<?php //echo $this->Html->link("", array('action' => 'pay_moip', $auction['Auction']['id']), array('class'=> 'pagarAgora', 'target' => "_blank"), false, false); ?>
		<?php echo $this->Html->link("", array('controller' => 'payments', 'action' => 'moip_buy', 'auction', $auction['Auction']['id'], 'meu' => true, 'admin' => false), array('class'=> 'pagarAgora'), false, false); ?>

        <p class="lightGrey">INFORMAÇÕES DO LEILÃO</p>
        <style type="text/css">td { padding: 3px; }</style> 
        <table cellspacing="0" style="margin-top: 5px;">
            <tr>
                <td class="subTitle">Arremate:</td>
                <td class="value"> R$ <?php echo $price; ?></td>
            </tr>
            <tr>
                <td class="subTitle">Frete:</td>
                <td class="value"> R$ <?php echo $shipment; ?></td>
            </tr>
            <tr>
                <td class="subTitle">Seus BIDs:</td>
                <td class="value"> <?php echo $bidsCount; ?></td>
            </tr>
            <tr>
                <td class="subTitle">Valor no Parceiro:</td>
                <td class="value"> R$ <?php echo $price_store; ?></td>
            </tr>
            <tr>
                <td class="subTitle">Economia:</td>
                <td class="value"><span style="color: red; font-weight: bold"> R$ <?php echo $saving; ?></span></td>
                <td></td>
            </tr>
            <tr>
                <td class="subTitle">Desconto:</td>
                <td class="value"><span style="color: #006699; font-weight: bold"> <?php echo $discount; ?>%</span></td>
            </tr>
        </table>
        <div class="produtoVencedor">
			<?php echo empty($site) ? $image_tag : $this->Html->link($image_tag, $site, array('target' => "_blank", 'escape' => false)); ?>
        </div>
        <div class="dashedLine"></div>
    </div>
</div>
<?php endif; ?>
