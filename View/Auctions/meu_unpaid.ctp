<div class="arremateBlueBar" style="width: 420px; padding-left: 10px; border-left: 1px solid gray; border-radius: 10px 0px 0px 10px;">PRODUTO</div>
<div class="arremateBlueBar" style="width: 110px; text-align: center;">PREÇO</div>
<div class="arremateBlueBar" style="padding-left: 60px; width: 98px; border-right: 1px solid gray; border-radius: 0px  10px 10px 0px;">DATA</div>
<div style="clear: both;padding-bottom: 10px;"></div>
<?php
if (!empty($auctions)):
//debug($auctions);
	$i = 0;
	foreach ($auctions as $auction):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}

		$auction_title = $this->Text->truncate ($auction['Auction']['title'], 60, array('ending' => "...", 'exact' => true, 'html' => false));

		$temp_image = $this->Image->image_out($auction['Item']['Image'][0]['dir'], $auction['Item']['Image'][0]['filename'], 'small', array('class' => 'left'));
		$auction_link_array = array('controller' => 'auctions', 'action' => 'view', $auction['Auction']['slug'], 'meu' => false, 'admin' => false);
		
		$price = 0;
		if($auction['Auction']['is_price_limited']) {
			$price = min($auction['Auction']['price'], $auction['Auction']['price_max']);
		} else {
			$price = $auction['Auction']['price'];
		}
		
		//$discount = ($auction['Auction']['price_store'] > 0) ? number_format((1 - $price / $auction['Auction']['price_store'])*100, 2, ',', '') : 100;
		//$saving = ($auction['Auction']['price_store'] > 0) ? number_format($auction['Auction']['price_store'] - $price, 2, ',', '') : 0;
		//$saving_percent = ($auction['Auction']['price_store'] > 0) ? number_format((1 - $price / $auction['Auction']['price_store'])*100, 2, ',', '') : 0;
			
		$price = number_format($price/100, 2, ',', '');
		$price_store = number_format( ($auction['Auction']['price_store'] > 0) ? $auction['Auction']['price_store'] : 0, 2, ',', '');
		
		$shipping = 0;
		if ($auction['Auction']['is_free_ship'] == 0)
			$shipping = $auction['AuctionDetail']['delivery_cost'];
		$shipping = number_format($shipping, 2, ',', '');
?>


<div class="box" style="margin-bottom: 10px;">
	<div class="produtoBox" style="width: 420px;  border-right: 1px dashed gray; margin-left: 10px; height: 80px; margin-top: 20px; margin-bottom: 20px;">
		<div class="arrematadosIMG" style="width: 100px; height: 75px; margin-right: 20px; float: left;">
			<?php echo $this->Html->link($temp_image, $auction_link_array, array('escape' => false)); ?>
        </div>
		<p style="margin: 5px; font-weight: bold; height: 40px;"><?php echo $this->Html->link($auction_title, $auction_link_array, array('class' => 'bold', 'style' => 'font-size: 16px;', 'escape' => false)); ?></p>
<?php
if ($auction['Auction']['unpaid'] == 1 && !$auction['Auction']['expired'])
        echo $this->Html->link($this->Html->image("https://c14985174.ssl.cf2.rackcdn.com/img/btn_comprar.png", array('alt' => 'Comprar', 'style' => "width: 90px; height: 30px")), array('controller' => 'auctions', 'action' => 'won', $auction['Auction']['id']), array('class' => "compreAgora", 'style' => "width: 145px; height: 31px; float: left;", 'escape' => false));
?>
	</div>
	<div class="arremateValor azul2" style="position: absolute; top: 20px; left: 430px; margin: 0; width: 109px; border-right: 1px dashed gray; height: 65px; padding-top: 15px; font-weight: bold; text-align: center;">
		R$<?php echo $price; ?>
	</div>
	<p style="margin: 0px; position: absolute; top: 65px; left: 430px; width: 110px; text-align: center; font-size: 10px;">
		+ Frete<br />
		ao invés de<br>R$<?php echo $price_store; ?>
    </p>
    
	<p style="position: absolute; top: 20px; left: 550px; margin: 0; width: 134px; height: 65px; padding-top: 15px;  text-align: center;">
		Fechou em:<br><b> <?php echo date('d/m/Y', strtotime( $auction['Auction']['end_time'] )); ?><br> às <?php echo date('H:i', strtotime( $auction['Auction']['end_time'] )); ?></b>
	</p>
</div>
<?php
	endforeach;
else:
?>
<div class="box">
	<p style="margin:15px;">Você não tem nenhum leilão para pagar.</p>
</div><!-- box -->
<?php endif; ?>
