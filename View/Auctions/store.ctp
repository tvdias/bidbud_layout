<?php
// //$javascript->codeBlock(null, array('inline' => false));
// echo $this->Html->scriptStart(array('inline' => false));
// //echo 'session="'.session_id() .'"';
// //echo "current_sid = '". session_id() ."'";
// echo "user_logged_in = ". (isset($loggedUser) ? "true" : "false") .";";

// //$javascript->blockEnd();
// echo $this->Html->scriptEnd();
?>

<!--nocache-->
<?php if (empty($auctions) && empty($ended_auctions)): ?>
<div class="topBox">
	<div class="topBoxMSG">Desculpe</div>
	<p class="s16 cinza" style="position: absolute; left: 50px; top: 80px;">Nenhum leilão encontrado.</p>
	<div id="topBoxIMG"></div>
</div>
<?php
else:
	if (!empty($auctions)):
?>
<div id="leiloesaovivo" style="width: 960px;">
	<p style="float:left;font-weight: bold; font-size: 18px;color: black;">Leilões ao vivo</p>
	<div style="clear: both;"></div>
	<div id="produtosHolder" style="width: 960px;">
<?php
//echo $this->element('auction_items', array('auctions' => $auctions, 'cache' => "+10 minutes"));
	$auction_ids = array();

	foreach($auctions as $auction):
		$auction_ids[] = $auction['Auction']['id'];

		$auction_title = $this->Text->truncate($auction['Auction']['title'], 46, array('ending' => '...', 'exact' => true, 'html' => false));

		$image_tag = $this->Image->image_out($auction['Item']['Image'], null, "medium", array('style' => 'width: 128px; height: 96px', 'alt' => $auction['Auction']['title'], 'title' => $auction['Auction']['title'], 'clear' => true, 'empty_image' => 'photo1.jpg' ));
		//$image_tag2 = $image->image_out($auction['Image'], null, "medium", array('style' => 'width: 88px; height: 31px', 'alt' => $auction['Auction']['title'], 'title' => $auction['Auction']['title'], 'clear' => true, 'empty_image' => 'photo1.jpg' ));
		//$image_seller = $this->Image->avatar($auction["Item"]['user_id'], 'seller_small', array('style' => 'width: 90px; height: 30px', 'class' => "homeSellerIMG", 'alt' => "logo do parceiro", 'clear' => true, 'empty_image' => 'photo1.jpg' ));
		//$image_seller = $this->Image->store($auction["Store"]['id'], 'small', array('style' => 'width: 90px; height: 30px', 'class' => "homeSellerIMG", 'alt' => "logo do parceiro", 'clear' => true, 'empty_image' => 'photo1.jpg' ));
		$image_seller = $this->Image->image_out($auction["Store"]['Image'], null, 'small', array('width' => '90', 'height' => '30', 'style' => 'width: 90px; height: 30px', 'class' => "homeSellerIMG", 'alt' => "logo do parceiro", 'clear' => true, 'empty_image' => 'photo1.jpg' ));
		$auction_link_array = array('controller' => 'auctions', 'action' => 'view', $auction['Auction']['slug']);
		
		//$isClosed = !(!isset($auction['Auction']['closed']) || $auction['Auction']['closed'] == 0 );
		$isClosed = isset($auction['Auction']['closed']) && $auction['Auction']['closed'] == 1;
		// TODO : get status_id from cache/config/DB
		$isSuspended = ($auction['Auction']['status_id'] == 19);

		$price = 0;		
		if($auction['Auction']['is_price_limited']) {
			$price = min($auction['Auction']['price'], $auction['Auction']['price_max']);
		} else {
			$price = $auction['Auction']['price'];
		}
		$uprice = $price;
		$price = number_format($price/100, 2, ',', '');
?>
<div class="produto" name="auction_element" id="auction_<?php echo $auction['Auction']['id'];?>">
	<div class="produtoBoxTop">
		<div class="icons" style="width: 10px; float: left;"></div>

		<?php if($auction['Auction']['is_price_limited'] == 1) { echo "<img class=\"icons\" src=\"https://c14985174.ssl.cf2.rackcdn.com/img/ico_cifra_white.png\">"; } ?>
		<?php if($auction['Auction']['has_discounts'] == 1) { echo "<img class=\"icons\" src=\"https://c14985174.ssl.cf2.rackcdn.com/img/ico_porcento_white.png\">"; } ?>
		<?php if($auction['Auction']['is_free_ship'] == 1) { echo "<img class=\"icons\" src=\"https://c14985174.ssl.cf2.rackcdn.com/img/ico_frete_white.png\">"; } ?>
		<?php if($auction['Auction']['is_charity'] == 1) { echo "<img class=\"icons\" src=\"https://c14985174.ssl.cf2.rackcdn.com/img/ico_beneficente_white.png\">"; } ?>

		<img class="icons closeBox" style="float: right;" src="https://c14985174.ssl.cf2.rackcdn.com/img/ico_close_white.png">
	</div>
	<div class="produtoBoxBody">
		<h3><?php echo $this->Html->link($auction_title, $auction_link_array); ?></h3>
		<div class="fotoProd">
			<div style="width: 190px; height: 100px; margin-left: 15px;">
				<?php echo $this->Html->link($image_tag, $auction_link_array, array('escape' => false)); ?>
			</div>
		</div>
		<?php if (!$isClosed && !$isSuspended): ?>
		<h4><span id="timer_<?php echo $auction['Auction']['id'];?>" class="countdown">--:--:--</span></h4>
		<?php elseif ($isSuspended): ?>
		<h4><span id="timer_<?php echo $auction['Auction']['id'];?>" class="countdown suspended arrematado">Suspenso!</span></h4>
		<?php else: ?>
		<h4><span id="timer_<?php echo $auction['Auction']['id'];?>" class="arrematado">Arrematado!</span></h4>
		<?php endif; ?>
		<div class="valor">R$ <span id="price_<?php echo $auction['Auction']['id'];?>" class="price"><?php echo $price; ?></span></div>
		<div class="prodUser" style="width: 100%; text-align: center;"><?php if($discount > 0): ?><span id="discount_<?php echo $auction['Auction']['id'];?>"><?php echo $discount; ?></span>% de desconto!<?php else: echo "&nbsp"; endif; ?></div>
		<div class="bidmainbtn" id="bidmainbtn_<?php echo $auction['Auction']['id'];?>" style="height:35px; margin: 5px auto;"><a id="bidlnk_<?php echo $auction['Auction']['id'];?>" class="bidlnk" href="#">&nbsp;</a></div>
		<div class="prodUser" id="leader_<?php echo $auction['Auction']['id'];?>" style="width: 100%; text-align: center;"><?php echo $auction['Winner']['username'];?>&nbsp;</div>
	</div>
	<div id="cl_<?php echo $auction['Auction']['id'];?>" style="display:none;"><?php echo $isClosed || $isSuspended;?></div>
	<div id="et_<?php echo $auction['Auction']['id'];?>" style="display:none;"><?php echo $isSuspended ? 999999999999 : $auction['Auction']['end_timestamp'];?></div>
	<div id="tl_<?php echo $auction['Auction']['id'];?>" style="display:none;"><?php echo $auction['Auction']['timestamp_ms'];?></div>
    <div id="ti_<?php echo $auction['Auction']['id'];?>" style="display:none;"><?php echo $auction['Auction']['time_increase'];?></div>
	<div id="pr_<?php echo $auction['Auction']['id'];?>" style="display:none;"><?php echo $uprice; ?></div>
</div>
<?php
endforeach;

//$this->Html->script(array('https://c14985174.ssl.cf2.rackcdn.com/js/gettime.min.js', 'https://c14985174.ssl.cf2.rackcdn.com/js/plugins.color.min.js'), array('inline' => false));
$this->Html->script('https://c14985174.ssl.cf2.rackcdn.com/js/plugins.color.min.js', array('inline' => false));
echo $this->Html->scriptStart(array('inline' => false));
// $(document).ready(function(){
// 	//setInterval('clkCycle()',clkCycleTime);
// 	var timestamp = 0;
// 	clkCycle();
// 	gettime();
// });
echo "gettime();";
echo $this->Html->scriptEnd();
?>
		<div id="timestamp" style="display: none">0</div>
	</div>
	<?php echo $this->element('search_pagination'); ?>
</div>
<?php
	endif;
	if (!empty($ended_auctions)):
?>
<div id="leiloesareematados" style="width: 960px;">
	<p style="float:left;font-weight: bold; font-size: 18px;color: black;">Leilões Arrematados</p>
	<p style="float:right;font-weight: bold; font-size: 14px;color: black; display: none;"><?php echo $this->Html->link("ver mais", array('controller' => 'auctions', 'action' => 'winner'), array(), false, false); ?> »</p>
	<div style="clear: both;"></div>
	<div id="arrematadosHolder" style="width: 960px;">
<?php
		//echo $this->element('auction_ended_items', array('auctions' => $ended_auctions, 'cache' => "+10 minutes"));
//$number_format = array('places' => 2, 'before' => 'R$', 'escape' => false, 'decimals' => ',', 'thousands' => '.');
	foreach($ended_auctions as $auction):
		$auction_title = $this->Text->truncate($auction['Auction']['title'], 22, array('ending' => "...", 'exact' => true, 'html' => false));
		$image_tag = $this->Image->image_out($auction['Item']['Image'], null, "medium", array('alt' => $auction['Auction']['title'], 'title' => $auction['Auction']['title'], 'clear' => true, 'empty_image' => 'photo1.jpg' ));
		$auction_link_array = array('controller' => 'auctions', 'action' => 'view', $auction['Auction']['slug']);
//		$price = 0;
//		if($auction['Auction']['is_price_limited']) {
//			$price = min($auction['Auction']['price'], $auction['Auction']['price_max']);
//		} else {
//			$price = $auction['Auction']['price'];
//		}
//		
//		$saving = number_format($auction['Auction']['price_store'] - $price, 2, ',', '');
//		$discount = number_format((1 - $price / $auction['Auction']['price_store'])*100, 2, ',', '');
//		$price = number_format($price/100, 2, ',', '');


		$price = 0;
		if($auction['Auction']['is_price_limited']) {
			$price = min($auction['Auction']['price'], $auction['Auction']['price_max']);
		} else {
			$price = $auction['Auction']['price'];
		}
		$price = $price/100;
		
		//$discount = ($auction['Auction']['price_store'] > 0) ? number_format((1 - $price / $auction['Auction']['price_store'])*100, 2, ',', '') : 100;
		//$saving = ($auction['Auction']['price_store'] > 0) ? number_format($auction['Auction']['price_store'] - $price, 2, ',', '') : 0;
		$saving_percent = ($auction['Auction']['price_store'] > 0) ? number_format((1 - $price / $auction['Auction']['price_store'])*100, 2, ',', '') : 0;
			
		$price = number_format($price, 2, ',', '');
		//$price_store = ($auction['Auction']['price_store'] > 0) ? number_format($auction['Auction']['price_store'], 2, ',', '') : 0;
?>
<div class="produtoArrematado" style="width: 320px;">
    <div class="fotArrematado">
    	<?php echo $this->Html->link($image_tag, $auction_link_array, array('escape' => false)); ?>
    </div>
    <div class="arremateTitulo"><?php echo $this->Html->link($auction_title, $auction_link_array, array('title' => $auction['Auction']['title'], 'escape' => false)); ?></div>
    <div class="arremateValor">R$<?php echo $price; ?> <?php if($saving_percent > 0) echo"<span class=\"arremateDesconto\">". $saving_percent ."% de desconto</span>"; ?></div>
</div>
<?php
	endforeach;
?>
	</div>
	<div style="clear: both;"></div>
	<p style="float:right;font-weight: bold; font-size: 14px;color: black;"><?php echo $this->Html->link("ver mais", array('controller' => 'auctions', 'action' => 'winner'), array(), false, false); ?> »</p>
</div>
<?php
	endif;
endif;
?>
<!--/nocache-->