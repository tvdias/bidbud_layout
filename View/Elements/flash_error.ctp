<style>
.alert {
	border: 1px solid #999;
	padding: 15px;
	padding-left: 5px;
	margin-bottom: 15px;
	color: #fff;
	text-shadow: 0 1px 1px #000;
}

.alert:hover {
	cursor: pointer;
}

.alert img {
	margin:-5px 5px -8px;	
}

.alert.badge {
	position: absolute;
	display: block;
	z-index: 50;
	right: 0px !important;
	top:0px !important;
	margin: 0;
	text-align: center;
	padding: 0 3px;
	line-height: 10px;
	text-indent: 0;
	font-size: 11px;
	-moz-box-shadow:none;
	-webkit-box-shadow:none;
	box-shadow:none;
	-webkit-border-radius: 0px;
	-moz-border-radius: 0px;
	border-radius: 0px;
	-webkit-border-bottom-left-radius: 2px;
	-moz-border-radius-bottomleft: 2px;
	border-bottom-left-radius: 2px;
}

.alert {
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
}

.alert_red {
	background: #9e253b; /* old browsers */
	background: -moz-linear-gradient(top, #9e253b 0%, #7C1F30 99%); /* firefox */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#9e253b), color-stop(99%,#7C1F30)); /* webkit */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#9e253b', endColorstr='#7C1F30',GradientType=0 ); /* ie */
	border: 1px solid #7C1F30;
	-moz-box-shadow:0px 1px 0px rgba(255,255,255,1),inset 0px 1px 0px rgba(255,255,255,0.2);
	-webkit-box-shadow:0px 1px 0px rgba(255,255,255,1),	inset 0px 1px 0px rgba(255,255,255,0.2);
	text-shadow:0px -1px 1px rgba(000,000,000,1),0px 1px 0px rgba(255,255,255,0.2) border-radius: 3px;
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
	zoom:1;
}

.alert_red:hover {
	background: #7C1F30; /* old browsers */
	background: -moz-linear-gradient(top, #7C1F30 0%, #9e253b 99%); /* firefox */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#7C1F30), color-stop(99%,#9e253b)); /* webkit */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#7C1F30', endColorstr='#9e253b',GradientType=0 ); /* ie */
	zoom:1;	
}
</style>
<div class="alert alert_red">
<?php
	echo $this->Html->image("admin/icons/small/white/alert.png", array('alt' => "", 'height' => "24", 'width' => "24")) ." ". $message;
?>
</div>