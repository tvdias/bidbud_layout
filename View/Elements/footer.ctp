<div style="clear: both; height:5px">&nbsp;</div>
<div id="footer">
	<center>
		<div id="bottomMenu">
			<?php echo $this->Html->link("Sobre o Apetrexo Leilão", array('controller' => 'pages', 'action' => 'display', 'about', 'meu' => false), array());?> &nbsp; | &nbsp;
			<?php echo $this->Html->link("Fale conosco", "http://sac.apetrexoleilao.com.br", array('target' => '_blank'));?> &nbsp; | &nbsp;
			<?php echo $this->Html->link("Termos e Condições de Uso", array('controller' => 'pages', 'action' => 'display', 'agreement', 'meu' => false), array());?> &nbsp; | &nbsp;
			<?php echo $this->Html->link("Política de Privacidade", array('controller' => 'pages', 'action' => 'display', 'privacy', 'meu' => false), array());?>
			<?php //echo $this->Html->link("Ajuda", "/ajuda", array());?>
			<?php //echo $this->Html->link("FAQ", "/faq", array());?>
			<?php //echo $this->Html->link("Auditoria", "/auditoria", array());?>
	
			<span style="color: lightGrey; float: right;">apetrexoleilao.com.br © 2012</span>
	
			<div style="margin-top: 30px;">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="left"><a href="http://www.moip.com.br" target="_blank"><img src="https://c14985174.ssl.cf2.rackcdn.com/img/moip_pagamentos.jpg" border="0"></a></td>
						<td id="seals" align="right" style="width: 132px;">
							<div style="clear: both; padding: 5px;">
								<a href="http://www.bidbud.com.br" target="_blank"><?php echo $this->Html->image("https://c14985174.ssl.cf2.rackcdn.com/img/bidbud_2.png", array('alt' => "BidBud - Plataforma de Leilões")); ?></a>
							</div>
						</td>
					</tr>
				</table>
				<div style="width: 100%; font-size: 9px; color: gray; margin-top: 10px;">Imagens meramente ilustrativas. Para maiores informações sobre cada leilão, acesse sua página específica e verifique todas as condições de aquisição/utilização.</div>
			</div>
		</div>
	</center>
</div>
<div id="bottom_message" class="newbottominfo" style="display:none;">
	<div style="left: 50%; position: absolute; margin-left: -400px; width: 800px;">
		<p>&nbsp;</p>
	</div>
</div>
<!--nocache--><?php
$debug = Configure::read('debug');
$test_server = false;
if (!in_array($_SERVER['HTTP_HOST'], array("apetrexoleilao.com.br", "www.apetrexoleilao.com.br"))) $test_server = true;
if($test_server):
?>
<div style="display: block; background: #D2F0FF; position: fixed; left: 10px; top: 10px; padding: 3px 8px; z-index: 99;">
	ESTE É UM SERVIDOR DE TESTES!<br/>
	NENHUM LEILÃO É VÁLIDO!!
</div>
<?php endif; ?>
<div sub-key="" ssl="off" origin="pubsub.pubnub.com" id="pubnub"></div>
<?php
echo $this->Html->script("pubnub-3.1.min.js", array('inline' => true));
echo $this->Html->scriptStart(array('inline' => true));
?>
(function(){
	PUBNUB.subscribe({
		channel	: "<?php echo $test_server ? "test_auctions" : "auctions" ?>",
		error	: function() {
			<?php if($debug) echo "$('#debug_node').prepend('<li>Conexão perdida. Reconectando...</li>');"; ?>
	},
	callback : function(message) {
		<?php if($debug) echo "$('#debug_node').prepend('<li>Mensagem recebida</li>');"; //echo "$('#debug_node').prepend('<li>'+ message.st +' - '+ message.u +': '+ message.c +'/'+ message.p +'/'+ message.l +'</li>');"; ?>
		updateCnt(message);
	},
	connect	: function() {
		<?php if($debug) echo "$('#debug_node').prepend('<li>Conexão estabelecida.</li>');"; ?>
	}
	})
})();

$(document).ready(function(){
	seal_installSeal();
});
<?php
echo $this->Html->scriptEnd();
if($debug):
?>
<div style="clear:both; height: 10px;">&nbsp;</div>
<div style="width:100%">
<?php
	echo "<ul id=\"debug_node\"></ul>";
	echo $this->element('sql_dump');
?>
</div>
<?php
endif;
?>
<!--/nocache-->
<?php /*<a href="http://lanceseguro.com.br/rank/" style="visibility:hidden;"><img src="http://lanceseguro.com.br/rank/button.php?u=BidBud" alt="Lance Seguro - Ranking" border="0" /></a>*/ ?>