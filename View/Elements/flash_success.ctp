<style>
.alert {
	border: 1px solid #999;
	padding: 15px;
	padding-left: 5px;
	margin-bottom: 15px;
	color: #fff;
	text-shadow: 0 1px 1px #000;
}

.alert:hover {
	cursor: pointer;
}

.alert img {
	margin:-5px 5px -8px;	
}

.alert.badge {
	position: absolute;
	display: block;
	z-index: 50;
	right: 0px !important;
	top:0px !important;
	margin: 0;
	text-align: center;
	padding: 0 3px;
	line-height: 10px;
	text-indent: 0;
	font-size: 11px;
	-moz-box-shadow:none;
	-webkit-box-shadow:none;
	box-shadow:none;
	-webkit-border-radius: 0px;
	-moz-border-radius: 0px;
	border-radius: 0px;
	-webkit-border-bottom-left-radius: 2px;
	-moz-border-radius-bottomleft: 2px;
	border-bottom-left-radius: 2px;
}

.alert {
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
}

.alert_blue {
	background: #1C5EA0; /* old browsers */
	background: -moz-linear-gradient(top, #1C5EA0 0%, #064792 99%); /* firefox */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#1C5EA0), color-stop(99%,#064792)); /* webkit */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1C5EA0', endColorstr='#064792',GradientType=0 ); /* ie */
	border: 1px solid #064792;
	-moz-box-shadow:0px 1px 0px rgba(255,255,255,1),inset 0px 1px 0px rgba(255,255,255,0.2);
	-webkit-box-shadow:0px 1px 0px rgba(255,255,255,1),	inset 0px 1px 0px rgba(255,255,255,0.2);
	text-shadow:0px -1px 1px rgba(000,000,000,1),0px 1px 0px rgba(255,255,255,0.2)
	border-radius: 3px;
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
	zoom:1;
}

.alert_blue:hover {
	background: #064792; /* old browsers */
	background: -moz-linear-gradient(top, #064792 0%, #1C5EA0 99%); /* firefox */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#064792), color-stop(99%,#1C5EA0)); /* webkit */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#064792', endColorstr='#1C5EA0',GradientType=0 ); /* ie */
	zoom:1;	
}
</style>
<div class="alert alert_blue">
<?php
	echo $this->Html->image("admin/icons/small/white/alert.png", array('alt' => "", 'height' => "24", 'width' => "24")) ." ". $message;
?>
</div>