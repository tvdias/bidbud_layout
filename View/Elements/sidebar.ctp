<div id="sideBarTop"></div>
<div id="sideBarMiddle">
    <h1 style="margin-top:-50px;">
    	<span class="left">Tipos de Leilões</span>
        <span class="right">
        	<img class="close tiposClose" src="https://c14985174.ssl.cf2.rackcdn.com/img/ico_minimize.png">
            <img src="https://c14985174.ssl.cf2.rackcdn.com/img/ico_expand.png" class="close tiposClose" style="display: none;">
		</span>
	</h1>
    <span class="tiposBox" style="display: inline-block;">
        <?php /*<div style="clear: both"><img style="float: left" src="https://c14985174.ssl.cf2.rackcdn.com/img/ico_cifra.png"><h2>Preço Limitado</h2></div>*/ ?>
        <div style="clear: both"><img style="float: left" src="https://c14985174.ssl.cf2.rackcdn.com/img/ico_porcento.png"><h2>Vale Desconto</h2></div>
        <div style="clear: both"><img style="float: left" src="https://c14985174.ssl.cf2.rackcdn.com/img/ico_frete.png"><h2>Frete Grátis</h2></div>
        <?php /*<div style="clear: both"><img style="float: left" src="https://c14985174.ssl.cf2.rackcdn.com/img/ico_beneficente.png"><h2>Beneficente</h2></div>*/ ?>
        <div style="clear: both; height: 20px;"></div>
    </span>
    <?php /*
    <div class="sealsBox" style="display: inline-block; padding-left: 40px;">
    	<div class="siteblindadoseal"></div>
        <div style="clear: both; height: 10px;"></div>
    </div>
    */ ?>

    <h1><span class="left">Destaques</span><span class="right"><img class="close destaquesClose" src="https://c14985174.ssl.cf2.rackcdn.com/img/ico_minimize.png"><img src="https://c14985174.ssl.cf2.rackcdn.com/img/ico_expand.png" class="close destaquesClose" style="display: none;"></span></h1>
    <div id="destaquesLeftHolder">
		<?php
		echo $this->Html->image("https://c14985174.ssl.cf2.rackcdn.com/img/destaques/iphone.jpg");
		//echo $this->Html->link($this->Html->image("https://c14985174.ssl.cf2.rackcdn.com/img/destaques/iphone.jpg"), array('controller' => 'auctions', 'action' => 'view', 'ipad_2_apple_mc773bz_a_3g_16gb_wi_fi_ios_5_2_cameras_para_facetime_video_em_hd_preto_gratis'), array('escape' => false));
		?>
    </div>

    <?php if (isset($feedback)): ?>
	<h1>
		<span class="left">Depoimentos</span>
		<span class="right"><img class="close depoimentosClose" src="https://c14985174.ssl.cf2.rackcdn.com/img/ico_minimize.png"><img src="https://c14985174.ssl.cf2.rackcdn.com/img/ico_expand.png" class="close depoimentosClose" style="display: none;"></span>
	</h1>
	<?php
	//$feedback = ClassRegistry::init('Feedback')->getRandom(1, 0);
	$image_tag = $this->Image->image_out($feedback['Image'], null, "medium", array('alt' => "", 'width' => "150px", 'style' => "width: 150px; margin-left: 23px", 'clear' => true, 'empty_image' => "photo1.jpg" ));
	?>
	<div class="depoimentos">
		<?php echo $image_tag; ?><br />
		<div style="margin-top:6px;" class="title"><?php echo $feedback['User']['username']; ?></div>
		<div style="color: gray; font: normal 12px Arial, Helvetica;">
			<?php echo $feedback['Feedback']['text']; //$this->Text->truncate($feedback['Feedback']['text'], 500, array('ending' => '...', 'exact' => false, 'html' => true)); ?>
		</div>
	</div>
	<?php endif; ?>

	<h1>
		<span class="left">Facebook</span>
		<span class="right"><img class="close facebookClose" src="https://c14985174.ssl.cf2.rackcdn.com/img/ico_minimize.png"><img src="https://c14985174.ssl.cf2.rackcdn.com/img/ico_expand.png" class="close facebookClose" style="display: none;"></span>
	</h1>
	<iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2FApetreXoLeilao&amp;width=200&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;border_color&amp;stream=false&amp;header=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:200px; height:258px;" allowTransparency="true"></iframe>
</div>
<div id="sideBarBottom"></div>