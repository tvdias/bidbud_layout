<div id="sideBarTop"></div>
<div id="sideBarMiddle" style="width: 235px; padding-left: 13px;">

<?php if (!empty($sidebar_auctions)): ?>
	<h1 style="padding-bottom: 1px; width:220px; margin-top:-50px;">
		<span class="left">Leilões em Andamento</span>
		<span class="right" style="display: none;"><img class="close tiposClose" src="https://c14985174.ssl.cf2.rackcdn.com/img/ico_minimize.png"><img src="https://c14985174.ssl.cf2.rackcdn.com/img/ico_expand.png" class="close tiposClose" style="display: none;"></span>
	</h1>
<?php
$auction_ids = array();
foreach($sidebar_auctions as $auction):
	array_push($auction_ids, $auction['Auction']['id']);
	$auction_title = $this->Text->truncate($auction['Auction']['title'], 50, array('ending' => "...", 'exact' => true, 'html' => false));
	$image_tag = $this->Image->image_out($auction['Item']['Image'], null, "medium", array('style' => 'width: 100px; height: 75px', 'alt' => $auction['Auction']['title'], 'title' => $auction['Auction']['title'], 'clear' => true, 'empty_image' => 'photo1.jpg' ));
	$auction_link_array = array('controller' => 'auctions', 'action' => 'view', $auction['Auction']['slug']);

	//$isClosed = (!isset($auction['Auction']['closed']) || $auction['Auction']['closed'] == 0 ) ? 0 : 1;
	$isClosed = isset($auction['Auction']['closed']) && $auction['Auction']['closed'] == 1;
	// TODO : get status_id from cache/config/DB
	$isSuspended = ($auction['Auction']['status_id'] == 19);

	$price = 0;		
	if($auction['Auction']['is_price_limited']) {
		$price = min($auction['Auction']['price'], $auction['Auction']['price_max']);
	} else {
		$price = $auction['Auction']['price'];
	}
	$uprice = $price;
	$price = number_format($price/100, 2, ',', '');
?>
	<div id="auction_<?php echo $auction['Auction']['id'];?>" name="auction_element" class="roundedBorder brancoBg" style="height: 180px; width:220px; margin-top: 15px;">
		<div class="azulBg" style="height: 30px;border-radius: 10px 10px 0px 0px;">
			<div class="icons" style="width: 10px; float: left;"></div>
			<?php if($auction['Auction']['is_price_limited'] == 1) { echo "<img class=\"icons\" src=\"https://c14985174.ssl.cf2.rackcdn.com/img/ico_cifra_white.png\">"; } ?>
			<?php if($auction['Auction']['has_discounts'] == 1) { echo "<img class=\"icons\" src=\"https://c14985174.ssl.cf2.rackcdn.com/img/ico_porcento_white.png\">"; } ?>
			<?php if($auction['Auction']['is_free_ship'] == 1) { echo "<img class=\"icons\" src=\"https://c14985174.ssl.cf2.rackcdn.com/img/ico_frete_white.png\">"; } ?>
			<?php if($auction['Auction']['is_charity'] == 1) { echo "<img class=\"icons\" src=\"https://c14985174.ssl.cf2.rackcdn.com/img/ico_beneficente_white.png\">"; } ?>
			<img class="icons closeBox" style="float: right;" src="https://c14985174.ssl.cf2.rackcdn.com/img/ico_close_white.png">
		</div>
		<div class="p5">
			<div class="s12 azul bold" style="width: 100%; margin-bottom:5px; height:30px;"><?php echo $this->Html->link($auction_title, $auction_link_array, array('title' => $auction['Auction']['title'], 'class' => "s12 azul bold", 'escape' => false)); ?></div>
			<div class="floatLeft produto100x75">
				<?php echo $this->Html->link($image_tag, $auction_link_array, array('escape' => false)); ?>
            </div>
			<div style="text-align: center; width: 105px; margin: 0px; padding: 5px 5px 0px 105px; height: 70px;">
			<?php if (!$isClosed && !$isSuspended): ?>
				<div id="timer_<?php echo $auction['Auction']['id'];?>" class="s16 bold azul borderBottom" style="padding-bottom: 2px;">--:--:--</div>
			<?php elseif ($isSuspended): ?>
				<div id="timer_<?php echo $auction['Auction']['id'];?>" class="s16 bold azul borderBottom" style="padding-bottom: 2px;">Suspenso</div>
			<?php else: ?>
				<div id="timer_<?php echo $auction['Auction']['id'];?>" class="s16 bold red borderBottom" style="padding-bottom: 2px;">Arrematado!</div>
			<?php endif; ?>
				<div class="azul2 s14 bold" style="margin-top: 2px;">
					R$ <span id="price_<?php echo $auction['Auction']['id'];?>" class="price"><?php echo $price; ?></span>
				</div>
				<div id="leader_<?php echo $auction['Auction']['id'];?>" class="s10 cinza bold" style="text-align: center;"><?php echo $auction['Winner']['username'];?>&nbsp;</div>
			</div>
			<div id="bidmainbtn_<?php echo $auction['Auction']['id'];?>" class="budSmall floatLeft" style="margin: -12px 0px 0px 115px;"><a id="bidlnk_<?php echo $auction['Auction']['id'];?>" class="bidlnk" href="#">&nbsp;</a></div>
		</div>
		<div id="cl_<?php echo $auction['Auction']['id'];?>" style="display:none;"><?php echo $isClosed || $isSuspended;?></div>
		<div id="et_<?php echo $auction['Auction']['id'];?>" style="display:none;"><?php echo $isSuspended ? 999999999999 : $auction['Auction']['end_timestamp'];?></div>
		<div id="tl_<?php echo $auction['Auction']['id'];?>" style="display:none;"><?php echo $auction['Auction']['timestamp_ms'];?></div>
        <div id="ti_<?php echo $auction['Auction']['id'];?>" style="display:none;"><?php echo $auction['Auction']['time_increase'];?></div>
		<div id="pr_<?php echo $auction['Auction']['id'];?>" style="display:none;"><?php echo $uprice; ?></div>
	</div>
<?php
endforeach;
endif;
?>
</div>
<div id="sideBarBottom"></div>