<!-- Start Results Bar -->
<?php
$this->Paginator->options(array('url' => $this->passedArgs));
if (is_string($this->Paginator->numbers())):
?>
<center id="paginator" style="clear: both; padding: 40px;">
	<?php echo $this->Paginator->numbers(array('separator' => '', 'class' => "bottomNumbers", 'modulus' => 5, 'first' => 2, 'last' => 2)); ?>
</center>
<?php endif; ?>
<!-- End Results Bar -->