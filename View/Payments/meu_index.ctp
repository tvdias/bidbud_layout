<?php
$this->Number->addFormat('BRL', array('before' => 'R$ ', 'thousands'=>'.', 'decimals'=>',', 'places' => 2, 'zero' => "R$ 0"));
?>

<div id="caminho">
	<?php $this->Html->addCrumb('Meu Cadastro', array('controller' => 'users', 'action' => 'index', 'meu' => false)); ?>
	<p><?php echo $this->element('crumb'); ?></p>
	<div style="clear: both;"></div>
</div><!-- caminho -->

<div id="conteudoTitulo">
	<h2>Meus Pagamentos</h2>
	<div style="clear: both;"></div>
</div><!-- conteudoTitulo -->

<div class="box" style="margin-bottom: 20px; padding: 10px;">
	<font color="#666666" size="2">
	<table cellpadding="0" cellspacing="5" width="100%">
	<thead>
	<tr>
		<th align="left">Descrição</th>
		<th align="left" style="width: 110px;">Valor</th>
		<th align="left" style="width: 110px;">Frete</th>
		<th align="left" style="width: 110px;">Total</th>
		<th style="width: 110px;">&nbsp;</th>
	</tr>
	</thead>
	<tbody>
	<?php
	if ($payments):
	foreach ($payments as $payment): ?>
		<tr style="height: 32px">
			<?php /*<td width="120px"><?php echo $this->Time->niceShort($payment['Payment']['created']); ?>&nbsp;</td>*/ ?>
			<td><?php echo h($payment['Payment']['description']); ?>&nbsp;</td>
			<td><font color="#0099FF"><?php echo $this->Number->currency($payment['Payment']['value'], 'BRL'); ?>&nbsp;</font></td>
			<td><font color="#0099FF"><?php echo $this->Number->currency($payment['Payment']['shipping'], 'BRL'); ?>&nbsp;</font></td>
			<td><font color="#0099FF"><?php echo $this->Number->currency($payment['Payment']['value']+$payment['Payment']['shipping'], 'BRL'); ?>&nbsp;</font></td>
			<td align="center">
			<?php
				// 'new','initialized','authorized','not_authorized','canceled'
				switch ($payment['Payment']['status']) {
					case 'new':
						echo "Novo";
						break;
					case 'initialized':
						echo "Em andamento";
						break;
					case 'authorized':
						echo "Autorizado<br/>(". $this->Time->format('d/m/Y H:i', $payment['Payment']['authorized']) .")";
						break;
					case 'not_authorized':
						echo "Não aprovado";
						break;
					case 'canceled':
						echo "Cancelado";
						break;
				}
			?>
			</td>
		</tr>
	<?php
	endforeach;
	else:
	?>
		<tr><td colspan="5">Nenhum pagamento realizado.</td></tr>
	<?php
	endif;
	?>
	</tbody>
	</table>
    </font>
	<p style="display:none;">
	<?php
//	echo $this->Paginator->counter(array(
//	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
//	));
	?>	</p>

	<div class="paging" style="display:none;">
	<?php
//		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
//		echo $this->Paginator->numbers(array('separator' => ''));
//		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>