<?php
$this->Html->script('https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js', array('inline' => false));
$this->Html->script('http://www.moip.com.br/transparente/MoipWidget.js', array('inline' => false));
//$payment['Payment']['gateway_token'] = "D250B1Z2P0C3Y1N3E0H8J3O0I5Z1J4M7H4S0C040U0M051E6U748R8G0C2B3";
?>
<div id="MoipWidget" 
             data-token="<?php echo $payment['Payment']['gateway_token']; ?>" 
             callback-method-success="funcao_sucesso" 
             callback-method-error="funcao_falha" 
             callback-method-error-validation="funcao_falha_validacao" 
             callback-method-error-network="funcao_falha_comunicacao"></div>
<?php echo $this->Html->scriptStart(array('inline' => false)); ?>
var funcao_sucesso = function(data) {
	//CALLBACK DE SUCESSO
	//alert('Sucesso \n\n Mensagem: ' + data.Mensagem + ' \n\n ID Moip: ' + data.CodigoMoIP);
	//$("#sendToMoip").removeAttr("disabled");
	document.location.href='<?php echo $return_url; ?>';
};
$(function() {
	$("#processingpayment").dialog({
		autoOpen: false,
		modal: true,
		resizable: false,
		draggable: false,
		title: "Processando Pagamento"
	});
	$("#processingpayment_fail").dialog({
		autoOpen: false,
		modal: true,
		resizable: false,
		draggable: false,
		title: "Falha no Pagamento",
		buttons: {
			Ok: function() {
				$(this).dialog("close");
			}
		}
	});
});
<?php echo $this->Html->scriptEnd(); ?>
<style>
.ui-dialog-titlebar-close {
	display: none;
}
</style>
<h3>Dados da Compra</h3>
<div class="topBox" style="height: 86px; margin-top:-32px">
	<font size="2"><div style="margin-top:4px; margin-left:6px;" class="wrapper">
		<table class="valor-compra" summary="Tabela contendo dados contabilizados para pagamento do produto">
			<tbody>
			<tr class="total-descricao">
				<th style="color:#535353" align="left">Descrição:</th>
				<td style="color:#2199d5"><b><?php echo $payment['Payment']['description']; ?></b></td>
			</tr>
			<tr class="total-produtos">
				<th style="color:#535353" align="left">Total da compra:</th>
				<td style="color:#2199d5"><b>R$ <?php echo $payment['Payment']['value']; ?></b></td>
			</tr>
			<tr class="total-frete">
				<th style="color:#535353" align="left">Frete:</th>
				<td style="color:#2199d5"><b><?php echo $payment['Payment']['shipping'] ? $payment['Payment']['shipping'] : "Grátis"; ?></b></td>
			</tr>
			</tbody>
			<tfoot>
			<tr class="valor-total">
				<th style="color:#535353" align="left">Total a pagar:</th>
				<td style="color:#2199d5"><b>R$ <?php echo ($payment['Payment']['value'] + $payment['Payment']['shipping']); ?></b></td>
			</tr>
			</tfoot>
		</table> 
	</div>
    </font>
</div>

<h3 style="padding-top: 35px">Opções de Pagamento</h3>
<div class="topBox" style="margin-top:-30px; height:525px">
	<div id="ctl00_Conteudo_ctrlFormaPagamento_TodosPagamentos" class="pagamento" style="margin-left:6px; margin-top:8px;">
		<div id="ctl00_Conteudo_ctrlFormaPagamento_divFormaPagamentoCartao" class="formaPagamentoWrapper">
			<h5 id="cartao">Cartão de Crédito</h5>
			<div class="pagamento-cartao-wrapper">
				<div id="ctl00_Conteudo_ctrlFormaPagamento_VltSummaryCartao" class="aviso-erro" style="display:none;"></div>
				<div id="ctl00_Conteudo_ctrlFormaPagamento_divCartao1" class="cartao">
					<form>
					<fieldset class="selec-cartoes">
                    <font style="margin-top:2px" size="2" color="#666666">
						<legend><span><b>Selecione o cartão de crédito desejado:</b></span></legend>							
                        </font>
                        <font size="2" color="#666666">
						<ul style="list-style-type: none; margin-left:-55px; margin-top:7px; margin-bottom:7px;" class="lista-formas-pagamento lista-bandeiras">
							<li><label><input id="" name="OpcsCartao" value="Visa" class="" type="radio"><img style="margin-left:-56px;" src="https://c14985174.ssl.cf2.rackcdn.com/img/bandeiras/visa.png" /> - Visa</label></li>
							<li><label><input id="" name="OpcsCartao" value="VisaElectron" class="" type="radio"><img style="margin-left:-56px;" src="https://c14985174.ssl.cf2.rackcdn.com/img/bandeiras/visaelectron.png" /> - Visa Electron</label></li>
                            <li><label><input id="" name="OpcsCartao" value="Mastercard" class="" type="radio"><img style="margin-left:-56px;" src="https://c14985174.ssl.cf2.rackcdn.com/img/bandeiras/master.png" /> - Mastercard</label></li>
                            <li><label><input id="" name="OpcsCartao" value="AmericanExpress" class="" type="radio"><img style="margin-left:-56px;" src="https://c14985174.ssl.cf2.rackcdn.com/img/bandeiras/amex.png" /> - American Express</label></li>
                            <li><label><input id="" name="OpcsCartao" value="Dinners" class="" type="radio"><img style="margin-left:-56px;" src="https://c14985174.ssl.cf2.rackcdn.com/img/bandeiras/dinners.png" /> - Dinners Club</label></li>
							<li><label><input id="" name="OpcsCartao" value="Hipercard" class="" type="radio"><img style="margin-left:-56px;" src="https://c14985174.ssl.cf2.rackcdn.com/img/bandeiras/hipercard.png" /> - Hipercard</label></li>
                            <li><label><input id="" name="OpcsCartao" value="Aura" class="" type="radio"><img style="margin-left:-56px;" src="https://c14985174.ssl.cf2.rackcdn.com/img/bandeiras/aura.png" /> - Aura</label></li>
						</ul>
                        </font>
					</fieldset>
					<font size="2" style="color:#535353;"><b>Nome (como no Cartão):</b></font> <?php echo $this->Form->text('name', array('label' => false, 'div' => false, 'class' => "inputText", 'style' => "width: 170px;", "name" => "Nome", "default" => $payment['User']['Profile']['name'] ." ". $payment['User']['Profile']['name_last'])); ?><br />
					<font size="2" style="color:#535353"><b>Numero do Cartão:</b></font> <input style="margin-left:34px; margin-top:2px; width:170px;" type="text" id="Numero" name="Numero" value="" class="inputText" /><br />
					<font size="2" style="color:#535353"><b>Data da Expiração:</b></font> <input style="margin-left:33px; margin-top:2px; width:60px;" type="text" id="Expiracao" name="Expiracao" value="mm/aa" size="2" class="inputText" />
					<font size="2" style="color:#535353"><b>CVV:</b></font> <input style="margin-top:2px; width:60px;" type="text" id="CodigoSeguranca" name="CodigoSeguranca" value="" size="4" class="inputText" /><br />
					<font size="2" style="color:#535353"><b>Data de Nascimento:</b></font> <?php echo $this->Form->text('birthdate', array('label' => false, 'div' => false, 'class' => "inputText", 'style' => "width: 170px; margin-left:23px; margin-top:2px;", "name" => "DataNascimento", "default" => $this->Time->format('d/m/Y', $payment['User']['Profile']['birthdate']))); ?><br />
					<font size="2" style="color:#535353"><b>Telefone:</b></font> <?php echo $this->Form->text('phone', array('label' => false, 'div' => false, 'class' => "inputText", 'style' => "width: 170px; margin-left:95px; margin-top:2px;", "name" => "Telefone", "default" => $payment['User']['Profile']['phone_code1'] ."-". $payment['User']['Profile']['phone1'])); ?><br />
					<font size="2" style="color:#535353"><b>CPF:</b></font> <?php echo $this->Form->text('cpf', array('label' => false, 'div' => false, 'class' => "inputText", 'style' => "width: 170px; margin-left:124px; margin-top:2px;", "name" => "Identidade", "default" => $payment['User']['Profile']['cpf'])); ?><br />
                    </form>
                    <button style="background-color:#FFF; margin-top:5px; cursor:hand;" id="sendToMoip" onclick="sendmoip();"><img src="https://c14985174.ssl.cf2.rackcdn.com/img/btn_finalizar.png"/></button>
                    <br /><br />
                    <h5 id="cartao">Débito ou Boleto Bancário</h5>
					<p><font size="2" color="#666666">para selecionar pagar com débito ou boleto bancário </font><a href="<?php echo $payment['Payment']['gateway_payment_url']; ?>" target="_blank"><b>clique aqui</b></a>.</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="processingpayment" style="display: none;">
	<p>Porfavor aguarde enquanto processamos o seu pagamento...</p>
</div>
<div id="processingpayment_fail" style="display: none;">
	<p></p>
</div>	