<div id="caminho">
    <?php //$html->addCrumb('Meu BidBud', '/meubidbud'); ?>
	<p><?php echo $this->element('crumb'); ?></p>
	<div style="clear: both;"></div>
</div><!-- caminho -->

<div class="topBox">
	<div class="topBoxMSG" style="left: 30px; top: 20px;">Proceder com Pagamento</div>
	<p class="s16 cinza" style="position: absolute; left: 30px; top: 50px;">
		Seu sistema bloqueou o redirecionamento do nosso sistema de cobranças.<br/>
        Clique <?php echo $this->Html->link('aqui', $payment_url, array('class' => 's16', 'target' => '_blank', 'escape' => false)); ?> para prosseguir.
	</p>
	<div id="topBoxIMG"></div>
</div>