﻿<p>Parabéns <?php echo $user['username'];?>!</p>

<p>Você arrematou <?php echo $auction['title'];?> por R$<?php echo number_format($auction['price'], 2, ',', '');
if(isset($auction['shipment']) && $auction['shipment'] > 0) {
	echo " + R$". number_format($auction['shipment'], 2, ',', '') ." de frete";
}
if(isset($auction['winner_bids'])) {
	if ($auction['winner_bids'] == 1)
		echo " com 1 lance";
	elseif ($auction['winner_bids'] > 1)
		echo " com ". $auction['winner_bids'] ." lances";
}
?>.</p>

<p>Você tem 5 (cinco) dias corridos para realizar o pagamento.<br />
Para efetuar o pagamento acesse a sua página pessoal no site.</p>

<p style="font-weight: bold;">Quer ganhar 15 lances? Ao receber o produto (não é válido para pacotes de lances) envie o seu depoimento! Caso ele seja aceito, você receberá os lances diretamente na sua conta.</p>

<p style="font-weight: bold; color:#C00">Gostaríamos de ressaltar que, de acordo com os nossos temos de uso, você está limitado a 2 arremates a cada 24 horas e 5 a cada 30 dias.</p>