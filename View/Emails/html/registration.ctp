﻿<p>Bem vindo ao BidBud!</p>

<p>Seu cadastro foi realizado com sucesso!</p>

<p>Por questões de segurança é necessário confirmá-lo clicando <a href='<?php echo Router::url(array('controller' => 'users', 'action' => 'activation', urlencode($data['email']), $data['activation_code']), true);?>'>aqui</a>.<br />
Caso o link não funcione acesse a página <a href='<?php echo Router::url(array('controller' => 'users', 'action' => 'activation_pre'), true);?>'><?php echo Router::url(array('controller' => 'users', 'action' => 'activation_pre'), true);?></a> e digite o seu código de ativação.</p>

<p>Seu código de ativação é <strong><?php echo $data['activation_code'];?></strong></p>
