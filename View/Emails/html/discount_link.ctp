﻿<p>Olá <?php echo $data['User']['username']; ?>,</p>

<p>Como você sabe, o BidBud faz de tudo para que você fique satisfeito e consiga realizar as melhores compras. Dessa forma, conforme as regras do leilão <?php echo $data['Auction']['title']; ?>, enviamos abaixo um link exclusivo do parceiro <?php echo $data['Seller']['username'];?> com diversos produtos com <?php echo $data['Seller']['discount']; ?>% de desconto.</p>

<?php
if (substr($data['Seller']['cupon'], 0, 4) != "http")
	$data['Seller']['cupon'] = "http://". $data['Seller']['cupon'];
?>
<p>Link: <a href="<?php echo $data['Seller']['cupon']; ?>"><?php echo $data['Seller']['cupon']; ?></a></p>

<p>Mas atenção, este link é válido até <?php echo $data['Seller']['expiration_date']; ?>.</p>

<p>Não perca tempo! Entre no site do parceiro <a href="<?php echo $data['Seller']['cupon']; ?>"><?php echo $data['Seller']['username'];?></a> e boas compras!</p>