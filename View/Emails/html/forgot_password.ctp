﻿<p>Olá, <?php echo $data['User']['username'];?>!</p>

<p>Por motivos de segurança não armazenamos a sua senha. Para acessar a sua conta será necessário criar uma nova senha. <a href='<?php echo Router::url(array('controller' => 'users', 'action' => 'recoverpassword', $data['new_password']), true);?>'>Clique aqui para continuar</a>.</p>

<p>Caso tenha recebido esse email por engano, favor desconsiderar.</p>