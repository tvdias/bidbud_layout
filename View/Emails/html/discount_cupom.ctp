﻿<p>Olá <?php echo $data['User']['username']; ?>,</p>

<p>Como você sabe, o BidBud faz de tudo para que você fique satisfeito e consiga realizar as melhores compras. Dessa forma, conforme as regras do leilão <?php echo $data['Auction']['title']; ?>, enviamos abaixo um cupom de <?php echo $data['Seller']['discount']; ?>% de desconto para ser utilizado no site do parceiro <a href="<?php echo $data['Seller']['website']; ?>"><?php echo $data['Seller']['username'];?></a>.</p>

<p>Código do Cupom: <?php echo $data['Seller']['cupon']; ?></p>

<p>Mas atenção, este cupom é válido até <?php echo $data['Seller']['expiration_date']; ?> e poderá ser utilizado uma única vez pelo link deste e-mail.</p>

<p>Não perca tempo! Entre no site do parceiro <a href="<?php echo $data['Seller']['website']; ?>"><?php echo $data['Seller']['username'];?></a> e boas compras!</p>