﻿<?php
//$image_prod = $image->image_out($data['Auction']['img'], null, "medium", array( 'full_link' => true, 'border' => '0', 'width' => '100', 'height' => '75', 'clear' => true, 'alt' => '', 'empty_image' => 'photo1.jpg' ));
?>

<p>Olá<?php if(!empty($data['User']['username'])) echo ", ".$data['User']['username']; ?>!</p>

<p>O próximo leilão de <?php echo $html->link($data['Auction']['title'], $data['Auction']['url'], array('style' => "color: #006699; text-decoration: none"), false, false); ?> já está no ar. Você tem mais essa chance de arrematar seu sonho! A fase final do leilão está marcada para <?php echo $html->link(date('d/m/Y - H:i\h\s', $data['Auction']['date']), $data['Auction']['url'], array('style' => "color: #006699; text-decoration: none;"), false, false); ?>. Não perca!!!</p>

<?php if(!empty($data['message'])) echo "<p>". $data['message'] ."</p>"; ?>