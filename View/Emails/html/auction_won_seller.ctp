<p>Leilão finalizado: <?php echo $auction['title']; ?> (id: <?php echo $auction['id']; if ($auction['item_internalid']) echo " id do produto: ". $auction['item_internalid'];?>)

<p>Usuário:</p>
<ul>
	<li>Nome: <?php echo $user['name'];?></li>
	<li>E-mail: <?php echo $user['email'];?></li>
	<li>Apelido: <?php echo $user['username'];?></li>
	<li>ID: <?php echo $user['id'];?></li>
	<li>CPF: <?php echo $user['cpf'];?></li>
	<li>Telefone(s): <?php echo $user['phone'];?></li>
	<li>Endereço: <?php echo $user['address'];?></li>
</ul>

<p>Informações:</p>
<ul>
	<li>Preço: <?php echo number_format($auction['price'], 2, ',', ''); ?></li>
	<li>Frete: <?php echo number_format((isset($auction['shipping_cost']) ? $auction['shipping_cost'] : 0), 2, ',', ''); ?></li>
	<li>Lances do vencedor: <?php echo isset($auction['winner_bids']) ? $auction['winner_bids'] : "Não disponível"; ?></li>
	<li>Término do leilão: <?php echo $auction['end_time'];?></li>
	<?php //<li>Início do leilão:</li> ?>
</ul>

<p>Descontos Gerados: (em breve)</p>
<ul>
	<li>x%: 1</li>
</ul>