<div id="caminho">
	<?php $this->Html->addCrumb('Meu Cadastro', array('controller' => 'users', 'action' => 'index', 'meu' => false)); ?>
	<p><?php echo $this->element('crumb'); ?></p>
	<div style="clear: both;"></div>
</div><!-- caminho -->

<div id="conteudoTitulo">
	<h2>Lances</h2>
	<div style="clear: both;"></div>
</div><!-- conteudoTitulo -->

<div class="box" style="margin-bottom: 20px; padding: 10px;">
	<font color="#666666" size="2">
	<table cellpadding="0" cellspacing="5" width="100%">
	<thead>
	<tr>
		<th style="width: 120px;">Data</th>
		<th style="width: 60px;">Lances</th>
		<th>Descrição</th>
	</tr>
	</thead>
	<?php
	foreach ($bids as $bid): ?>
	<tr>
		<td align="center"><?php echo $this->Time->format('d/m/Y H:i', $bid['Bid']['created']); ?>&nbsp;</td>
		<td align="center"><font color="#3399FF"><?php echo h($bid['Bid']['credits']); ?>&nbsp;</font></td>
		<td>
			<?php echo isset($bid['BidCredit']) ? $bid['BidCredit']['description'] : "Leilão ". $bid['Auction']['id'] ?>
			<?php //echo $this->Html->link($this->Text->truncate($bid['Auction']['title'], 40, array('ending' => '...', 'exact' => true, 'html' => false)), array('controller' => 'auctions', 'action' => 'view', $bid['Auction']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
    </font>
	<p style="display:none;">
	<?php
//	echo $this->Paginator->counter(array(
//	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
//	));
	?>	</p>

	<div class="paging" style="display:none;">
	<?php
//		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
//		echo $this->Paginator->numbers(array('separator' => ''));
//		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>